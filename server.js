
/**
 * Module dependencies.
 */
var mongoose = require('mongoose');
var config = require('./config/env.js').config();

// Bootstrap db connection
var db = mongoose.connect(config.dbConnectionString);

var dbConnection = mongoose.connection;

dbConnection.on('error', function(){
  console.error('\x1b[31m', 'Could not connect to MongoDB!');
});

dbConnection.on('connected', function(){
   // Init the express application
  var app = require('./config/express.js')(db);

  //Bootstrap passport config
  require('./config/passport')();

  var io = require('socket.io')(app);
  io.sockets.on('connection', require('./app/routes/socket'));

  // Start the app by listening on <port>
  app.listen(config.port, function(){
    console.log('Running on NODE_ENV: %s', process.env.NODE_ENV);
    console.log('Express server listening on port ' + config.port);
  });

  // Expose app
  exports = module.exports = app;
}); 
