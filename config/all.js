'use strict';

module.exports = { 
  templateEngine: 'swig', 
  assets: {
    lib: {
      css: [
        'public/vendor/animate.css/animate.css',
        'public/vendor/bootstrap/dist/css/bootstrap.min.css',
        'public/vendor/flapper/css/flapper.css',
        'public/vendor/font-awesome/css/font-awesome.css',
        'public/vendor/fonts/css/fonts.css',
        'public/vendor/ionicons/css/ionicons.min.css',
        'public/vendor/fullcalendar/fullcalendar.css',
        'public/vendor/fullcalendar/fullcalendar.print.css',
        'public/vendor/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css',
        'public/vendor/bootstrap-daterangepicker/daterangepicker-bs3.css',  
        'public/vendor/toastr/toastr.css',
        'public/vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        'public/vendor/angucomplete/angucomplete.css'
      ],
      js: [
        'public/vendor/jplayer/jquery.jplayer/jquery.jplayer.js',
        'public/vendor/jquery/dist/jquery.min.js',
        'public/vendor/jquery-ui/ui/jquery-ui.js',
        'public/vendor/bootstrap/dist/js/bootstrap.min.js',
        'public/vendor/angular/angular.min.js',
        'public/vendor/angular-route/angular-route.js',
        'public/vendor/moment/min/moment.min.js',
        'public/vendor/bootstrap-daterangepicker/daterangepicker.js',
        'public/vendor/highcharts/highcharts.js',
        'public/vendor/highcharts/modules/data.js',
        'public/vendor/highcharts/modules/drilldown.js',
        'public/vendor/flapper/src/jquery.flapper.js',
        'public/vendor/fullcalendar/fullcalendar.js',
        'public/vendor/input-mask/jquery.inputmask.js',
        'public/vendor/input-mask/jquery.inputmask.date.extensions.js',
        'public/vendor/input-mask/jquery.inputmask.extensions.js',
        'public/vendor/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js',
        'public/vendor/raphael/raphael-min.js',
        'public/vendor/angucomplete/angucomplete.js',
        'public/vendor/bootstrap-timepicker/js/bootstrap-timepicker.min.js'
      ]
    },
    css: [
      'public/css/*.css'
    ],
    js: [
      'public/js/*.js'
    ]
  }
};