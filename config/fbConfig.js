'use strict'
//=========================================================
// 
// ALL FACEBOOK RELATED APP CONFIGS
//
//=========================================================

exports.apps = [
      {
        appName: 'Nestea Beach',
        appID: '826051450772637',
        appSecret: '239d4bdf351c0798c90503c59d9728b1'
      },
      {
        appName: 'Nestea Beach',
        appID: '826051450772637',
        appSecret: '239d4bdf351c0798c90503c59d9728b1'
      },
      {
        appName: 'Nestea Beach PH',
        appID: '169921833199214',
        appSecret: '69982b11a488e9ed56d8cd560beef662'
      },
      {
        appName: 'Nestea Beach PH',
        appID: '169921833199214',
        appSecret: '69982b11a488e9ed56d8cd560beef662'
      },
      {
        appName: 'Nestea Beach PH',
        appID: '169921833199214',
        appSecret: '69982b11a488e9ed56d8cd560beef662'
      }
];
  
exports.dummyFBAccounts = [
    {
      accountName : 'nesteabeach2015@yahoo.com',
      guestAccessToken : 'CAALvShdKHJ0BAOCSDl6JJ68zhVckUyUE2OAZA22DNiyMlrrqxFS18caZAluoLecDFIA7OXTplZCVZCndSRsMLCvgmHXI1UZCjAKHo2lpKSHySeHEZCLab6FyuMZBUZAtZCa998VN23od3l2j3IBRPxSzSVl1vZBr72TlZCMo8GnOEX25rKsEbZBcuHUZA',
      appSecret : '239d4bdf351c0798c90503c59d9728b1'
    },
    {
      accountName : 'goorahna.client2@gmail.com',
      guestAccessToken : 'CAALvShdKHJ0BACNZCasGjL4NRStZAl2xRkFnaQZCJgTfJicJTZBsk1d8UuARlQlSdxnWen3ax97ZAaelSydqQZC6QhTWmCZAuswIX54SkhglGuh0KeSGyMfXFx6QzqK3MNth5qQGz22Emr3Eu79QxwetQWPM3J11y0ck9WvVd3rgOu2rOSH949jmittDVzekdwZD',
      appSecret : '239d4bdf351c0798c90503c59d9728b1'
    },
    {
      accountName : 'goorahna.client1@gmail.com',
      guestAccessToken : 'CAACaiwKB5m4BAPyYKV5qmPK6Ev2fYC2I9ccZCAma7bE21butK1hwidXNQmF60m1LYUtQep4MfE9lqZCgKM0KIuFpZBaH5NqAF7578XINUOOwWIxOh9bBpMhIRsGpCtWcO3wA51FGAJoFjLgYQdbLxUquAYkkbZB0egSQZCW22lX4mrn8Co3Xa',
      appSecret : '69982b11a488e9ed56d8cd560beef662'
    },
    {
      accountName : 'goorahna.client3@gmail.com',
      guestAccessToken : 'CAACaiwKB5m4BAPQHZAkfOtig61S5tltra25VcIaqeLjCPj4sdGNzGpr0I8RZBBpzsEaJLytQaSaYzoVO1lmSgx1MaWpFshNo9quPYXo3z31vdYkRzTGleJzK3E6bRyfBb2sFCZBYathVeUQaZAfwld1qcDIf5IsdZC96pBgLdEL0OZC7wQZA57Kq0ApYLZBrIEQZD',
      appSecret : '69982b11a488e9ed56d8cd560beef662'
    },
    {
      accountName : 'nesteabeach2015@yahoo.com',
      guestAccessToken : 'CAACaiwKB5m4BAEsCztyK3ZBgpBhFqBYDqy7x1uJENZCdNW3TrE3P95vEk1QBguinb0IXKq8gvatQadGC47qlKs6n7EoNw8URS4RvafeITX0ZBXrY3zyfn7AC1JRdMxQk7pp2ZCwSghppurNb3ncTf2OXPjMgpZCppEH90Rq1JqP4Y1MJkmVdr',
      appSecret : '69982b11a488e9ed56d8cd560beef662'
    },
    {
      accountName : 'goorahna.client2@gmail.com',
      guestAccessToken : 'CAACaiwKB5m4BAB257ebDeesIR6VwhH0RmgpIP8EK1EkyYXTkFUBCKDaBzg00urHbVnk5Kl2ktKZCckeVlBf2lUG0bp5D60Se4vZCyhyOv2fw2hLpHgNZAEfKZCVlj21ssFjHbZA2ZAIqxZAhCfJADnBB4PNZCZCZBDvRO8czrnYiCZAgr0Cw1uQfW0p',
      appSecret : '69982b11a488e9ed56d8cd560beef662'
    },
    {
      accountName : 'goorahna.client1@gmail.com',
      guestAccessToken : 'CAALvShdKHJ0BAA2qbABc7xuyAdHkuZC3danqg8CNYwdp9ndAhxw1YfyTTFyVeczDlIPZArZAG1CuLGyaFZCHWmRzmQWkZBMAAR0CnNEimoa1WhYJsWDzhNUxY897qSDJU60rGdyHWdkZAWFBGO07HGjRUZB0gH5zWEzzmskCL2Mm06QIIMfUYkraiyC4C9ZAvmYZD',
      appSecret : '239d4bdf351c0798c90503c59d9728b1'
    },
    {
      accountName : 'goorahna.client3@gmail.com',
      guestAccessToken : 'CAALvShdKHJ0BAOEDxAvIhTfsMbsZCken9ZAeRZCRtEgCc6e1QBDap6phrW0QiZADkZAOvovXknOH4tUmwP8TnjsZB46zEF5F34HBZCRWZBj1jODPnFKIok6v6RgEfQ2Tq0ZCJYcVOBh46NpGq9FYkgEur8YS4XjbgSJEZAKEToMG8jzVlOK4WQpZAEyEUCyLppH5FYZD',
      appSecret : '239d4bdf351c0798c90503c59d9728b1'
    }
];
