var testConfiguration = {
  port: process.env.PORT || 9000,
  dbConnectionString: 'mongodb://localhost:27017/ExpoAppBetaDB-Nestea2015Test1',
  mailerUserName: "exhibitapp@itemhound.com",
  mailerPassword: 'exhibitapp123!'
};

var devConfiguration = {
  port: process.env.PORT || 9000,
  dbConnectionString: 'mongodb://localhost:27017/ExpoAppBetaDB-YamahaLMWTest1',
  mailerUserName: "exhibitapp@itemhound.com",
  mailerPassword: 'exhibitapp123!'
};

var prodConfiguration = {
  port: process.env.PORT || 9000,  
  dbConnectionString: 'mongodb://localhost:27017/ExpoAppBetaDB-YamahaLMWTest1' 
};

exports.config = function () {
  process.env.NODE_ENV = process.env.NODE_ENV || "test";
  if (process.env.NODE_ENV === "development"){
    return devConfiguration;
  }
  else if (process.env.NODE_ENV === "test"){
    return testConfiguration;
  }
  else if (process.env.NODE_ENV === "production"){
    return prodConfiguration;
  }
};
