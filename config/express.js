'use strict';

/**
 * Module dependencies.
 */

var express = require('express');
var path = require('path');
var config = require('./env.js').config();
//var assets = require('./assets.js');
var MongoStore = require('connect-mongo')(express);
var passport = require('passport');
//var consolidate = require('consolidate');
var helmet = require('helmet');

/*
var routes = require('../app/controllers').routeController;
var AppDataProvider = require('../app/dataProvider/dataProvider.js').DataProvider;
var appDataProvider = new AppDataProvider();
*/

module.exports = function(db){
  //initialize express app
  var app = express();  

  // Setting application local variables
  //app.locals.jsFiles = assets.getJavaScriptAssets();
  //app.locals.cssFiles = assets.getCSSAssets();
  // app.locals.jsAdminFiles = assets.getAdminJavaScriptAssets();

  // all environments  
  app.set('port', config.port);
  app.use(express.static(path.resolve('./public')));

  // Showing stack errors
  app.set('showStackError', true);

  // Set swig as the template engine
  // app.engine('html', consolidate[assets.templateEngine]);
  
  // Set views path and view engine
  app.set('view engine', 'jade');
  // app.set('view engine', 'html');
  app.set('views', './app/views');

  // Environment dependent middleware
  if (process.env.NODE_ENV === 'development') {
    // Disable views cache
    app.use(express.errorHandler());     
    app.use(express.logger('dev'));
    app.set('view cache', false);

  } else if (process.env.NODE_ENV === 'production') {
    app.locals.cache = 'memory';
  }

  app.use(express.favicon());
  app.use(express.json());
  app.use(express.urlencoded());
  app.use(express.methodOverride());

  app.use(express.cookieParser());
  app.use(express.session({    
    secret : '_ itemhound secret 123! _',
    cookie: { maxAge: 24 * 60 * 60 * 1000 },
    store: new MongoStore({ db: db.connection.db, clear_interval: 3600 })
  }));

  // use passport session
  app.use(passport.initialize());
  app.use(passport.session());

  // Use helmet to secure Express headers
  app.use(helmet.xframe());
  app.use(helmet.xssFilter());
  app.use(helmet.nosniff());
  app.use(helmet.ienoopen());
  app.disable('x-powered-by');

  app.use(app.router);  
  
  //Require routing files
  require('../app/routes/web.js')(app);
  require('../app/routes/api.js')(app);

  //event routes
  require('../app/_NesteaBeach2015Module/routes/api.js')(app);

  // Assume 'not found' in the error msgs is a 404. this is somewhat silly, but valid, you can do whatever you like, set properties, use instanceof etc.
  app.use(function(err, req, res, next) {
    // If the error object doesn't exists
    if (!err){
      return next();
    } 
    // Log it
    console.error(err.stack);

    // Error page
    res.status(500).send("Error 500");
  });

  // Assume 404 since no middleware responded
  app.use(function(req, res) {
    res.status(404).render("404");
  });

  var server = require('http').createServer(app);

  return server;
};
