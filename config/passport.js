'use strict';

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var userController = require('../app/controllers').userController;

module.exports = function() {  
  
  //Local Strategy
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password' 
  },
  function(username, password, done){    
    userController.validateUser({"username":username, "password":password},function(error, user){
      if(error.length){
        return done(null, false, {message : 'Invalid username or password'});
      }
      else{
        return done(null, user);
      }
    });
  }));

  passport.serializeUser(function(user, done) {
    //console.log("SERIALIZING USER using ID: " + user.id);
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    userController.fetchUserById(id, function(err, user){  
      //console.log("deSERIALIZING USER using ID: " + user.id);  
      done(null, user);
    }); 
  });

};