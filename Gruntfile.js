'use strict';

module.exports = function(grunt) {

  var watchFiles = {    
    serverJS: ['Gruntfile.js', 'server.js', 'config/*.js', 'app/controllers/userController.js'],
    cclientJS: ['public/js/*.js'],
    clientCSS: ['public/css/*.css']
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),    
    watch:{
      serverJS: {
        files: watchFiles.serverJS,
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      clientJS: {
        files: watchFiles.clientJS,
        tasks: ['jshint'],
        options: {
          livereload: true
        }
      },
      clientCSS: {
        files: watchFiles.clientCSS,
        tasks: ['csslint'],
        options: {
          livereload: true
        }
      }
    },
    csslint: {
      options: {
        csslintrc: '.csslintrc',
      },
      all: {
        src: watchFiles.clientCSS
      }
    },
    jshint: {
      all: {
        src: watchFiles.clientJS.concat(watchFiles.serverJS),
        options: {
          jshintrc: '.jshintrc'
        }
      }
    },
    nodemon: {
      dev: {
        script: 'server.js',
        options: {
          nodeArgs: ['--debug'],
          ext: 'js, html',
          watch: watchFiles.serverJS
        }
      }
    },
    concurrent: {
      default: ['nodemon', 'watch'],      
      options: {
        logConcurrentOutput: true
      }
    },
    env: {
      test: {
        NODE_ENV: 'test'
      }
    }
  });
  
  // Load NPM tasks 
  require('load-grunt-tasks')(grunt);

  grunt.task.registerTask('loadConfig', 'task that loads the config into a grunt option.', function() {
    var config = require('./config/all.js');

    grunt.config.set('applicationJavaScriptFiles', config.assets.js);
    grunt.config.set('applicationcSSSFiles', config.assets.css);
  });

  grunt.registerTask('default', ['lint']);
  grunt.registerTask('lint', ['jshint', 'csslint']);  

};
