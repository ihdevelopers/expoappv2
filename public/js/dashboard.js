// var expoApp = angular.module('expoApp', ['ngRoute', 'expoApp.services']);
var expoApp = angular.module('expoApp', ['ngRoute']);


expoApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl: 'templates/main.html',
		controller: 'mainController'
	});
	$routeProvider.when('/guests', {
		templateUrl: 'templates/guests.html',
		controller: 'guestsController'
	});
	$routeProvider.otherwise({
		redirectTo: '/'
	});
}]);

expoApp.service('timeStamped', function() {
			var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
	return {
		getEpochTime: function(timestamp) {
			var date = new Date(timestamp);
			var hours;
			if (date.getHours() <= 12)
				hours = date.getHours();
			else
				hours = 12 - (24 - date.getHours());
			var finalseconds;
			var finalminutes;
			var finalhours;
			var AMPM;
			if (date.getHours() >= 12) AMPM = "PM"
			else AMPM = "AM" 
			if (hours < 10 ) finalhours = '0' + hours.toString();
			else finalhours = hours.toString();
			if (date.getMinutes() < 10 ) finalminutes = '0' + date.getMinutes().toString();
			else finalminutes = date.getMinutes().toString();
			if (date.getSeconds() < 10 ) finalseconds = '0' + date.getSeconds().toString();
			else finalseconds = date.getSeconds().toString();
			return months[date.getMonth()] + ' ' + date.getDate() + ' ' + finalhours + ':' + finalminutes + ':' + finalseconds + ' ' + AMPM;
		}
} });

expoApp.directive('toggle', function() {
    console.log('in toggle directive!')
    return {
    restrict: 'A',
    link: function(scope, element, attrs){
      if (attrs.toggle=="tooltip"){
        $(element).tooltip();
      }
      if (attrs.toggle=="popover"){
        $(element).popover();
      }
    }
  };
})

expoApp.controller('expoAppController', function($scope, $http, $interval, timeStamped) {
	console.log("in expoAppController");
	var date = new Date();
	var weekday = new Array(7);
	var month = new Array(12);

	$scope.eventsDetails = [];
	$scope.chooseEvent = {};
	$scope.submittedBooths = [];
	$scope.modalBoothProfile = {};
	$scope.eventIDsubmitted;
	$scope.boothrequirement = true;
	$scope.showoptionalbutton = true;
	$scope.user = {};

	$(".userprofile").click(function() {
		$("#userprofile").modal("hide");
	})

	$scope.hideoptionbutton = function() {
		$("#userprofile").modal("hide");
		$scope.showoptionalbutton = true;
		$('.userprofile').click(function() {
			$("#userprofile").modal("hide");
		});
	}

	$("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
    $("[data-mask]").inputmask();

    $("#logoInput").change(function () {
    	if (this.files && this.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
		    	$scope.modalEventProfile.logoImage = e.target.result;
			}
			reader.readAsDataURL(this.files[0]);
		}
	});

	$("#bgInput").change(function () {
    	if (this.files && this.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
		    	$scope.modalEventProfile.backgroundImage = e.target.result;
			}
			reader.readAsDataURL(this.files[0]);
		}
	});

	$http({
		method: 'GET',
		url: '/getUserInfo'
	}).success(function(data, status, headers, config) {
		console.log(JSON.stringify(data, null, '    '))
		if (data.status == "OK") {
			$scope.user = {
				id: data.data.id,
				username: data.data.email.split('@')[0],
				email: data.data.email,
				mobileNo: data.data.mobileNo,
				eventIDs: data.data.eventIDs
			}

			angular.forEach($scope.user.eventIDs, function(eachEvent, index) {
				$http({
					method: 'GET',
					url: '/getEventDetails',
					params: {eventID: eachEvent}
				}).success(function(data, status, headers, config) {
					if (data.status == "OK") {
						$scope.eventsDetails.push(data.data);
						if (index == ($scope.user.eventIDs.length - 1)) {
							$scope.chooseEvent = data.data;
							$scope.chooseEventDate = $scope.convertDate($scope.chooseEvent.date)
						}
					}
					else {
						$(".userprofile").html("Try Again");
						$("#userprofileheader").html("Error Message")
		   
						var options = {
						  show: true,
						  keyboard: false,
						  backdrop: 'static'
						}
						$("#userprofile").modal(options);
						$(".modal-body h3").html("Event Details Cannot Be Retrieved.");
					}
				});
			});
		}
	});	

	month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

	$scope.dateNow = weekday[date.getDay()] + ', ' + month[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
	console.log("$scope.dateNow: " + $scope.dateNow)
	$scope.timeNow = timeStamped.getEpochTime(new Date().getTime());


	$interval(function () {
		$scope.timeNow = timeStamped.getEpochTime(new Date().getTime());
	}, 1000); // every 17 milliseconds just to spiced it up a bit.

	$scope.convertDate = function(date) {
		return moment(date).format('MMMM DD, YYYY');
	}

	$scope.editEventProfile = function() {
		$(".editeventprofile").html("Edit event");
		$("#editeventprofileheader").html("Edit Event Profile");
   
		var options = {
		  show: true,
		}
		$http({
			method: 'GET',
			url: '/getAllEventBooths',
			params: {eventID: $scope.chooseEvent.id}
		}).success(function(data) {
			console.log('Event Booths:')
			$scope.alleventBooths = data.data
			console.log($scope.alleventBooths);

		});
		$("#editeventprofile").modal(options);
		$(".editeventprofile").click(function() {

		});
		// $http({
		// 	method: 'POST',
		// 	url: '/specialAddAdmin',
		// 	data: {admin: {username: 'irise@itemhound.com', password: 'sosecret', email:"irise@itemhound.com", mobileNo: "09173147616"}}
		// }).success(function(data) {
		// 	console.log(data)
		// })
	}

	$scope.showUserProfile = function() {
		$(".userprofile").html("Okay");
   
		var options = {
		  show: true,
		  keyboard: false,
		  backdrop: 'static'
		}
		$("#userprofile").modal(options);
		$(".modal-body h3").html("View User profile goes here.");
	}

	$scope.viewSettings = function() {
		$(".userprofile").html("Okay");
		$("#userprofileheader").html("Change User Settings")
   
		var options = {
		  show: true,
		  keyboard: false,
		  backdrop: 'static'
		}
		$("#userprofile").modal(options);
		$(".modal-body h3").html("Change User settings goes here.");
	}

	$scope.getEventProfile = function() {
		if ($scope.chooseEvent.name == "Create New Event...") {
			$(".userprofile").html("Continue");
			$("#userprofileheader").html("Create New Event")
	   
			var options = {
			  show: true
			}
			$("#userprofile").modal(options);
			$(".modal-body h3").html("Create New event goes here.");
		}
		$scope.chooseEventDate = $scope.convertDate($scope.chooseEvent.date)
	}

	$scope.createEvent = function() {
		$(".eventprofile").html("Next Page");
		$("#eventprofileheader").html("Create New Event");

		var options = {
		  show: true
		}
		$("#eventprofile").modal(options);
	}

	$scope.submitEventForm = function() {
		$("#eventprofile").modal("hide");

		$("#guestprofileheader").html("Choose Guest Fields");
		var options = {
		  show: true
		}
		$("#guestprofile").modal(options);
	}

	$scope.submitFieldsForm = function() {
		$("#guestprofile").modal("hide");

		$scope.modalEventProfile.guestDetailsRequired = []; 
		var fields = ['uID', 'firstName', 'lastName', 'middleName', 'nickName', 'gender', 'creditIDs', 'transactionIDs', 'eventID', 'email',
						'mobileNo', 'title', 'profession', 'company', 'education', 'streetAddress1', 'streetAddress2', 'cityAddress',
						'regionAddress', 'country',	'zip',	'religion', 'facebookPage', 'twitterHandle', 'instagramHandle', '', 'birthDate']; 

		if($scope.modalEventProfile.preRegAvailable == "true") $scope.modalEventProfile.preRegAvailable = true;
		else $scope.modalEventProfile.preRegAvailable = false;

		if($scope.modalEventProfile.activationRequired == "true") $scope.modalEventProfile.activationRequired = true;
		else $scope.modalEventProfile.activationRequired = false;

		for (var prop in $scope.modalGuestProfile) {
			if ($scope.modalGuestProfile.hasOwnProperty(prop)) {
				$scope.modalEventProfile.guestDetailsRequired.push(fields.indexOf(prop) + 1);
			}
		}

		$scope.modalEventProfile.organizerID = $scope.user.id;

		$http({
			method: 'POST',
			url: '/createEvent',
			data: {newEvent: $scope.modalEventProfile}
		}).success(function(data, status, headers, config) {
			if (data.status == "OK") {
				$scope.eventIDsubmitted = data.data;
				$scope.modalEventProfile = {};
				$scope.modalGuestProfile = {};
				$("#userprofileheader").html("Message Alert");
				var options = {
				  show: true,
				  keyboard: false,
				  backdrop: 'static'
				}
				$scope.showoptionalbutton = false;

				$("#userprofilebody").html("Successfully created event!");
				$("#userprofile").modal(options);
				$(".userprofile").html("Add booths now");
				$(".userprofile").click(function() {
					$scope.showoptionalbutton = true;
					$("#userprofile").modal("hide");
					$('.userprofile').click(function() {
						$("#userprofile").modal("hide");
					});
		
					$("#boothprofileheader").html("Add New Booths");
					var options = {
					  show: true
					}
					$("#boothprofile").modal(options);
				});
			}
			else {
				$("#userprofileheader").html("Message Alert");
				var options = {
				  show: true
				}
				$("#userprofilebody").html("Failed in adding new event. Please try again.");
				$("#userprofile").modal(options);
			}
		});		
	}

	$scope.showboothDialog = function() {
		$scope.eventIDsubmitted = $scope.chooseEvent.id;
		$("#editeventprofile").modal("hide");
		
		$("#boothprofileheader").html("Add New Booths");
		var options = {
		  show: true
		}
		$("#boothprofile").modal(options);
	}

	$scope.backGuestModal = function() {
		$("#boothprofile").modal("hide");
		
		$("#guestprofileheader").html("Choose Guest Fields");
		var options = {
		  show: true
		}
		$("#guestprofileheader").modal(options);
	}

	$scope.backModal = function() {
		$("#guestprofile").modal("hide");
		
		$("#eventprofileheader").html("Create New Event");
		var options = {
		  show: true
		}
		$("#eventprofile").modal(options);
	}

	$scope.addAnotherBoothModal = function() {
		if (($scope.modalBoothProfile.name != "") || ($scope.modalBoothProfile != {})) {
			$scope.submittedBooths.push({name: $scope.modalBoothProfile.name, type: $scope.modalBoothProfile.type, maxTransactionsPerGuest: $scope.modalBoothProfile.maxTransactionsPerGuest});
			$scope.boothrequirement = true;
		}
		else $scope.boothrequirement = false;
	}

	$scope.submitAllBooths = function() {
		var length = $scope.submittedBooths.length - 1;

		submitBooth();

		function submitBooth() {
			var element = $scope.submittedBooths[length];
			console.log('Submit Booth', element);
			element.eventID = $scope.eventIDsubmitted;
			element.claimDataIDs = [];
			element.creditDataIDs = [];
			$http({
				method: 'POST',
				url: '/addBooth',
				data: {newBooth: element}
			}).success(function(data, status, headers, config) {
				if (data.status == "OK") {
					length = length - 1;
					if (length < 0) {
						$("#boothprofile").modal("hide");
						$scope.modalBoothProfile = {};
						$scope.submittedBooths = [];	
					}
					else submitBooth();
				}
				else {
					console.log(data);
				}
			});
		}
	}
});

expoApp.controller('mainController', function($scope, $http) {
	$scope.$on('$viewContentLoaded', onReady);

	function onReady() {
		 $('#container').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: 'Average Hourly Count of Incoming Registered and Activated Guests'
        },
        // subtitle: {
        //     text: 'Source: WorldClimate.com'
        // },
        xAxis: [{
            categories: ['17:00 - 17:30', '17:30 - 18:00', '18:30 - 19:00', '19:00 - 19:30', '19:30 - 20:00', '20:00 - 20:30',
                '20:30 - 21:00', '21:00 - 21:30', '21:30 - 22:00', '22:00 - 22:30', '22:30 - 23:00', '23:00 - 23:30']
        }, {labels: {rotation: -45}}],
        yAxis: [{ // Primary yAxis
            labels: {
                // format: '{value}°C',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Number of Guests',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }, { // Secondary yAxis
            title: {
                text: 'Registered Guests',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            labels: {
                // format: '{value} mm',
                style: {
                    color: Highcharts.getOptions().colors[0]
                }
            },
            opposite: true
        }],
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y: .0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        series: [{
            name: 'Registered',
            type: 'column',
            yAxis: 1,
            data: [0, 10, 53, 62, 102, 103, 93, 90, 35, 23, 11, 0],
            tooltip: {
                // valueSuffix: ' mm'
            }

        }, {
            name: 'Activated',
            type: 'line',
            yAxis: 1,
            data: [0, 10, 53, 62, 102, 103, 93, 90, 35, 23, 11, 0],
            tooltip: {
                // valueSuffix: '°C'
            }
        }]
    });

// Make monochrome colors and set them as default for all pies
    Highcharts.getOptions().plotOptions.pie.colors = (function () {
        var colors = [],
            base = Highcharts.getOptions().colors[0],
            i;

        for (i = 0; i < 10; i += 1) {
            // Start out with a darkened base color (negative brighten), and end
            // up with a much brighter color
            colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        }
        return colors;
    }());

    // Build the chart
    $('#container2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Average Hourly Update of Booths Visited'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Booths visited',
            data: [
                ['Balance Board',   45.0],
                ['Dew Pong',       26.8],
                {
                    name: 'Neon Wall',
                    y: 12.8,
                    sliced: true,
                    selected: true
                },
                ['Skate Safe',    8.5],
                ['Neon Pool',     6.2],
                ['Facebook Like',   0.7]
            ]
        }]
    });

		$scope.totalPresent = 0;
		$scope.totalRegistered = 0;
		$scope.totalnotyetRegistered = 0;
		$scope.totalPreregistered = 0;
		$scope.totalEngagements = 0;
		$scope.boothMost = 0;
		$scope.boothMost2 = 0;
		$scope.boothMost3 = 0;

		$scope.$watch('chooseEvent', function() {
			$http({
				method: 'GET',
				url: '/getAllEventGuests',
				params: {eventID: $scope.chooseEvent.id}
			}).success(function(data, status, headers, config) {
				if (data.status == "OK") {
					$http({
						method: 'GET',
						url: '/getAllEventTransactions',
						params: {eventID: $scope.chooseEvent.id}
					}).success(function(data2, status, headers, config) {
						if (data.status == "OK") {
							$scope.allTransactions = data2.data;
							console.log('Event Transactions:', $scope.allTransactions);
							$scope.totalPresent = getAllPresentGuests(data.data).length;
							$scope.totalRegistered = getAllRegisteredGuests(data.data).length;
							// $scope.totalnotyetRegistered = getAllUnregisteredGuests(data.data).length;
							$scope.totalPreregistered = getAllPreregisteredGuests(data.data).length;

							$scope.totalEngagements = getTotalEngagements($scope.allTransactions).length;
						}
					});
				}
				else {
					// $(".userprofile").html("Try Again");
					// $("#userprofileheader").html("Error Message")
	   
					// var options = {
					//   show: true,
					//   keyboard: false,
					//   backdrop: 'static'
					// }
					// $("#userprofile").modal(options);
					// $(".modal-body h3").html(data.message);
				}
			});
		});
			
	}

	function getAllPresentGuests(list) {
		var finalList = [];
		angular.forEach(list, function(guest) {
			if (guest.isPresent == true) finalList.push(guest);
		});
		return finalList;
	}

	function getAllRegisteredGuests(list) {
		var finalList = [];
		angular.forEach(list, function(guest) {
			if (guest.preRegistered == false) finalList.push(guest);
		});
		return finalList;
	}

	function getAllUnregisteredGuests(list) {
		var finalList = [];
		// angular.forEach(list, function(guest) {
		// 	if (guest.isPresent == true) finalList.push(guest);
		// });
		return finalList;
	}

	function getAllPreregisteredGuests(list) {
		var finalList = [];
		angular.forEach(list, function(guest) {
			if (guest.preRegistered == true) finalList.push(guest);
		});
		return finalList;
	}

	function getTotalEngagements(list) {
		var finalList = [];
		angular.forEach(list, function(transaction) {
			if (transaction.data.type == "checkin") {
				finalList.push(transaction);
			}
		});
		return finalList;
	}

	function getMostvisitedBooths(list) {
		var finalList = [];
		angular.forEach(list, function(transaction) {
			finalList.push(transaction);
		});
		return finalList;
	}
});

expoApp.controller('guestsController', function($scope, $http, timeStamped) {
	$scope.$on('$viewContentLoaded', onReady);

	function onReady() {

		$scope.$watch('chooseEvent', function() {
			$scope.iris2 = [];
			$http({
				method: 'GET',
				url: '/getAllEventGuests',
				params: {eventID: $scope.chooseEvent.id}
			}).success(function(data, status, headers, config) {
				if (data.status == "OK") {
					$scope.iris2 = $scope.iris2.concat(getAllPreregisteredGuests(data.data), getAllRegisteredGuests(data.data));
				}
				else {
				}
			});

		});
	}

	function getAllPresentGuests(list) {
		var finalList = [];
		angular.forEach(list, function(guest) {
			if (guest.isPresent == true) finalList.push(guest);
		});
		return finalList;
	}

	function getAllRegisteredGuests(list) {
		var finalList = [];
		angular.forEach(list, function(guest) {
			if (guest.preRegistered == false) {
				guest.type = "Registered onsite";
				finalList.push(guest);
			}
		});
		return finalList;
	}

	function getAllUnregisteredGuests(list) {
		var finalList = [];
		// angular.forEach(list, function(guest) {
		// 	if (guest.isPresent == true) finalList.push(guest);
		// });
		return finalList;
	}

	function getAllPreregisteredGuests(list) {
		var finalList = [];
		angular.forEach(list, function(guest) {
			if (guest.preRegistered == true) {
				guest.type = "Pre-registered";
				guest.activated = false;
				finalList.push(guest);
			}
		});
		return finalList;
	}

	function checkActivation(list) {
		var finalList = [];
		angular.forEach(list, function(guest) {
		});
	}

	$scope.getTimeString = function(time) {
		return timeStamped.getEpochTime(time);
	}

});