var counter = 0; // counter to detect time elapsed
var confettiInterval;
var airportTimer;
var W; // window width
var H; // window height

var socket = io.connect();
socket.on('start-draw', function (data) {
    console.log('picking winner');
    pickWinner(data);
});

var opts = {
    chars_preset: 'alphanum',
    align: 'left',
    width: 15,
    timing: 100,
    //min_timing: 300
};

$(document).ready(function(){
    
    $(document).keypress(function(e) {
        if(e.which == 13) {
            pickWinner();
        }
    })

// <<<<<<< HEAD
//     $("#congrats").css('width', '720px')
//     $("#congrats").css('height', '100px')
// =======
    // $("#congrats").width('css', '720px');
    // $("#congrats").height('css', '100px');
// >>>>>>> bca0bf85b29382ce80008c448f88b348dc7a8cf4

    W = window.innerWidth;
    H = window.innerHeight;

    document.getElementById('name').style.visibility = 'hidden';
    document.getElementById('firstname').style.visibility = 'visible';
    document.getElementById('airportdiv').style.visibility = 'visible';
    
    $('input.display').flapper(opts);

    $("#drums").jPlayer({
        swfPath: "/Jplayer.swf",
        wmode: "window"
    });

    $('#drums').jPlayer("setMedia", {mp3:"/drumroll.mp3"});
    $('#drums').jPlayer("supplied", "mp3");
    
    $("#applause").jPlayer({
        swfPath: "/Jplayer.swf",
        wmode: "window"
    });

    $('#applause').jPlayer("setMedia", {mp3:"/applause-1.mp3"});
    $('#applause').jPlayer("supplied", "mp3");

    /* Make <canvas> the background of the document */
    var canvas = $("canvas");
    var ctx = canvas[0].getContext("2d"),
    width = $(document).width(),
    height = $(document).height(), a, b;
    canvas.attr("width", width);
    canvas.attr("height", height);

});

function startConfetti() {
    // initialize canvas
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");

    // initialize dimensions
    W = window.innerWidth;
    H = window.innerHeight;
    canvas.width = W;
    canvas.height = H;

    // confetti particles
    var mp = 300; //max particles
    var particles = [];
    for(var i = 0; i < mp; i++)
    {
        particles.push({
            x: Math.random()*W, //x-coordinate
            y: Math.random()*H, //y-coordinate
            //r: Math.random()*4+1, //radius
            //d: Math.random()*mp, //density
            width: Math.random()*9+1,
            height: Math.random()*9+1,
            color: "rgba(" + Math.floor((Math.random() * 255)) +", " + Math.floor((Math.random() * 255)) +", " + Math.floor((Math.random() * 255)) + ", 0.8)"
        })
    }

    // drawing flakes
    function draw()
    {
        ctx.clearRect(0, 0, W, H);

        for(var i = 0; i < mp; i++)
        { 
            var p = particles[i];
            ctx.beginPath();
            ctx.fillStyle = p.color;
            ctx.moveTo(p.x, p.y);
            //ctx.arc(p.x, p.y, p.r, 0, Math.PI*2, true);
            ctx.fillRect(p.x,p.y,p.width,p.height);
            ctx.fill();
        }

        update();
    }

    // moving snowflakes
    var angle = 0;
    function update()
    {
        angle += 0.01;
        for(var i = 0; i < mp; i++)
        {
            var p = particles[i];
            //p.y += Math.cos(angle+p.d) + 1 + p.r/2;
            //p.x += Math.sin(angle) * 2;
            p.y += Math.cos(angle+p.height) + 1 + p.width/2;
            p.x += Math.sin(angle) * 2;

            // sending flakes back from the top when it exits
            if(p.x > W+5 || p.x < -5 || p.y > H)
            {
                if(i%3 > 0) //66.67% of the flakes
                {
                    //particles[i] = {x: Math.random()*W, y: -10, r: p.r, d: p.d, color : p.color};
                    particles[i] = {x: Math.random()*W, y: -10, width: p.width, height: p.height, color: p.color};
                }
                else
                {
                    //If the flake is exitting from the right
                    if(Math.sin(angle) > 0)
                    {
                        //Enter from the left
                        //particles[i] = {x: -5, y: Math.random()*H, r: p.r, d: p.d, color: p.color};
                        particles[i] = {x: -5, y: Math.random()*H, width: p.width, height: p.height, color: p.color};
                    }
                    else
                    {
                        //Enter from the right
                        //particles[i] = {x: W+5, y: Math.random()*H, r: p.r, d: p.d, color : p.color};
                        particles[i] = {x: W+5, y: Math.random()*H, width: p.width, height: p.height, color : p.color};
                    }
                }
            }
        }
    }

    // animation loop
    confettiInterval = setInterval(draw, 33);
};

var participants = [];

function fetchDB() {
    $.ajax({
        method: "GET",
        url: "/getAllGuestsWithValidCredits",
        data: {credit: {eventID:1, creditDataID: 1, minTotalValue: 1}}
    }).done(function(data) { 
        console.log('data', data);
        if ((data.status == "OK") && (data.data.length > 0)) {
            participants = [];
            console.log(data.data)
            data.data.forEach(function(member, index) {
                var date = new Date(member.timeRegistered);
                if ((member.totalValue > 1) && ((date.getDate() == new Date().getDate()) && (date.getMonth() == new Date().getMonth()) && (date.getFullYear() == new Date().getFullYear())))  {
                    for (i = 0; i < member.totalValue; i++) {
                        participants.push({uID: member.uID, firstName: member.firstName, lastName: member.lastName});
                    }
                }
                else if (member.totalValue == 1) participants.push({uID: member.uID, firstName: member.firstName, lastName: member.lastName});
            });
            pickWinInProgress = true;
            var itemhoundregex = /itemhound/i;
            participants = participants.filter(function(a){return (!a.won)});
            participants = participants.filter(function(a){return !(itemhoundregex.test(a.COMPANY))});
            
            // make necessary elements hidden
            document.getElementById('congrats').style.visibility = 'hidden';
            // clear confetti - supposedly
            document.getElementById('canvas').getContext("2d").clearRect(0,0,W,H);
            // $('#drums').jPlayer("play");
            // $('#applause').jPlayer("stop");
             
            airportTimer = setInterval(airportStyle, 500); // one person in chosen, shown after 10 secs
            }
        else {
            alert('No guest with valid credits. Please try again.')
            // participants = [
            //     {id: 1, firstName: "Iris", lastName: "Endozo"},
            //     {id: 2, firstName: "Mary Jane", lastName: "Madelo"}
            // ]
            // airportTimer = setInterval(airportStyle, 500);
        }
    });   
};

var pickWinInProgress = false;

function pickWinner(){
    if (pickWinInProgress) return;
    fetchDB();
};

function airportStyle(){
        counter++;
        if(counter == 20) //end of first run
        {
            clearInterval(airportTimer);
            increment = 0;
            counter = 0;
            // startConfetti();
            pickWinInProgress = false;
            // $('#drums').jPlayer("stop");
            // $('#applause').jPlayer("play");
            document.getElementById('congrats').style.visibility = 'visible';
            // (participants[totoongWinner.index]).won = true;;
            console.log(totoongWinner)
            $.ajax({
                method: 'GET',
                url: '/getGuestDetails',
                data: {guestID: null, uID: totoongWinner.uID, isGuestID: false}
            }).done(function(data){
                console.log(data)
                if (data.status == "OK") {
                    socket.emit('new-winner',{data: {data: {eventID: 1, boothID: 11, uID: data.data.uID, claimDataID: 1, timestamp: new Date().getTime()}} , winner: totoongWinner});
                }
                else {
                    alert('Error getting guest details from server.');
                }
            })
        }
        else
        {
            document.getElementById('canvas').getContext("2d").clearRect(0,0,W,H);
            clearInterval(confettiInterval);
            //choose winner
            if(counter<19)
            {
                var max = participants.length - 1;
                var min = 0;
                var randNum = Math.floor(Math.random() * (max - min + 1) + min);
                
                $('#firstname').val((participants[randNum].firstName).toUpperCase()).change();
                $('#surname').val((participants[randNum].lastName).toUpperCase()).change();
                totoongWinner = participants[randNum];
                totoongWinner.index = randNum;
            }
        }
}

var totoongWinner = {};
