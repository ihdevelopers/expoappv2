
expoApp.service('timeStamped', function() {
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  return {
    getEpochTime: function(timestamp) {
      var out = moment(timestamp).format('MMM DD hh:mm:ss A');
      return out;
      /*
      var date = new Date(timestamp);
      var hours;
      if (date.getHours() <= 12)
        hours = date.getHours();
      else
        hours = 12 - (24 - date.getHours());
      var finalseconds;
      var finalminutes;
      var finalhours;
      var AMPM;
      if (date.getHours() >= 12) AMPM = "PM"
      else AMPM = "AM" 
      if (hours < 10 ) finalhours = '0' + hours.toString();
      else finalhours = hours.toString();
      if (date.getMinutes() < 10 ) finalminutes = '0' + date.getMinutes().toString();
      else finalminutes = date.getMinutes().toString();
      if (date.getSeconds() < 10 ) finalseconds = '0' + date.getSeconds().toString();
      else finalseconds = date.getSeconds().toString();
      return months[date.getMonth()] + ' ' + date.getDate() + ' ' + finalhours + ':' + finalminutes + ':' + finalseconds + ' ' + AMPM;
      */
    }
  } 
});

expoApp.service('Toastr', function() {
  toastr.options.extendedTimeOut = 0;
  toastr.options.fadeOut = 250;
  toastr.options.fadeIn = 250;
  toastr.options.closeButton = false;
  toastr.options.debug = false;
  toastr.options.progressBar = false;
  toastr.options.onclick = null;
  toastr.options.showDuration = "300";
  toastr.options.hideDuration = "1000";
  toastr.options.timeOut = "5000";
  toastr.options.extendedTimeOut = "1000";
  toastr.options.showEasing = "swing";
  toastr.options.hideEasing = "linear";
  toastr.options.showMethod = "fadeIn";
  toastr.options.hideMethod = "fadeOut";

  return {
    show: function(type, msg, title) {
       toastr[type](msg, title);
    }
  }
});

expoApp.directive('toggle', function() {
    return {
    restrict: 'A',
    link: function(scope, element, attrs){
      if (attrs.toggle=="tooltip"){
        $(element).tooltip();
      }
      if (attrs.toggle=="popover"){
        $(element).popover();
      }
    }
  };
});

expoApp.directive('showtab', function() {
  return {
    link: function (scope, element, attrs) {
      element.click(function (e) {
        e.preventDefault();
        $(element).tab('show');
      })
    }
  }
})

expoApp.filter('convertDateMMMMDDYYYY', function(){
  return function(input) {
    var out = moment(input).format('MMMM DD, YYYY');     
    return out;
  };
});

expoApp.filter('unixTimeToTimeString', function(){
  return function(timestamp) {
    var out = moment(timestamp).format('MMM DD hh:mm:ss A');
    return out;
  };
});
