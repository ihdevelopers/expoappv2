var expoApp = angular.module('expoApp', ['ngRoute', 'datatables']);

expoApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'js/expoApp/partials/main.html',
    controller: 'mainController'
  });
  $routeProvider.when('/guests', {
    templateUrl: 'js/expoApp/partials/guests.html',
    controller: 'guestsController'
  });
  $routeProvider.when('/engagements', {
    templateUrl: 'js/expoApp/partials/engagements.html',
    controller: 'engagementsController'
  });
  $routeProvider.otherwise({
    redirectTo: '/'
  });
}]);
