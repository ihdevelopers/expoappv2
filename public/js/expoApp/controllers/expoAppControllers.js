expoApp.controller('expoAppController', function($scope, $http, $interval, timeStamped, Toastr) {
  var date = new Date();
  var weekday = new Array(7);
  var month = new Array(12);

  //------- INIT SCOPE VARIABLES -------//
  $scope.eventsDetails = [];
  $scope.createdNewEvent = false;
  $scope.chooseEvent = {};
  $scope.submittedBooths = [];
  $scope.modalBoothProfile = {};
  $scope.eventIDsubmitted;
  $scope.showBoothFieldsError = false;
  $scope.showEditBoothFieldsError = false;
  $scope.disableSubmitAllBooths = true;  
  $scope.editEventProfileMode = false;
  $scope.editUserProfile = false;
  $scope.user = {};
  $scope.boothToEdit = {};
  $scope.eventToEdit = {};
  var origUserDetails = {};


  //------- INITIALIZE EVENT DETAILS -------//
  var getRandomArbitrary = function(min, max) {
    return Math.random() * (max - min) + min;
  } // testing purposes only

  function getAllPresentGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.isPresent == true) finalList.push(guest);
    });
    return finalList;
  }

  function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
  }

  function process() {
    angular.forEach($scope.user.eventIDs, function(eachEvent, index) {
      console.log("eachEvent[" + index + "]: " + eachEvent);
      $http({
        method: 'GET',
        url: '/getEventDetails',
        params: {eventID: eachEvent}
      }).success(function(data, status, headers, config) {
        if (data.status == "OK") {
          $scope.eventsDetails.push(data.data);
          if (index == ($scope.user.eventIDs.length - 1)) {
            $scope.chooseEvent = data.data;
            // $http({
            //   method: 'GET',
            //   url: '/getAllEventGuests',
            //   params: {eventID: $scope.chooseEvent.id, limitedFieldsOnly: true}
            // }).success(function(getAllEventGuests_res, status) {
            //   if(getAllEventGuests_res.status == "OK") {
            //     var allPresentGuests = getAllPresentGuests(getAllEventGuests_res.data);
            //     var allUniqueDates = [];
            //     angular.forEach(allPresentGuests, function(eachGuest) {
            //       // eachGuest.timePresent = getRandomArbitrary(1430359200000, 1430683200000); // for testing purposes only (old db model, no timePresent field yet)
            //       eachGuest.timePresentString = moment(eachGuest.timePresent).format("MM/DD/YYYY");
            //     })
            //     allPresentGuests.sort(dynamicSort('timePresent'));
            //     allUniqueDates = _.uniq(_.pluck(allPresentGuests, "timePresentString"));
            //     console.log("allUniqueDates:")
            //     console.log(allUniqueDates)
            //     angular.forEach(allUniqueDates, function(eachDate){
            //       var _obj = new Object();
            //       _obj.day = eachDate;
            //       // $scope.chooseEventDay.push(_obj);
            //     })
            //     $scope.chooseEventDay.push({"day": "03/24/2015"}) // DELETE THIS! AND UNCOMMENT '$scope.chooseEventDay.push(_obj);'
            //     $scope.chooseThisEventDay = _.first($scope.chooseEventDay);
            //     console.log($scope.chooseThisEventDay)
            //   }
            // })
          }
        }
        else {
          Toastr.show('warning', 'Event Details Cannot Be Retrieved.', 'Error Occurred!');              
        }
      });
    });
  }

  function initializeEventDetails(){
    $scope.eventsDetails = [];

    if(_.isEmpty($scope.user)){
      $http({
        method: 'GET',
        url: '/getUserInfo'
      }).success(function(data, status, headers, config) {
        if (data.status == "OK") {
          $scope.user = {
            id: data.data.id,
            firstName: data.data.firstName,
            lastName: data.data.lastName,
            username: data.data.username,
            email: data.data.email,
            mobileNo: data.data.mobileNo,
            eventIDs: data.data.eventIDs
          };

          origUserDetails = {
            id: data.data.id,
            firstName: data.data.firstName,
            lastName: data.data.lastName,
            username: data.data.username,
            email: data.data.email,
            mobileNo: data.data.mobileNo,
            eventIDs: data.data.eventIDs
          };

          // angular.forEach($scope.user.eventIDs, function(eachEvent, index) {
          //   $http({
          //     method: 'GET',
          //     url: '/getEventDetails',
          //     params: {eventID: eachEvent}
          //   }).success(function(data, status, headers, config) {
          //     if (data.status == "OK") {
          //       $scope.eventsDetails.push(data.data);
          //       if (index == ($scope.user.eventIDs.length - 1)) {
          //         $scope.chooseEvent = data.data;
                  
          //       }
          //     }
          //     else {
          //       Toastr.show('warning', 'Event Details Cannot Be Retrieved.', 'Error Occurred!');              
          //     }
          //   });
          // });
          process();
        }
      });
    }
    else{      
      process();
    }
  }

  initializeEventDetails();


  //------- DATE/TIME -------//
  var date = new Date();
  var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  $scope.dateNow = weekday[date.getDay()] + ', ' + month[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
  $scope.timeNow = timeStamped.getEpochTime(new Date().getTime());

  $interval(function () {
    $scope.timeNow = timeStamped.getEpochTime(new Date().getTime());
  }, 500);


  //------- USER PROFILE -------//
  $scope.showUserProfile = function() {
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $("#userprofile").modal(options);    
  };

  $scope.hideUserProfile = function(){
    $("#userprofile").modal("hide");
    $scope.editUserProfile = false;
    $scope.user = origUserDetails;    
  };

  $scope.toggleEditUserProfile = function(){
    $scope.editUserProfile = !$scope.editUserProfile;
  };

  $scope.editUserInfo = function(userInfo){

    //TODO: add post to edit user info api here //
    $("#userprofile").modal("hide");
    $scope.editUserProfile = false;

    updateUserInfo(userInfo, function(err){
      if(err){
        console.log(err);
        Toastr.show('warning', 'Update user info failed!', 'Update Failed!'); 
      }
      else{
        origUserDetails = {
          firstName: userInfo.firstName,
          lastName: userInfo.lastName,
          username: userInfo.username,
          email: userInfo.email,
          mobileNo: userInfo.mobileNo,
          eventIDs: userInfo.eventIDs
        };        
        Toastr.show('success', 'Successfully updated user info!', 'Update Successful!'); 
      }
    });
  };


  //------- EVENT DETAILS -------//
  $scope.openCreateEventModal = function() {
    var options = {
      show: true
    };
    $("#eventprofile").modal(options);
  };

  $scope.submitCreateNewEventForm = function() {
    $("#eventprofile").modal("hide");
    var options = {
      show: true
    }
    $("#guestprofile").modal(options);
  };

  $scope.returnToCreateEventModal = function() {
    $("#guestprofile").modal("hide");    
    
    var options = {
      show: true
    }
    $("#eventprofile").modal(options);
  };

  $scope.submitGuestFieldsForm = function() {
    $("#guestprofile").modal("hide");

    $scope.modalEventProfile.guestDetailsRequired = []; 
    var fields = ['uID', 'firstName', 'lastName', 'middleName', 'nickName', 'gender', 'creditIDs', 'transactionIDs', 'eventID', 'email','mobileNo', 'title', 'profession', 'company', 'education', 'streetAddress1', 'streetAddress2', 'cityAddress','regionAddress', 'country', 'zip',  'religion', 'facebookPage', 'twitterHandle', 'instagramHandle', '', 'birthDate']; 

    $scope.modalEventProfile.preRegAvailable = ($scope.modalEventProfile.preRegAvailable == "true") ? true : false;   
    $scope.modalEventProfile.activationRequired = ($scope.modalEventProfile.activationRequired == "true") ? true : false;

    for (var prop in $scope.modalGuestProfile) {
      if ($scope.modalGuestProfile.hasOwnProperty(prop)) {
        $scope.modalEventProfile.guestDetailsRequired.push(fields.indexOf(prop) + 1);
      }
    }

    $scope.modalEventProfile.organizerID = $scope.user.id;

    $http({
      method: 'POST',
      url: '/createEvent',
      data: {newEvent: $scope.modalEventProfile}
    }).success(function(data, status, headers, config) {
      if (data.status == "OK") {

        //Add New Event to user's eventId list
        $scope.user.eventIDs.push(data.data);
        $scope.createdNewEvent = true;

        $scope.eventIDsubmitted = data.data;
        $scope.modalEventProfile = {};
        $scope.modalGuestProfile = {};

        var options = {
          show: true,
          keyboard: false,
          backdrop: 'static'
        };

        $("#createBoothPromptDialog").modal(options);
      }
      else {
        Toastr.show('warning', 'Failed in adding new event. Please try again.', 'Create New Event Failed!');
      }
    });   
  };  

  $scope.openEditEventModal = function() {
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $("#editeventprofile").modal(options);    
  };

  $scope.hideEditEventProfileModal = function(){
    $scope.editEventProfileMode = false;
    $("#editeventprofile").modal("hide");
  };

  $scope.submitEditEventInfoForm = function(eventDetails){

    validateEventInfoForm(eventDetails, function(err){
      if(err){
        Toastr.show('warning', err, 'Failed to edit event!');
      }
      else{
        updateEventInfo(eventDetails, function(err){
          if(err){
            console.log(err);
            Toastr.show('warning', 'Failed to update event info!', 'Error!');
          }
          else{
            Toastr.show('success', 'Successfully updated event info!', 'Update Successful!'); 
            $("#editeventprofile").modal("hide");
            $scope.editEventProfileMode = false;

            //apply to scope new event details
            $scope.chooseEvent.name = eventDetails.name;
            $scope.chooseEvent.date = moment(eventDetails.date);
            $scope.chooseEvent.location = eventDetails.location;
            $scope.chooseEvent.guestEstimate = eventDetails.guestEstimate;
          }          
        });
      }
    });
  };

  $scope.editEventProfile = function(eventDetails){
    $scope.eventToEdit = angular.copy(eventDetails);
    console.log($scope.eventToEdit);
    $scope.editEventProfileMode = !$scope.editEventProfileMode;    
  };


  //------- BOOTH DETAILS -------//
  $scope.openEditEventBoothsModal = function(){
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $http({
      method: 'GET',
      url: '/getAllEventBooths',
      params: {eventID: $scope.chooseEvent.id}
    }).success(function(data) {
      $scope.alleventBooths = data.data;
    });
    $("#editboothprofile").modal(options);
  };

  $scope.closeEditEventBoothsModal = function(){
    $("#editboothprofile").modal('hide');
  };

  $scope.openAddBoothModal = function() {
    $scope.eventIDsubmitted = $scope.chooseEvent.id;    
    $("#editboothprofile").modal("hide");    
    
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $("#boothprofile").modal(options);
  };

  $scope.hideAddBoothModal = function(){
    $("#boothprofile").modal('hide');
  };

  $scope.addBoothsLater = function(){
    //chose to add booths, but changed mind
    $("#boothprofile").modal('hide');

    //reset flag
    $scope.createdNewEvent = false;

    //get newly created event
    initializeEventDetails();
  };

  $scope.openFromNewEventAddBoothsModal = function(){
    $("#createBoothPromptDialog").modal("hide");
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $("#boothprofile").modal(options);
  };

  $scope.closeCreateBoothPromptModal = function(){
    //chose to add booths later;
    $("#createBoothPromptDialog").modal('hide');
    //get newly created event
    console.log('initializeEventDetails due to closeCreateBoothPromptModal')
    initializeEventDetails();
  };

  $scope.queueBoothSubmit = function() {
    if(_.isEmpty($scope.modalBoothProfile.name) || _.isEmpty($scope.modalBoothProfile.type) || _.isNull($scope.modalBoothProfile.maxTransactionsPerGuest) || $scope.modalBoothProfile.maxTransactionsPerGuest < 0 ){
      $scope.showBoothFieldsError = true;
    }
    else{
      $scope.submittedBooths.push({
        name: $scope.modalBoothProfile.name,        
        type: $scope.modalBoothProfile.type,
        boothHash : Date.now(),
        maxTransactionsPerGuest: $scope.modalBoothProfile.maxTransactionsPerGuest,
        removeBooth: function(){
          for(var i = 0; i < $scope.submittedBooths.length; i++){
            if($scope.submittedBooths[i].boothHash === this.boothHash){
              $scope.submittedBooths.splice(i,1);
              break;
            }
          }
        }
      });
      $scope.showBoothFieldsError = false;
      $scope.modalBoothProfile = {};
      $scope.disableSubmitAllBooths = false;
    }
  };

  $scope.submitAllBooths = function() {
    var length = $scope.submittedBooths.length - 1;
    submitBooth($scope.eventIDsubmitted, $scope.submittedBooths, length);
  };

  $scope.showEditBoothDetailsModal = function(boothDetails){
    console.log(boothDetails);
    $scope.boothToEdit.name = boothDetails.name;
    $scope.boothToEdit.type = boothDetails.type;
    $scope.boothToEdit.id = boothDetails.id;
    $scope.boothToEdit.maxTransactionsPerGuest = boothDetails.maxTransactionsPerGuest;

    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };

    $("#editboothdetails").modal(options);
  };

  $scope.submitEditBoothDetails = function(boothDetails){
    if(_.isEmpty(boothDetails.name) || _.isEmpty(boothDetails.type) || _.isNull(boothDetails.maxTransactionsPerGuest) || boothDetails.maxTransactionsPerGuest < 0 ){
      $scope.showBoothFieldsError = true;
    }
    else{
      $scope.showBoothFieldsError = false;

      updateBoothInfo(boothDetails, function(err){
        if(err){
           Toastr.show('warning', 'Failed to edit booth details!', 'Error!');
        }
        else{
          Toastr.show('success', 'Successfully editted booth!', 'Success!');
          $("#editboothdetails").modal('hide');

          //probably get booth details again to reflect changes.
          var _i = _.findIndex($scope.alleventBooths, function(_booth){
            return _booth.id === boothDetails.id;
          });

          if( _i !== -1){
            $scope.alleventBooths[_i].name = boothDetails.name;
            $scope.alleventBooths[_i].maxTransactionsPerGuest = boothDetails.maxTransactionsPerGuest;
          }
        }
      });
    }
  };

  $scope.cancelEditBoothDetails = function(){
    $("#editboothdetails").modal('hide');
  };


  //------- FORM SUBMIT FUNCTIONS -------//  
  function submitBooth(eventId, boothsArray, index){
    var element = boothsArray[index];

    element.eventID = eventId;
    element.claimDataIDs = [];
    element.creditDataIDs = [];

    $http({
      method: 'POST',
      url: '/addBooth',
      data: {newBooth: element}
    }).success(function(data, status, headers, config) {
      if (data.status == "OK") {
        index = index - 1;

        if (index < 0) {
          $("#boothprofile").modal("hide");
          $scope.modalBoothProfile = {};
          $scope.submittedBooths = [];
          $scope.disableSubmitAllBooths = true;

          Toastr.show('success', 'Successfully added new booths!', 'Success!');

          //check if adding of booth is from a newly created event. if so, get latest event
          if($scope.createdNewEvent){
            $scope.createdNewEvent = false;
            initializeEventDetails();
          }          
          
        }
        else submitBooth(eventId, boothsArray, index);
      }
      else {
        //error occured
        Toastr.show('warning', 'Failed to add additional booths', 'Error Occurred!');
        console.log(data);
      }
    });
  }

  function validateEventInfoForm(eventInfo, callback){
    if(!moment(eventInfo.date).isValid()){
      callback('Please specify a valid date'); 
    }
    else if(eventInfo.guestEstimate < 0){
      callback('Please specify a valid estimated number of guests');
    }
    else{
      callback();
    }
  }

  function updateUserInfo(userInfo, callback){
   $http({
      method: 'POST',
      url: '/updateUserDetails',
      data: {id: userInfo.id, firstName: userInfo.firstName, lastName: userInfo.lastName, email: userInfo.email, mobileNo: userInfo.mobileNo},
    }).success(function(data) {
      console.log(data);
      callback();
    }).error(function(data){
      console.log(data);
      callback("Error");
    });
  }

  function updateEventInfo(eventInfo, callback){
    $http({
      method: 'POST',
      url: '/updateEventDetails',
      data: {_id: eventInfo._id, id: eventInfo.id, date: eventInfo.date, name: eventInfo.name, location: eventInfo.location, guestEstimate: eventInfo.guestEstimate},
    }).success(function(){
      callback(null);
    }).error(function(data){
      console.log(data);
      callback("Error");
    });
  }

  function updateBoothInfo(boothInfo, callback){
    $http({
      method: 'POST',
      url: '/updateEventBoothDetails',
      data: { id: boothInfo.id, name: boothInfo.name, maxTransactionsPerGuest: boothInfo.maxTransactionsPerGuest},
    }).success(function(data) {
      console.log(data);
      callback();
    }).error(function(data){
      console.log(data);
      callback("Error");
    }); 
  }
  

  //------- AUX FUNCTIONS -------//
  $("#datemask").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});
    $("[data-mask]").inputmask();

    $("#logoInput").change(function () {
      if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $scope.modalEventProfile.logoImage = e.target.result;
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

  $("#bgInput").change(function () {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $scope.modalEventProfile.backgroundImage = e.target.result;
      }
      reader.readAsDataURL(this.files[0]);
    }
  });

});
