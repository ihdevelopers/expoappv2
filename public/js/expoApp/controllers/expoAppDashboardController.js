expoApp.controller('mainController', function($scope, $http, Toastr) {
  $scope.$on('$viewContentLoaded', onReady);

  var colors = Highcharts.getOptions().colors;

  function getAllPresentGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.isPresent == true) finalList.push(guest);
    });
    return finalList;
  }

  function onReady() {
    var hasOwnProperty = Object.prototype.hasOwnProperty;

    var generateGraphs = function() {
      $scope.totalRegistered = $scope.totalPreregistered + $scope.totalWalkins;
      console.log($scope.allEventCheckinTransactions)
      generateDataForRegistrationHistogramGraph($scope.allEventGuests);
      generateDataForPreRegistrationHistogramGraph($scope.allEventGuests, $scope.chooseEvent.date);
      generateDataForCheckinBoothsPieGraph($scope.allEventCheckinTransactions);
      generateDataForClaimBoothsHistogramGraph($scope.allEventClaimTransactions);
    }
  
    var computeDashboardData = function() {
      console.log('computeDashboardData for $scope.allEventCheckinTransactions: ')
      console.log($scope.allEventCheckinTransactions)
      console.log($scope.chooseThisEventDay)

      $scope.totalPresent = 0;
      $scope.totalnotyetRegistered = 0;
      $scope.totalPreregistered = 0;
      $scope.totalWalkins = 0;
      $scope.totalAnonymousGuests = 0;
      $scope.totalEngagements = 0;
      $scope.totalBoothCheckins = 0;
      $scope.totalBoothClaims = 0;
      var apiCounter = 0;

      if($scope.chooseThisEventDay == undefined || $scope.chooseThisEventDay == null) {
        drawRegistrationHistogramGraph([], [], [], [], true);
        drawPreRegistrationHistogramGraph([], [], true);
        drawBoothsVisitedPieGraph([], true);
        drawClaimTransactionsHistogram([], [], true);
      }
      else {
        $http({
          method: 'GET',
          url: '/getPresentGuestsCount',
          params: {eventID: $scope.chooseEvent.id, dateString: $scope.chooseThisEventDay.day}
        }).success(function(getPresentGuestsCount_response, status) {
          if(getPresentGuestsCount_response.status == "OK") {
            $scope.totalPresent = getPresentGuestsCount_response.data;
            console.log('OK on /getPresentGuestsCount');
            apiCounter++;
            if(apiCounter == 7) {
              generateGraphs();
            }
          }
          else{
            Toastr.show('error', JSON.stringify(getPresentGuestsCount_response), 'Error')
          }
        }).error(function(getPresentGuestsCount_response, status){
          Toastr.show('error', 'Unable to get present guests count.', 'Error');
          console.log(JSON.stringify(getPresentGuestsCount_response, null, '    '));
        })

        $http({
          method: 'GET',
          url: '/getPreRegisteredGuestsCount',
          params: {eventID: $scope.chooseEvent.id, dateString: $scope.chooseThisEventDay.day}
        }).success(function(getPreRegisteredGuestsCount_response, status) {
          if(getPreRegisteredGuestsCount_response.status == "OK") {
            $scope.totalPreregistered = getPreRegisteredGuestsCount_response.data;
            console.log('OK on /getPreRegisteredGuestsCount');
            apiCounter++;
            if(apiCounter == 7) {
              generateGraphs();
            }
          }
          else{
            Toastr.show('error', JSON.stringify(getPreRegisteredGuestsCount_response), 'Error')
          }
        }).error(function(getPreRegisteredGuestsCount_response, status) {
          Toastr.show('error', 'Unable to get preregistered guests count.', 'Error');
          console.log(JSON.string(getPreRegisteredGuestsCount_response, null, '    '));
        })

        $http({
          method: 'GET',
          url: '/getWalkinGuestsCount',
          params: {eventID: $scope.chooseEvent.id, dateString: $scope.chooseThisEventDay.day}
        }).success(function(getWalkinGuestsCount_response, status) {
          if(getWalkinGuestsCount_response.status == "OK") {
            $scope.totalWalkins = getWalkinGuestsCount_response.data;
            console.log('OK on /getWalkinGuestsCount');
            apiCounter++;
            if(apiCounter == 7) {
              generateGraphs();
            }
          }
          else{
            Toastr.show('error', JSON.stringify(getWalkinGuestsCount_response), 'Error')
          }
        }).error(function(getWalkinGuestsCount_response, status) {
          Toastr.show('error', 'Unable to get walkin guests count.', 'Error');
          console.log(JSON.string(getWalkinGuestsCount_response, null, '    '));
        })

        $http({
          method: 'GET',
          url: '/getAnonymousGuestsCount',
          params: {eventID: $scope.chooseEvent.id, dateString: $scope.chooseThisEventDay.day}
        }).success(function(getAnonymousGuestsCount_response, status) {
          if(getAnonymousGuestsCount_response.status == "OK") {
            $scope.totalAnonymousGuests = getAnonymousGuestsCount_response.data;
            console.log('OK on /getAnonymousGuestsCount');
            apiCounter++;
            if(apiCounter == 7) {
              generateGraphs();
            }
          }
          else{
            Toastr.show('error', JSON.stringify(getAnonymousGuestsCount_response), 'Error')
          }
        }).error(function(getAnonymousGuestsCount_response, status) {
          Toastr.show('error', 'Unable to get anonymous guests count.', 'Error');
          console.log(JSON.string(getAnonymousGuestsCount_response, null, '    '));
        })

        $http({
          method: 'GET',
          url: '/getEventEngagementsCount',
          params: {eventID: $scope.chooseEvent.id, dateString: $scope.chooseThisEventDay.day}
        }).success(function(getEventEngagementsCount_response, status) {
          if(getEventEngagementsCount_response.status == "OK") {
            $scope.totalEngagements = getEventEngagementsCount_response.data;
            console.log('OK on /getEventEngagementsCount');
            apiCounter++;
            if(apiCounter == 7) {
              generateGraphs();
            }
          }
          else{
            Toastr.show('error', JSON.stringify(getEventEngagementsCount_response), 'Error')
          }
        }).error(function(getEventEngagementsCount_response, status) {
          Toastr.show('error', 'Unable to get event engagements count.', 'Error');
          console.log(JSON.string(getEventEngagementsCount_response, null, '    '));
        })


        $http({
          method: 'GET',
          url: '/getEventCheckinsCount',
          params: {eventID: $scope.chooseEvent.id, dateString: $scope.chooseThisEventDay.day}
        }).success(function(getEventCheckinsCount_response, status) {
          if(getEventCheckinsCount_response.status == "OK") {
            $scope.totalBoothCheckins = getEventCheckinsCount_response.data;
            console.log('OK on /getEventCheckinsCount');
          }
          else {
            Toastr.show('error', JSON.stringify(getEventCheckinsCount_response) + '; CheckinCount', 'Error')
          }
          apiCounter++;
          if(apiCounter == 7) {
            generateGraphs();
          }
        }).error(function(getEventCheckinsCount_response, status) {
          Toastr.show('error', 'Unable to get event checkins count.', 'Error');
          console.log(JSON.string(getEventCheckinsCount_response, null, '    '));
        })

        $http({
          method: 'GET',
          url: '/getEventClaimsCount',
          params: {eventID: $scope.chooseEvent.id, dateString: $scope.chooseThisEventDay.day}
        }).success(function(getEventClaimsCount_response, status) {
          console.log("getEventClaimsCount_response: ")
          console.log(JSON.stringify(getEventClaimsCount_response))
          if(getEventClaimsCount_response.status == "OK") {
            $scope.totalBoothClaims = getEventClaimsCount_response.data;
            console.log('OK on /getEventClaimsCount')
          }
          else{
            Toastr.show('error', JSON.stringify(getEventClaimsCount_response) + '; claimCount', 'Error')
          }
          apiCounter++;
          if(apiCounter == 7) {
            generateGraphs();
          }
        }).error(function(getEventClaimsCount_response, status) {
          Toastr.show('error', 'Unable to get event claims count.', 'Error');
          console.log(JSON.string(getEventClaimsCount_response, null, '    '));
        })
      }
      

    }

    $scope.changedDate = function(selectedDate) {
      console.log('selectedDate: ' + selectedDate);

      computeDashboardData();
    }

    $scope.$watch('chooseEvent', function() {
      $scope.totalRegistered = 0;
      $scope.allEventGuests = [];
      $scope.allEventBooths = [];
      $scope.chooseEventDay = [];
      $scope.allEventCheckinTransactions = [];
      $scope.allEventClaimTransactions = [];
      $scope.chooseThisEventDay = [];
      var getCounter = 0;

      $http({
        method: 'GET',
        url: '/getAllEventGuests',
        params: {eventID: $scope.chooseEvent.id, limitedFieldsOnly: true}
      }).success(function(data, status, headers, config) {
        if (data.status == "OK") {
          $scope.allEventGuests = data.data;
          var allPresentGuests = getAllPresentGuests(data.data);
          var allUniqueDates = [];
          angular.forEach(allPresentGuests, function(eachGuest) {
            // eachGuest.timePresent = getRandomArbitrary(1430359200000, 1430683200000); // for testing purposes only (old db model, no timePresent field yet)
            eachGuest.timePresentString = moment(eachGuest.timePresent).format("MM/DD/YYYY");
          })
          allPresentGuests.sort(dynamicSort('timePresent'));
          allUniqueDates = _.uniq(_.pluck(allPresentGuests, "timePresentString"));
          console.log("allUniqueDates:")
          console.log(allUniqueDates)
          angular.forEach(allUniqueDates, function(eachDate){
            var _obj = new Object();
            _obj.day = eachDate;
            $scope.chooseEventDay.push(_obj);
          })
          if($scope.chooseEventDay.length > 1) {
            $scope.chooseEventDay.unshift({'day': 'All Days'}); // adds to beginning of array
          }
          $scope.chooseThisEventDay = _.first($scope.chooseEventDay);
          console.log($scope.chooseThisEventDay)
          console.log('OK on /getAllEventGuests');

          getCounter++;
          if(getCounter == 4) {
            computeDashboardData();
          }
        }
        else {
          console.log("STATUS ERROR on getAllEventGuests")
          // $(".userprofile").html("Try Again");
          // $("#userprofileheader").html("Error Message")
     
          // var options = {
          //   show: true,
          //   keyboard: false,
          //   backdrop: 'static'
          // }
          // $("#userprofile").modal(options);
          // $(".modal-body h3").html(data.message);
        }
      }).error(function(data, status) {
        Toastr.show('error', 'Unable to connect to server', 'Error!');
        console.log(data.data);
      })

      $http({
        method: 'GET',
        url: '/getAllEventBooths',
        params: {eventID: $scope.chooseEvent.id}
      }).success(function(getAllEventBooths_response, status, headers, config) {
        if (getAllEventBooths_response.status == "OK") {
          $scope.allEventBooths = getAllEventBooths_response.data;
          console.log('OK on /getAllEventBooths');
          getCounter++;
          if(getCounter == 4) {
            computeDashboardData();
          }
        }
        else{
          console.log("STATUS ERROR on getAllEventBooths");
        }
      }).error(function(getAllEventBooths_response, status, headers, config) {
        Toastr.show('error', 'Unable to get all event booths.', 'Error!');
        console.log(getAllEventBooths_response.data)
      })

      $http({
        method: 'GET',
        url: '/getEventCheckins',
        params: {eventID: $scope.chooseEvent.id}
      }).success(function(getEventCheckins_response, status) {
        if(getEventCheckins_response.status == "OK") {
          $scope.allEventCheckinTransactions = getEventCheckins_response.data; // [{timestamp, boothName}]
          console.log($scope.allEventCheckinTransactions)
          console.log('OK on /getEventCheckins');
          console.log($scope.allEventCheckinTransactions)
          getCounter++;
          if(getCounter == 4) {
            computeDashboardData();
          }
        }
        else{
          getCounter++;
          if(getCounter == 4) {
            computeDashboardData();
          }
        }
      }).error(function(getEventCheckins_response, status) {
        Toastr.show('error', 'Unable to get event checkins.', 'Error!');
        console.log(JSON.string(getEventCheckins_response, null, '    '));
      });

      $http({
        method: 'GET',
        url: '/getEventClaims',
        params: {eventID: $scope.chooseEvent.id}
      }).success(function(getEventClaims_response, status) {
        if(getEventClaims_response.status == "OK") {
          $scope.allEventClaimTransactions = getEventClaims_response.data;
          console.log('OK on /getEventClaims');
          getCounter++;
          if(getCounter == 4) {
            computeDashboardData();
          }
        }
        else{ // no eventClaims yet
          getCounter++;
          if(getCounter == 4) {
            computeDashboardData();
          }
        }
      }).error(function(getEventClaims_response, status) {
        Toastr.show('error', 'Unable to get event claims.', 'Error!');
        console.log(JSON.string(getEventClaims_response, null, '    '));
      })

    })


    // Make monochrome colors and set them as default for all pies
    Highcharts.getOptions().plotOptions.pie.colors = (function () {
      var pieColors = [],
        base = Highcharts.getOptions().colors[0],
        i;

      for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        pieColors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
      }
      return pieColors;
    }());
  }

  function getAllRegisteredGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.preRegistered == false) finalList.push(guest);
    });
    return finalList;
  }

  function getAllUnregisteredGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
     if (guest.isPresent == true) finalList.push(guest);
    });
    return finalList;
  }

  function getAllPreregisteredGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.preRegistered == true) finalList.push(guest);
    });
    return finalList;
  }

  function getAllWalkinGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.preRegistered == false) finalList.push(guest);
    });
    return finalList;
  }

  function getTotalEngagements(list, eventBooths) {
    var finalList = [];
    angular.forEach(list, function(transaction) {
      var result = (_.result(_.findWhere(eventBooths, { 'id': transaction.boothID }), 'type')) // check booth type of transaction from eventBooths
      if(result == "checkin"){
        finalList.push(transaction);
      }
    });
    return finalList;
  }

  function getMostvisitedBooths(list) {
    var finalList = [];
    angular.forEach(list, function(transaction) {
      finalList.push(transaction);
    });
    return finalList;
  }

  function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
  }

  function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
  }

  // -------------------------------------------------------------------------
  // FUNCTIONS FOR DATA GENERATION FOR EVENT-DAY REGISTRATION HISTOGRAM GRAPH
  // -------------------------------------------------------------------------
  function groupRegisteredGuestsByDate(allPresentGuests) {
    var groupedPresentGuests = _.chain( allPresentGuests ).reduce(function( memo, guestData ) {
      memo[ guestData.timePresentString ] = memo[ guestData.timePresentString ] || [];
      memo[ guestData.timePresentString ].push( guestData.details );
      return memo;
    }, {}).map(function( details, timePresentString ) {
      return {
        Details: details,
        TimePresentString: timePresentString
      }
    }).value();

    angular.forEach(groupedPresentGuests, function(eachGuest) {
      angular.forEach(eachGuest.Details, function(eachGuestDetail) {
        if((eachGuestDetail.details != undefined) && (eachGuestDetail.details != null)) {
          delete eachGuestDetail.details;
        }
      })
    })

    return(groupedPresentGuests)
  }

  function checkDateToProcess(processTheseGuests) {
    var dataToProcess = [];
    if($scope.chooseThisEventDay.day != "All Days") {
      angular.forEach(processTheseGuests, function(eachDate) {
        if(eachDate.TimePresentString == $scope.chooseThisEventDay.day) {
          dataToProcess = eachDate.Details;
        }
      })
    }
    else{
      angular.forEach(processTheseGuests, function(eachDate) {
        angular.forEach(eachDate.Details, function(eachDetail) {
          dataToProcess.push(eachDetail)
        })
      })
    }
    return dataToProcess;
  }

  function generateTimeIntervals(date, startHour, endHour) {
    var timeIntervalSeries = [];
    var timeString;

    if(startHour > endHour) {
      endHour = endHour + 12;
    }
    for(var i=startHour; i<=endHour; i++) {
      if(i<10) {
        timeString = "0" +i + ":00 - " + "0" + i + ":59";
      }
      else{
        timeString = i + ":00 - " + i + ":59";
      }
      var obj = new Object();
      obj.date = date;
      obj.intervalStart = i;
      obj.intervalEnd = i+1;
      obj.intervalString = timeString;
      timeIntervalSeries.push(obj)
    }
    return timeIntervalSeries;
  }

  function groupRegistrationsByTimeIntervalHour(data) {
    var groupedRegByTimeIntervalHour = _.chain( data ).reduce(function( memo, timeIntervalData ) {
      memo[ timeIntervalData.timeInterval ] = memo[ timeIntervalData.timeInterval ] || [];
      memo[ timeIntervalData.timeInterval ].push( timeIntervalData.data );
      return memo;
    }, {}).map(function( data, timeInterval) {
      return {
        TimeInterval: timeInterval,
        Data: data
      }
    }).value();

    return groupedRegByTimeIntervalHour;
  }

  function isEmpty(obj){
    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
  }

  function groupRegistrationsByTimeIntervalMinute(data, day) {
    angular.forEach(data, function(eachTimeInterval) {
      eachTimeIntervalStart = eachTimeInterval.TimeInterval.slice(0,5);
      eachTimeIntervalEnd = eachTimeInterval.TimeInterval.slice(8);
      eachTimeIntervalStartHour = eachTimeInterval.TimeInterval.slice(0,2);

      eachTimeInterval.date = day;
      var minuteIntervalArray = [];
      var minuteIntervalStringArray = [];

      var tempStartMinute = 9;
      for(var minuteCount=tempStartMinute; minuteCount<60; minuteCount+=10) {
        if(tempStartMinute == 9){
          var minuteIntervalString = eachTimeIntervalStart + "-" + eachTimeIntervalStartHour + ":0" + minuteCount;
          var object = new Object();
          object.MinuteIntervalStart = moment(day).hour(Number(eachTimeIntervalStartHour)).minute(0).second(0);
          object.MinuteIntervalEnd = moment(day).hour(Number(eachTimeIntervalStartHour)).minute(minuteCount).second(59);
        }
        else{
          var minuteIntervalString = eachTimeIntervalStartHour + ":" + tempStartMinute + "-" + eachTimeIntervalStartHour + ":" + minuteCount;
          var object = new Object();
          object.MinuteIntervalStart = moment(day).hour(Number(eachTimeIntervalStartHour)).minute(tempStartMinute).second(0);
          object.MinuteIntervalEnd = moment(day).hour(Number(eachTimeIntervalStartHour)).minute(minuteCount).second(59);
        }

        minuteIntervalArray.push(object);
        minuteIntervalStringArray.push(minuteIntervalString)
        tempStartMinute = minuteCount+1;
      }
      eachTimeInterval.MinuteIntervalString = minuteIntervalStringArray;
      eachTimeInterval.MinuteInterval = minuteIntervalArray;
    })

    angular.forEach(data, function(eachTimeInterval) {
      angular.forEach(eachTimeInterval.MinuteInterval, function(eachMinuteInterval, index) {
        var tempIntervalData = [];
        eachMinuteInterval.MinuteIntervalString = eachTimeInterval.MinuteIntervalString[index];
        angular.forEach(eachTimeInterval.Data, function(eachTimeIntervalData) {
          if(moment(eachTimeIntervalData.timePresent).minute() >= moment(eachMinuteInterval.MinuteIntervalStart).minute() && moment(eachTimeIntervalData.timePresent).minute() <= moment(eachMinuteInterval.MinuteIntervalEnd).minute()) {
            tempIntervalData.push(eachTimeIntervalData);
          }
          else{
            // do nothing
          }
        })
        eachMinuteInterval.IntervalData = tempIntervalData;
      })
    })

    return data;
  }

  function createRegistrationHistogramData(data, regType) {
    var colorCounter = 0;
    var consolidatedData = [];

    angular.forEach(data, function(eachTimeInterval, index) {
      var tempCategories = [];
      var tempData = [];

      var drilldownObject = new Object();
      drilldownObject.type = "column";
      drilldownObject.level = 1;
      drilldownObject.color = colors[index];
      angular.forEach(eachTimeInterval.MinuteInterval, function(eachMinuteInterval) {
        tempCategories.push(eachMinuteInterval.MinuteIntervalString);
        tempData.push(eachMinuteInterval.IntervalData.length);
      })
      drilldownObject.categories = tempCategories;
      drilldownObject.data = tempData;
      if(regType == 'prereg') {
        drilldownObject.regType = "Pre-Registered Guests";
        drilldownObject.name = "Pre-Registered Guests Breakdown";
      }
      else if(regType == 'walkin') {
        drilldownObject.regType = "Walk-In Guests";
        drilldownObject.name = "Walkin Registered Guests Breakdown";
      }

      var dataObject = new Object();
      if(eachTimeInterval.Data[0]._id == undefined || eachTimeInterval.Data[0]._id == null) {
        dataObject.y = 0;
      }
      else{
        dataObject.y = eachTimeInterval.Data.length;
      }
      dataObject.color = colors[index];
      dataObject.drilldown = drilldownObject;

      consolidatedData.push(dataObject)
    })
   
    return consolidatedData;
  }

  function generateDataForRegistrationHistogramGraph(allEventGuests) {
    var allPresentGuests = getAllPresentGuests(allEventGuests);
    var walkinGuests = [];
    var preRegisteredGuests = [];
    var firstRegisteredGuest;
    var lastRegisteredGuest;
    var firstRegisteredGuestDay, firstRegisteredGuestHour, firstRegisteredGuestMinute, lastRegisteredGuestDay, lastRegisteredGuestHour, lastRegisteredGuestMinute;
    var timeIntervals = [];
    var currentEndHour = (moment().get('hour'));
    var startHour, endHour;
    var groupedAllRegisteredGuestsByDate = [];
    var groupedPreRegisteredGuestsByDate = [];
    var groupedWalkinGuestsByDate = [];
    var processTheseGuests = [];
    var allUniqueDates = [];
    var groupedTimeIntervalsHourPreReg = [];
    var groupedTimeIntervalsHourWalkin = [];
    var groupedTimeIntervalsMinutePreReg = [];
    var groupedTimeIntervalsMinuteWalkin = [];
    var categories = [];
    var dataPreReg = [];
    var dataWalkin = [];
    var data = [];
    var series = [];
    var nameSeries = [];

    angular.forEach(allPresentGuests, function(eachGuest) {
      // eachGuest.timePresent = getRandomArbitrary(1430359200000, 1430683200000); // for testing purposes only (old db model, no timePresent field yet; uncomment this for testing)
      eachGuest.timePresentString = moment(eachGuest.timePresent).format("MM/DD/YYYY");
      eachGuest.details = eachGuest;
    });

    allUniqueDates = _.uniq(_.pluck(allPresentGuests, "timePresentString"));

    groupedAllRegisteredGuestsByDate = groupRegisteredGuestsByDate(allPresentGuests);

    processTheseGuests = checkDateToProcess(groupedAllRegisteredGuestsByDate);

    if($scope.chooseThisEventDay.day == "All Days") {
      angular.forEach(processTheseGuests, function(eachGuest) {
        eachGuest.hourPresent = moment(eachGuest.timePresent).hour();
      })
      processTheseGuests.sort(dynamicSort("hourPresent"));
    }
    else {
      processTheseGuests.sort(dynamicSort("timePresent"));  
    }

    firstRegisteredGuest = _.first(processTheseGuests);
    lastRegisteredGuest = _.last(processTheseGuests);

    if(firstRegisteredGuest != undefined && lastRegisteredGuest != undefined) {
      firstRegisteredGuestDay = moment(firstRegisteredGuest.timePresent);
      lastRegisteredGuestDay = moment(lastRegisteredGuest.timePresent);
    }
    else{
      firstRegisteredGuestDay = undefined;
      lastRegisteredGuestDay = undefined;
    }

    if(firstRegisteredGuestDay != undefined && lastRegisteredGuestDay != undefined){
      firstRegisteredGuestHour = firstRegisteredGuestDay.get('hour');
      firstRegisteredGuestMinute = firstRegisteredGuestDay.get('minute');
      startHour = firstRegisteredGuestHour;
      lastRegisteredGuestHour = lastRegisteredGuestDay.get('hour');

      if(lastRegisteredGuestHour >= currentEndHour) {
        endHour = lastRegisteredGuestHour;
      }
      else{
        endHour = currentEndHour;
      }

      timeIntervals = generateTimeIntervals(firstRegisteredGuestDay, startHour, endHour);

      // form 'categories' (time intervals array) for graph
      angular.forEach(timeIntervals, function(eachTimeInterval) {
        categories.push(eachTimeInterval.intervalString);
      })

      //get data for each timeInterval
      var timeIntervalDataPreReg = [];
      var timeIntervalDataWalkin = [];
      angular.forEach(timeIntervals, function(eachInterval) {
        var countPreRegistered = 0;
        var countWalkin = 0;
        angular.forEach(processTheseGuests, function(eachPresentGuest) {
          var guestTimePresent = moment(eachPresentGuest.timePresent).get('hour');
          var intervalDataObj = new Object();
          intervalDataObj.timeInterval = eachInterval.intervalString;
          intervalDataObj.data = eachPresentGuest;
          if((guestTimePresent == eachInterval.intervalStart) && eachPresentGuest.preRegistered == true) {
            timeIntervalDataPreReg.push(intervalDataObj);
            countPreRegistered++;
          }
          else if((guestTimePresent == eachInterval.intervalStart) && eachPresentGuest.preRegistered == false) {
            timeIntervalDataWalkin.push(intervalDataObj);
            countWalkin++;
          }
        })
        if(countPreRegistered == 0) {
          timeIntervalDataPreReg.push({"timeInterval": eachInterval.intervalString, "data": {}});
        }
        if(countWalkin == 0) {
          timeIntervalDataWalkin.push({"timeInterval": eachInterval.intervalString, "data": {}});
        }
      })

      groupedTimeIntervalsHourPreReg = groupRegistrationsByTimeIntervalHour(timeIntervalDataPreReg);
      groupedTimeIntervalsHourWalkin = groupRegistrationsByTimeIntervalHour(timeIntervalDataWalkin);
      groupedTimeIntervalsMinutePreReg = groupRegistrationsByTimeIntervalMinute(groupedTimeIntervalsHourPreReg, firstRegisteredGuestDay);
      groupedTimeIntervalsMinuteWalkin = groupRegistrationsByTimeIntervalMinute(groupedTimeIntervalsHourWalkin, firstRegisteredGuestDay);

      // create data for preregd
      dataPreReg = createRegistrationHistogramData(groupedTimeIntervalsMinutePreReg, 'prereg');
      dataWalkin = createRegistrationHistogramData(groupedTimeIntervalsMinuteWalkin, 'walkin');

      data = [dataPreReg, dataWalkin];

      // assign same colors per stack row
      angular.forEach(data, function(eachData, index1) {
        angular.forEach(eachData, function(eachDataDrill, index2) {
          data[index1][index2].color = colors[index1];
          data[index1][index2].drilldown.color = colors[index1];
        })
      })

      angular.forEach(data, function(eachData) {
        var seriesObject = new Object();
        seriesObject.name = eachData[0].drilldown.regType;
        seriesObject.color = eachData[0].color;
        seriesObject.level = 0;
        seriesObject.data = eachData;
        series.push(seriesObject);
      })

      angular.forEach(series, function(eachSeries) {
        nameSeries.push(eachSeries.name)
      })

      drawRegistrationHistogramGraph(data, categories, series, nameSeries, false);
    }
    else{ // call histogram draw with empty data
      drawRegistrationHistogramGraph(data, categories, series, nameSeries, true);
    }
  }

  // ------------------------------------------------------------------
  // FUNCTION FOR DRAWING EVENT-DAY REGISTRATION HISTOGRAM GRAPH
  // ------------------------------------------------------------------
  function drawRegistrationHistogramGraph(dataObjectArray, categories, series, nameSeries, emptyData) {
    var chart;
    var categories = categories,
        name  = nameSeries,
        dataArray = [];
        level = 0;

        for(var i=0; i<dataObjectArray.length; i++){
          dataArray[i] = dataObjectArray[i];
        }

     function setChart(name, categories, data, color, level, type) {
          chart.xAxis[0].setCategories(categories);
          var dataLen = data.length;

          while(chart.series.length > 0) {
              chart.series[0].remove();                
          }
          for (var i = 0; i < dataLen; i++) {
              chart.addSeries({
                  type: type,
                  name: name,
                  data: data[i],
                  level: level,
                  color: color || 'white'
              });
          }
      }

      function setChart2(name, categories, data, color, level, type) {
          chart.xAxis[0].setCategories(categories);
          var dataLen = data.length;

          while(chart.series.length > 0) {
              chart.series[0].remove();                
          }
          for (var i = 0; i < dataLen; i++) {
              var thisColor = data[i][0].color;
              chart.addSeries({
                  type: type,
                  name: name[i],
                  data: data[i],
                  level: level,
                  color: thisColor || 'white'
              });
          }
      }
     
    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container',
            type: 'column',
            zoomType: 'xy'
        },
        title: {
            text: 'Average Hourly Count of Incoming Registered Guests'
        },
        subtitle: {
            text: 'Click the columns to view registration breakdown.<br/>Select and drag graph portion to zoom.'
        },
        xAxis: [
            { categories: categories },
            { labels: {rotation: -45} }
        ],
        yAxis: [{ // Primary yAxis
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Number of Guests',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }],
        legend: {
            enabled: true
        },
        plotOptions: {
            column: {
              stacking: 'normal',
              cursor: 'pointer',
              point: {
                 events: {
                    click: function() {

                       var drilldown = this.drilldown;

                       if (drilldown) { // drill down
                           this.series.chart.setTitle({
                               text: drilldown.name
                           });
                           setChart(drilldown.name, drilldown.categories, [drilldown.data], drilldown.color, drilldown.level, drilldown.type);

                       } else { // restore
                          this.series.chart.setTitle({
                            text: 'Registration Breakdown'
                          })
                          setChart2(name, categories, dataArray, this.color, level, 'column');
                       }
                    }
                 }
              },
              dataLabels: {
                  enabled: true,
                  // color: colors[0],
                  style: {
                      fontWeight: 'bold'
                  },
                  formatter: function () {
                      var point = this.point;

                      switch (this.series.options.level){
                          case 0:
                              if(point.y > 0)
                                  return point.y;
                          case 1:
                              if(point.y > 0)
                                  return point.y;
                      }
                      
                  }
              }              
            }
        },
        tooltip: {
            formatter: function () {
                var point = this.point,
                    s = '';

                switch (this.series.options.level) {
                    case 0:
                        s = 'Reg Type: <b>' + point.drilldown.regType + '</b><br/>Total Attendees: <b>' + point.y;
                        break;

                    case 1:
                        s = point.series.name + '<br/>Total Attendees: <b>' + point.y;
                        break;
                }
                return s;
            }
        },
        series: series,      
        exporting: {
           enabled: true
        }
    })

    if(emptyData == true && dataObjectArray.length == 0) {
      var title = chart.renderer.text('No data available yet.', 350, 290).css({
        fontSize: '30px',
        color: Highcharts.getOptions().colors[1]
      })
      .add();
    }
  }

  // ------------------------------------------------------------------
  // FUNCTIONS FOR DATA GENERATION FOR PREREGISTRATION HISTOGRAM GRAPH
  // ------------------------------------------------------------------
  function getAllUniquePreRegistrationDates(list) {
    var timePreRegisteredArray = [];
    angular.forEach(list, function(guest) {
      timePreRegisteredArray.push(moment(guest.timeRegistered).format("MM/DD/YYYY"));
    })
    timePreRegisteredArray = _.uniq(timePreRegisteredArray);
    return timePreRegisteredArray;
  }

  function getCompleteDates(allUniqueDates, eventDate) {
    if(allUniqueDates.length > 0) {
      var preRegistrationStartDate = moment((_.first(allUniqueDates)), "MM-DD-YYYY")
      var preRegistrationEndDate = moment(eventDate/*, "MM-DD-YYYY"*/);
      var currentDate = preRegistrationStartDate;
      var dateArray = []; // will contain daily local string dates from first day of kit distribution

      // generate an array of all days between date or 1st preRegistered guest and eventDate
      while(currentDate < preRegistrationEndDate) {
        dateArray.push(moment(currentDate).format("MM/DD/YYYY"));
        currentDate = moment(currentDate).add(1, 'days');
      }
    }

    return dateArray
  }

  function groupPreRegisteredGuestsByDate(allEventGuests, dailyDates) {
    var eventGuestObjArray = [];
    var groupedPreRegisteredGuestsByDate = [];
    angular.forEach(allEventGuests, function(eachEventGuest) {
      var guest = new Object();
      guest.Details = eachEventGuest;
      guest.PreRegDate = moment(eachEventGuest.timeRegistered).format("MM/DD/YYYY");
      eventGuestObjArray.push(guest);
    })

    groupedPreRegisteredGuestsByDate = _.chain( eventGuestObjArray ).reduce(function( memo, dateData ) {
      memo[ dateData.PreRegDate ] = memo[ dateData.PreRegDate ] || [];
      memo[ dateData.PreRegDate ].push( dateData.Details );
      return memo;
    }, {}).map(function( Details, PreRegDate ) {
      return {
        PreRegDate: PreRegDate,
        Details: Details
      }
    }).value();

    return(groupedPreRegisteredGuestsByDate);
  }

  function completeDateDetails(preRegGuestListByDate, dailyPreRegDates) {
    var availableRegDateArr = _.pluck(preRegGuestListByDate, "PreRegDate");
    angular.forEach(dailyPreRegDates, function(eachDate, index) {
      var _res = _.find(availableRegDateArr, function(date) {
        return date == eachDate;
      })
      if(_res == undefined) { // insert object on index when date is not present preRegGuestListByDate
        preRegGuestListByDate.splice(index, 0, {"PreRegDate": eachDate, "Details": []});
      }
    })
    return preRegGuestListByDate;
  }

  function createPreRegistrationHistogramData(preRegGuestListByDate) {
    var colors = Highcharts.getOptions().colors;
    var consolidatedData = [];

    angular.forEach(preRegGuestListByDate, function(eachPreRegGuestListByDate) {
      // var tempCategories = _.pluck(preRegGuestListByDate, 'PreRegDate');
      var dataObject = new Object();
      dataObject.date = eachPreRegGuestListByDate.PreRegDate;
      dataObject.y = eachPreRegGuestListByDate.Details.length;
      consolidatedData.push(dataObject);
    })

    return consolidatedData;
  }

  function createSeries(data) {
    var categories = _.pluck(data, "date");
    var series = [];
    var seriesObject = new Object();
    seriesObject.name = "Pre-Registration History";
    seriesObject.type = "column";
    seriesObject.data = data;
    series.push(seriesObject);
    return [categories, series];
  }

  function generateDataForPreRegistrationHistogramGraph(allEventGuests, eventDate) {
    var allPreRegisteredGuests = getAllPreregisteredGuests(allEventGuests);
    var allUniquePreRegistrationDates = [];
    var allPreRegistrationDailyDates = [];
    var groupedPreRegisteredGuestsByDate = [];
    // var completPreRegisteredGuestsByDate
    var dataPreRegistration = [];
    var seriesArray = [];
    var categories = [];
    var series = [];

    // non-empty pre-registered guests
    if(allPreRegisteredGuests.length > 0) {
      allPreRegisteredGuests.sort(dynamicSort("timeRegistered"));
      allUniquePreRegistrationDates = getAllUniquePreRegistrationDates(allPreRegisteredGuests);
      allPreRegistrationDailyDates = getCompleteDates(allUniquePreRegistrationDates, eventDate); // generate 'preReg days' until 1 day before event
      groupedPreRegisteredGuestsByDate = groupPreRegisteredGuestsByDate(allPreRegisteredGuests, allPreRegistrationDailyDates);
      groupedPreRegisteredGuestsByDate = completeDateDetails(groupedPreRegisteredGuestsByDate, allPreRegistrationDailyDates);

      // create data for preregistration histogram graph
      dataPreRegistration = createPreRegistrationHistogramData(groupedPreRegisteredGuestsByDate);
      seriesArray = createSeries(dataPreRegistration);

      categories = _.first(seriesArray);
      series = _.last(seriesArray);

      drawPreRegistrationHistogramGraph(categories, series, false);
    }
    else { // no pre-registered guests
      drawPreRegistrationHistogramGraph(categories, series, true);
    }
  }

  // -------------------------------------------------------
  // FUNCTION FOR DRAWING PRE-REGISTRATION HISTOGRAM GRAPH
  // -------------------------------------------------------
  function drawPreRegistrationHistogramGraph(categories, series, emptyData) {
    var chart2;
    chart2 = new Highcharts.Chart({
      chart: {
            renderTo: 'container2',
            type: 'column',
            zoomType: 'xy',
            width: 1000
        },
        title: {
            text: 'Count of Registered Guests During Pre-Registration Period'
        },
        subtitle: {
            text: 'Select and drag graph portion to zoom.'
        },
        xAxis: [{
            categories: categories
        }, {labels: {rotation: -45}}],
        yAxis: [{ // Primary yAxis
            labels: {
                // format: '{value}°C',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Number of Guests',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }],
        tooltip: {
            headerFormat: '<span style="font-size:10px">Date: <b>{point.key}</b></span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y: .0f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        legend: {
            enabled: true
        },
        plotOptions: {
          column: {
            stacking: 'normal',
            cursor: 'pointer',
            dataLabels: {
              enabled: true,
              style: {
                fontWeight: 'bold'
              },
              formatter: function() {
                var point = this.point;
                if(point.y > 0)
                  return point.y;
              }
            }
          }
        },
        series: series
    })

    if(emptyData == true && series.length == 0) {
      var title = chart2.renderer.text('No data available yet.', 350, 290).css({
        fontSize: '30px',
        color: Highcharts.getOptions().colors[1]
      })
      .add();
    }
  }


  // -------------------------------------------------------------------------
  // FUNCTIONS FOR DATA GENERATION FOR EVENT-DAY BOOTH CHECKINS PIE GRAPH
  // -------------------------------------------------------------------------
  function groupCheckinTransByDate(allCheckinTrans) {
    var groupedCheckinTransByDate = _.chain( allCheckinTrans ).reduce(function( memo, checkinData ) {
      memo[ checkinData.timeCheckinString ] = memo[ checkinData.timeCheckinString ] || [];
      memo[ checkinData.timeCheckinString ].push( checkinData.details );
      return memo;
    }, {}).map(function( details, timeCheckinString ) {
      return {
        Details: details,
        TimeCheckinString: timeCheckinString
      }
    }).value();

    angular.forEach(groupedCheckinTransByDate, function(eachTrans) {
      angular.forEach(eachTrans.Details, function(eachTransDetail) {
        if((eachTransDetail.details != undefined) && (eachTransDetail.details != null)) {
          delete eachTransDetail.details;
        }
      })
    })

    return groupedCheckinTransByDate;
  }

  function checkTransDateToProcess(processTheseTransactions) {
    var dataToProcess = [];

    if($scope.chooseThisEventDay.day != "All Days") {
      angular.forEach(processTheseTransactions, function(eachDate) {
        if(eachDate.TimeCheckinString == $scope.chooseThisEventDay.day) {
          dataToProcess = eachDate.Details;
        }
      })
    }
    else{
      angular.forEach(processTheseTransactions, function(eachDate) {
        angular.forEach(eachDate.Details, function(eachDetail) {
          dataToProcess.push(eachDetail)
        })
      })
    }
    return dataToProcess;
  }

  function groupCheckinTransByBoothID(allCheckinTrans) {
    var groupedCheckinTransByBooth = _.chain( allCheckinTrans ).reduce(function( memo,checkinData ) {
      memo[ checkinData.boothID ] = memo[ checkinData.boothID ] || [];
      memo[ checkinData.boothID ].push( checkinData.timestamp );
      return memo;
    }, {}).map(function( timestamp, boothID ) {
      return {
        BoothID: boothID,
        TimeStamp: timestamp
      }
    }).value();

    // count number of checkins per booth;
    angular.forEach(groupedCheckinTransByBooth, function(eachCheckin) {
      eachCheckin.CheckinCount = eachCheckin.TimeStamp.length;
    })

    groupedCheckinTransByBooth.sort(dynamicSort("-CheckinCount"));

    return groupedCheckinTransByBooth;
  }

  function groupCheckinTransByBoothName(groupedCheckinTransByBoothID) {
    angular.forEach(groupedCheckinTransByBoothID, function(eachTrans) {
      eachTrans.BoothID = parseInt(eachTrans.BoothID);
      eachTrans.BoothName = _.result(_.find($scope.allEventBooths, {'id': eachTrans.BoothID}), 'name');
    })

    groupedCheckinTransByBoothID.sort(dynamicSort('-CheckinCount'));

    return groupedCheckinTransByBoothID;
  }

  function createBoothCheckinPieGraphData(groupedCheckinTransByBooth) {
    var data = [];
    angular.forEach(groupedCheckinTransByBooth, function(eachGroup, index) {
      var _data = new Object();
      _data.name = eachGroup.BoothName;
      _data.y = eachGroup.CheckinCount;
      if(index == 0) {
        _data.sliced = true;
        _data.selected = true;
      }
      data.push(_data);
    })

    return data;
  }

  function generateDataForCheckinBoothsPieGraph(allCheckinTransactions) {
    var processTheseTransactions = [];
    var groupedCheckinTransByDate = [];
    var groupedCheckinTransByBoothID = [];
    var groupedCheckinTransByBoothName = [];
    var data = [];
    var series = [];
    var sampleData = [
      { 'boothID': 1, 'timestamp': 1426103110 },
      { 'boothID': 3, 'timestamp': 1426103110 },
      { 'boothID': 1, 'timestamp': 1426103110 },
      { 'boothID': 1, 'timestamp': 1426103110 },
      { 'boothID': 2, 'timestamp': 1426103110 },
      { 'boothID': 3, 'timestamp': 1426103110 },
      { 'boothID': 3, 'timestamp': 1426103110 },
      { 'boothID': 2, 'timestamp': 1426103110 },
      { 'boothID': 1, 'timestamp': 1426103110 },
      { 'boothID': 2, 'timestamp': 1426103110 }
    ];


    angular.forEach(allCheckinTransactions, function(eachTrans) {
      // eachTrans.timestamp = getRandomArbitrary(1430359200000, 1430683200000); // for testing purposes only (generate timestamp same as date range with guests registration; uncomment this for testing)
      eachTrans.timeCheckinString = moment(eachTrans.timestamp).format("MM/DD/YYYY");
      eachTrans.details = eachTrans;
    })
    // allCheckinTransactions.sort(dynamicSort('timestamp'));

    groupedCheckinTransByDate = groupCheckinTransByDate(allCheckinTransactions);
    processTheseTransactions = checkTransDateToProcess(groupedCheckinTransByDate);

    processTheseTransactions.sort(dynamicSort('timestamp'));

    groupedCheckinTransByBoothID = groupCheckinTransByBoothID(processTheseTransactions);

    if(groupedCheckinTransByBoothID.length > 0) {      
      groupedCheckinTransByBoothName = groupCheckinTransByBoothName(groupedCheckinTransByBoothID);
      data = createBoothCheckinPieGraphData(groupedCheckinTransByBoothName);
      series = [{
        type: 'pie',
        name: 'Booth Checkins Breakdown',
        data: data
      }]

      drawBoothsVisitedPieGraph(series, false);
    }
    else{ // empty allCheckinTransactions
      drawBoothsVisitedPieGraph(series, true);
    }
  }

  // ----------------------------------------------------------
  // FUNCTION FOR DRAWING EVENT-DAY BOOTH CHECKINS PIE GRAPH
  // ----------------------------------------------------------
  function drawBoothsVisitedPieGraph(series, emptyData) {
    // Build the chart
    var chart3;
    chart3 = new Highcharts.Chart({
      chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            renderTo: 'container3'
        },
        title: {
            text: 'Real-Time Summary of Checkin Booth Visits'
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">Booth: <b>{point.key}</b></span><br/>',
            pointFormat: 'Number of Checkins: <b>{point.y} ({point.percentage:.1f}%)</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ({point.percentage:.1f}) %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: series
    })
  
    if(emptyData == true && series.length == 0) {
      var title = chart3.renderer.text('No data available yet.', 350, 290).css({
        fontSize: '30px',
        color: Highcharts.getOptions().colors[1]
      })
      .add();
    }
  }

  // -------------------------------------------------------------------------
  // FUNCTIONS FOR DATA GENERATION FOR EVENT-DAY BOOTH CLAIM HISTOGRAM GRAPH
  // -------------------------------------------------------------------------
  function groupAllClaimTransByDate(allClaimTransactions) {
    var groupedClaimTrans = _.chain( allClaimTransactions ).reduce(function( memo, claimTransData ) {
      memo[ claimTransData.timeClaimString ] = memo[ claimTransData.timeClaimString ] || [];
      memo[ claimTransData.timeClaimString ].push( claimTransData.details );
      return memo;
    }, {}).map(function( details, timeClaimString ) {
      return {
        Details: details,
        TimeClaimString: timeClaimString
      }
    }).value();

    angular.forEach(groupedClaimTrans, function(eachTrans) {
      angular.forEach(eachTrans.Details, function(eachTransDetail) {
        delete eachTransDetail.details;
      })
    })

    return groupedClaimTrans;
  }

  function checkClaimDateToProcess(processTheseTransactions) {
    var dataToProcess = [];

    if($scope.chooseThisEventDay.day != "All Days") {
      angular.forEach(processTheseTransactions, function(eachDate) {
        if(eachDate.TimeClaimString == $scope.chooseThisEventDay.day) {
          dataToProcess = eachDate.Details;
        }
      })
    }
    else{
      angular.forEach(processTheseTransactions, function(eachDate) {
        angular.forEach(eachDate.Details, function(eachDetail) {
          dataToProcess.push(eachDetail)
        })
      })
    }
    return dataToProcess;
  }

  function createClainTransHistogramData(groupedClaimsByTimeInterval) {
    var data = [];
    angular.forEach(groupedClaimsByTimeInterval, function(eachGroup) {
      var _obj = new Object();
      _obj.name = eachGroup.TimeInterval;
      var first_id = (_.first(eachGroup.Data)).timestamp;
      if(first_id == undefined || first_id == null) {
        _obj.y = 0;
      }
      else {
        _obj.y = eachGroup.Data.length;
      }
      data.push(_obj);
    })

    return data;
  }

  function generateDataForClaimBoothsHistogramGraph(allClaimTransactions) {
    var sampleData = [
      {'timestamp': 1426103110000},
      {'timestamp': 1426120500000},
      {'timestamp': 1426116900000},
      {'timestamp': 1426113300000},
      {'timestamp': 1426109700000},
      {'timestamp': 1426106100000},
      {'timestamp': 1426095300000},
      {'timestamp': 1426097472000},
      {'timestamp': 1426103110000},
      {'timestamp': 1426103110000}
    ];
    var groupedAllClaimTransByDate = [];
    var processTheseTransactions = [];
    var firstClaim, lastClaim, firstClaimTrans, lastClaimTrans, firstClaimTransHour, lastClaimTransHour, startHour, endHour;
    var currentEndHour = moment().get('hour');
    var timeIntervals = [];
    var timeIntervalClaims = [];
    var groupedTimeIntervalsHourClaims = [];
    var categories = [];
    var data = [];
    var series = [];

    // for testing puroposes only
    /* angular.forEach(sampleData, function(eachData) {
      eachData.timestamp = new Date(eachData.timestamp)
    })
    var allClaimTransactions = sampleData; */
    // end of testing purposes

    angular.forEach(allClaimTransactions, function(eachClaim) {
      // eachClaim.timestamp = getRandomArbitrary(1430359200000, 1430683200000); // for testing purposes only (generate time claim within date of registrations; uncomment this for testing);
      eachClaim.timeClaimString = moment(eachClaim.timestamp).format("MM/DD/YYYY");
      eachClaim.details = eachClaim;
    });

    allClaimTransactions.sort(dynamicSort('timestamp'));
    groupedAllClaimTransByDate = groupAllClaimTransByDate(allClaimTransactions);
    processTheseTransactions = checkClaimDateToProcess(groupedAllClaimTransByDate)

    firstClaim = _.first(processTheseTransactions);
    lastClaim = _.last(processTheseTransactions);

    if(firstClaim != undefined && lastClaim != undefined) {
      firstClaimTrans = moment(firstClaim.timestamp);
      lastClaimTrans = moment(lastClaim.timestamp);
    }
    else {
      firstClaimTrans = undefined;
      lastClaimTrans = undefined;
    }

    if(firstClaimTrans != undefined && lastClaimTrans != undefined) {
      firstClaimTransHour = firstClaimTrans.get('hour');
      startHour = firstClaimTransHour;
      lastClaimTransHour = lastClaimTrans.get('hour');

      if(lastClaimTransHour >= currentEndHour) {
        endHour = lastClaimTransHour;
      }
      else{
        endHour = currentEndHour;
      }
      timeIntervals = generateTimeIntervals(firstClaimTrans, startHour, endHour);

      angular.forEach(timeIntervals, function(eachTimeInterval) {
        categories.push(eachTimeInterval.intervalString);
      })

      // get data for each timeInterval
      angular.forEach(timeIntervals, function(eachInterval) {
        var counter = 0;
        angular.forEach(processTheseTransactions, function(eachClaimTrans) {
          var claimTransTimestamp = moment(eachClaimTrans.timestamp).get('hour');
          var intervalDataObj = new Object();
          intervalDataObj.timeInterval = eachInterval.intervalString;
          intervalDataObj.data = eachClaimTrans;
          if(claimTransTimestamp == eachInterval.intervalStart) {
            timeIntervalClaims.push(intervalDataObj);
            counter++;
          }
        })
        if(counter == 0) {
          timeIntervalClaims.push({"timeInterval": eachInterval.intervalString, "data": {}});
        }
      })

      groupedTimeIntervalsHourClaims = groupRegistrationsByTimeIntervalHour(timeIntervalClaims);

      data = createClainTransHistogramData(groupedTimeIntervalsHourClaims);

      // form graph series
      var seriesObj = new Object();
      seriesObj.name = "Booth Claims Histogram";
      seriesObj.type = "column"
      seriesObj.data = data;
      series.push(seriesObj);

      drawClaimTransactionsHistogram(categories, series, false);
    }
    else{
      drawClaimTransactionsHistogram(categories, series, true);
    }
  }

  // -------------------------------------------------------------
  // FUNCTION FOR DRAWING EVENT-DAY BOOTH CLAIMS HISTOGRAM GRAPH
  // -------------------------------------------------------------
  function drawClaimTransactionsHistogram(categories, series, emptyData) {
    var chart4;
    chart4 = new Highcharts.Chart({
        chart: {
            renderTo: 'container4',
            type: 'column',
            zoomType: 'xy',
            width: 1000
        },
        title: {
            text: 'Real-Time Count of Claim Transactions'
        },
        subtitle: {
            text: 'Select and drag graph portion to zoom.'
        },
        xAxis: [{
            categories: categories
          }, {
            labels: {rotation: -45}
        }],
        yAxis: [{
            labels: {
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            title: {
                text: 'Number of Claims',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            }
        }],
        tooltip: {
            formatter: function() {
                var point = this.point;
                return 'Time Interval: <b>' + point.name + '</b><br/>Number of Claims: <b>' + point.y + '</b>'
            }
        },
        legend: {
            enabled: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold'
                    },
                    formatter: function() {
                      var point = this.point;
                      if(point.y > 0)
                        return point.y;
                    }
                }
            }
        },
        series: series
    })

    if(emptyData == true && series.length == 0) {
      var title = chart4.renderer.text('No data available yet.', 350, 290).css({
        fontSize: '30px',
        color: Highcharts.getOptions().colors[1]
      })
      .add();
    }
  }

});