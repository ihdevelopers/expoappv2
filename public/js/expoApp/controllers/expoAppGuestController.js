
expoApp.controller('guestsController', function($scope, $http, timeStamped, DTOptionsBuilder, Toastr) {
  //$scope.$on('$viewContentLoaded', onReady);  

  $scope.loadingGuests = true;

  function onReady() {
    $scope.$watch('chooseEvent', function() {
      
      $scope.eventGuests = [];
      $scope.guestLookup = {};

      if($scope.chooseEvent.id){
        
        $scope.loadingGuests = true;

        $http({
          method: 'GET',
          url: '/getAllEventGuests',
          params: {eventID: $scope.chooseEvent.id}
        }).success(function(data, status, headers, config) {
          if (data.status == "OK") {
            $scope.eventGuests = $scope.eventGuests.concat(getAllPreregisteredGuests(data.data), getAllRegisteredGuests(data.data));
            $scope.loadingGuests = false;
            for (var i = 0; i < $scope.eventGuests.length; i++ ){
              $scope.guestLookup[$scope.eventGuests[i].id] = $scope.eventGuests[i];
              $scope.guestLookup[$scope.eventGuests[i].id].eventGuestIndex = i;
            }
          }
          else {
          }
        });
      } 
    });
  }

  function getAllPresentGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.isPresent == true) finalList.push(guest);
    });
    return finalList;
  }

  function getAllRegisteredGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.preRegistered == false) {
        guest.type = "Registered onsite";
        finalList.push(guest);
      }
    });    
    return finalList;
  }

  function getAllUnregisteredGuests(list) {
    var finalList = [];
    // angular.forEach(list, function(guest) {
    //  if (guest.isPresent == true) finalList.push(guest);
    // });
    return finalList;
  }

  function getAllPreregisteredGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.preRegistered == true) {
        guest.type = "Pre-registered";
        guest.activated = false;
        finalList.push(guest);
      }
    });
    return finalList;
  }

  function checkActivation(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
    });
  }

  function updateGuestDetails(guestDetails, callback){
    $http({
      method: 'POST',
      url: '/updateGuestDetails',
      data: {_id: guestDetails._id, id: guestDetails.id, uID: guestDetails.uID, firstName: guestDetails.firstName, lastName: guestDetails.lastName, email: guestDetails.email},
    }).success(function(data) {      
      if(data.status === 'OK'){
        callback();
      }
      else{
        callback("Email address already in use");
      }
    }).error(function(data){
      console.log(data);
      callback("Server Error");
    }); 
  }

  onReady();

  $scope.dtOptions = DTOptionsBuilder.fromSource().withBootstrap().withOption('order', []);


  //-------- EDIT GUEST INFO --------//
  $scope.editGuestProfileMode = false;
  $scope.guestToEdit = {};

  $scope.showGuestProfile = function(guestId) {
    $scope.guestToEdit = {
      _id: $scope.guestLookup[guestId]._id,
      id: guestId,
      firstName: $scope.guestLookup[guestId].firstName,
      lastName: $scope.guestLookup[guestId].lastName,
      email: $scope.guestLookup[guestId].email,
      uID: $scope.guestLookup[guestId].uID,
      eventGuestIndex: $scope.guestLookup[guestId].eventGuestIndex
    };
    /*
    $scope.guestLookup[guestId];    
    */
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $("#guestdetails").modal(options);    
  };

  $scope.hideGuestProfile = function(){
    $("#guestdetails").modal("hide");
    $scope.editGuestProfileMode = false;
  };

  $scope.toggleEditGuestProfileMode = function(){
    $scope.editGuestProfileMode = !$scope.editGuestProfileMode;
  };

  $scope.editGuestDetails = function(guestDetails){

    if(guestDetails.uID === undefined){
      guestDetails.uID = null;
    }

    updateGuestDetails(guestDetails, function(err){
      if(err){
        Toastr.show('warning', err, 'Error!');
      }
      else{
        $scope.eventGuests[guestDetails.eventGuestIndex].firstName = guestDetails.firstName;
        $scope.eventGuests[guestDetails.eventGuestIndex].lastName = guestDetails.lastName;
        $scope.eventGuests[guestDetails.eventGuestIndex].email = guestDetails.email;
        $scope.eventGuests[guestDetails.eventGuestIndex].uID = guestDetails.uID;
        Toastr.show('success', 'Successfully updated guest info!', 'Update Successful!');
        $("#guestdetails").modal("hide");
        $scope.editGuestProfileMode = false;
      }
    });
  };

});
