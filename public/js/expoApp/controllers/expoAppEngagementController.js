
expoApp.controller('engagementsController', function($scope, $http, timeStamped, DTOptionsBuilder, Toastr) {
  //$scope.$on('$viewContentLoaded', onReady);  

  $scope.loadingEngagements = true;

  function onReady() {
    $scope.$watch('chooseEvent', function() {
      
      $scope.eventEngagements = [];
      $scope.eventEngagementsLookup = [];
      $scope.eventBooths = [];
      $scope.eventCheckinBooths = [];
      // $scope.eventClaimBooths = []; // for future use
      $scope.eventGuests = [];
      $scope.uniqueUIDs = [];
      $scope.uniqueUIDDetails = [];
      $scope.guestLookup = {};
      var getCounter = 0;

      // trial for dynamic header
      // $scope.tableHeaders = ['TransID', 'Last Name', 'First Name', 'RFID', 'Booth ID', 'Booth Name', 'Transaction Time']
      $scope.tableHeaders = ['First Name', 'Last Name', 'Mobile No.', 'RFID'];
      $scope.dynamicTableHeaders = [];

      if($scope.chooseEvent.id){
        
        $scope.loadingEngagements = true;

        $http({
          method: 'GET',
          url: '/getAllEventBooths',
          params: {eventID: $scope.chooseEvent.id}
        }).success(function(getAllEventBooths_response, status, headers, config) {
          if(getAllEventBooths_response.status == "OK") {
            $scope.eventBooths = getAllEventBooths_response.data;
            console.log($scope.eventBooths)

            // get all checkin booths
            angular.forEach($scope.eventBooths, function(eachBooth) {
              if(eachBooth.type == "checkin") {
                $scope.eventCheckinBooths.push(eachBooth);
              }
            })
            $scope.eventCheckinBooths.sort(dynamicSort("name"));
            angular.forEach($scope.eventCheckinBooths, function(eachCheckinBooth) {
              $scope.dynamicTableHeaders.push(eachCheckinBooth.name);
            })
            getCounter++;
            if(getCounter == 3){
              countBoothStats();
            }
          }
          else {

          }
        }).error(function(getAllEventBooths_response, status, headers, config) {
          Toastr.show('error', JSON.stringify(getAllEventBooths_response) + '; getAllEventBooths', 'Error')
        })

        $http({
          method: 'GET',
          url: '/getAllEventGuests',
          params: {eventID: $scope.chooseEvent.id, limitedFieldsOnly: true}
        }).success(function(getAllEventGuests_response, status, headers, config) {
          if(getAllEventGuests_response.status == "OK") {
            $scope.eventGuests = getAllEventGuests_response.data;
            console.log($scope.eventGuests)
            getCounter++;
            if(getCounter == 3){
              countBoothStats();
            }
          }
          else {

          }
        }).error(function(getAllEventGuests_response, status, headers, config) {
          Toastr.show('error', JSON.stringify(getAllEventGuests_response) + '; getAllEventGuests', 'Error')

        })        

        $http({
          method: 'GET',
          url: '/getAllEventTransactions',
          params: {eventID: $scope.chooseEvent.id}
        }).success(function(getAllEventTransactions_response, status, headers, config) {
          if (getAllEventTransactions_response.status == "OK") {
            // console.log(JSON.stringify(data, null, '    '))
            $scope.eventEngagements = getAllEventTransactions_response.data;
            console.log($scope.eventEngagements)
            angular.forEach($scope.eventEngagements, function(eachEngagement) {
              eachEngagement.boothName = _.result(_.findWhere($scope.eventBooths, {'id': eachEngagement.boothID}), 'name')
            })
            $scope.uniqueUIDs = _.uniq(_.pluck($scope.eventEngagements, 'uID'));
            console.log($scope.uniqueUIDs);

            $scope.loadingEngagements = false;
            getCounter++;
            if(getCounter == 3){
              countBoothStats();
            }
          }
          else {
          }
        }).error(function(getAllEventTransactions_response, status, headers, config) {
          Toastr.show('error', JSON.stringify(getAllEventTransactions_response) + '; getAllEventTransactions', 'Error')
        })
      } 
    });
  }

  function countBoothStats() {
    angular.forEach($scope.uniqueUIDs, function(eachUID) {
      var _obj = {};
      _obj.uID = eachUID;
      _obj.firstName = _.result(_.findWhere($scope.eventGuests, {'uID': eachUID}), 'firstName');
       _obj.lastName = _.result(_.findWhere($scope.eventGuests, {'uID': eachUID}), 'lastName');
      // _obj.gender = _.result(_.findWhere($scope.eventGuests, {'uID': eachUID}), 'gender');
      _obj.mobileNo = _.result(_.findWhere($scope.eventGuests, {'uID': eachUID}), 'mobileNo');
      angular.forEach($scope.eventCheckinBooths, function(eachCheckinBooth) {
        var tempBoothName = 'z' + eachCheckinBooth.name.replace(/\s/g, '');
        _obj[tempBoothName] = (_.where($scope.eventEngagements, {'boothID': eachCheckinBooth.id, 'uID': eachUID})).length;
      })
      $scope.uniqueUIDDetails.push(_obj);
    })
    console.log('$scope.uniqueUIDDetails')
    console.log($scope.uniqueUIDDetails)

    angular.forEach($scope.uniqueUIDDetails, function(eachUIDDetails) {
      var _obj = {};
      _obj.uID = eachUIDDetails.uID;
      _obj.firstName = eachUIDDetails.firstName;
      _obj.lastName = eachUIDDetails.lastName;
      // _obj.gender = eachUIDDetails.gender;
      _obj.mobileNo = eachUIDDetails.mobileNo;
      $scope.eventEngagementsLookup.push(_obj);
    })
  }

  function getAllPresentGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.isPresent == true) finalList.push(guest);
    });
    return finalList;
  }

  function getAllRegisteredGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.preRegistered == false) {
        guest.type = "Registered onsite";
        finalList.push(guest);
      }
    });    
    return finalList;
  }

  function getAllUnregisteredGuests(list) {
    var finalList = [];
    // angular.forEach(list, function(guest) {
    //  if (guest.isPresent == true) finalList.push(guest);
    // });
    return finalList;
  }

  function getAllPreregisteredGuests(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
      if (guest.preRegistered == true) {
        guest.type = "Pre-registered";
        guest.activated = false;
        finalList.push(guest);
      }
    });
    return finalList;
  }

  function checkActivation(list) {
    var finalList = [];
    angular.forEach(list, function(guest) {
    });
  }

  function updateGuestDetails(guestDetails, callback){
    $http({
      method: 'POST',
      url: '/updateGuestDetails',
      data: {_id: guestDetails._id, id: guestDetails.id, uID: guestDetails.uID, firstName: guestDetails.firstName, lastName: guestDetails.lastName, email: guestDetails.email},
    }).success(function(data) {      
      if(data.status === 'OK'){
        callback();
      }
      else{
        callback("Email address already in use");
      }
    }).error(function(data){
      console.log(data);
      callback("Server Error");
    }); 
  }

  function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
  }

  onReady();

  $scope.dtOptions = DTOptionsBuilder.fromSource().withBootstrap().withOption('order', []);


  //-------- EDIT GUEST INFO --------//
  $scope.editGuestProfileMode = false;
  $scope.guestToEdit = {};

  $scope.showGuestProfile = function(guestId) {
    $scope.guestToEdit = {
      _id: $scope.guestLookup[guestId]._id,
      id: guestId,
      firstName: $scope.guestLookup[guestId].firstName,
      lastName: $scope.guestLookup[guestId].lastName,
      email: $scope.guestLookup[guestId].email,
      uID: $scope.guestLookup[guestId].uID,
      eventGuestIndex: $scope.guestLookup[guestId].eventGuestIndex
    };
    /*
    $scope.guestLookup[guestId];    
    */
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $("#guestdetails").modal(options);    
  };

  $scope.hideGuestProfile = function(){
    $("#guestdetails").modal("hide");
    $scope.editGuestProfileMode = false;
  };

  $scope.toggleEditGuestProfileMode = function(){
    $scope.editGuestProfileMode = !$scope.editGuestProfileMode;
  };

  $scope.editGuestDetails = function(guestDetails){

    if(guestDetails.uID === undefined){
      guestDetails.uID = null;
    }

    updateGuestDetails(guestDetails, function(err){
      if(err){
        Toastr.show('warning', err, 'Error!');
      }
      else{
        $scope.eventEngagements[guestDetails.eventGuestIndex].firstName = guestDetails.firstName;
        $scope.eventEngagements[guestDetails.eventGuestIndex].lastName = guestDetails.lastName;
        $scope.eventEngagements[guestDetails.eventGuestIndex].email = guestDetails.email;
        $scope.eventEngagements[guestDetails.eventGuestIndex].uID = guestDetails.uID;
        Toastr.show('success', 'Successfully updated guest info!', 'Update Successful!');
        $("#guestdetails").modal("hide");
        $scope.editGuestProfileMode = false;
      }
    });
  };

});
