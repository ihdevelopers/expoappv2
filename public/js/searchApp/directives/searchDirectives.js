
searchApp.directive('ngEnter', function() {
    return {
        scope: {controlFn: '&callbackFn'},
        link: function(scope, element, attrs) {
            element.bind("keydown", function(event) {
                if (event.which === 13) {
                    scope.controlFn();
                }
            });
            // element.focus();
        }
    }    
});