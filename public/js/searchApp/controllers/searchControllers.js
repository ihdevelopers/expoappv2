searchApp.controller('searchControl', function($scope, $http, $compile, $rootScope) {
    $scope.guest;
    
    $scope.clickSearch = function() {
        console.log('Searching...')
        angular.element(".searchTree div").html('');
        $http({
            method: 'GET',
            url: "/getGuestDetailsByName",
            params: { searchEntry: angular.element('#searchbox').val() }
        }).success(function(data, status, headers, config) {
            console.log(data)
            if (data.data.length <= 0) {
                var ele = $('<div class="confirmParticipant"> </div>').html('<a href="javascript:void(0);"> <p> No Guest Found. </p> </a>');
                var compiledElement = $compile(ele)($scope);
                angular.element('.searchTree').append(compiledElement);
            }
            else {
                data.data.forEach(function(member) {
                    var ele = $('<div class="confirmParticipant" ng-click="confirmsearchID(\''+ member.firstName +'\', \''+ member.lastName +'\', \''+ member.id +'\', \''+ member.uID +'\');"> </div>').html('<a href="javascript:void(0);"> <p> RFID: ' + member.uID + '<br /> <br />' + member.firstName + ' ' + member.lastName + '<br /> <br />' + member.email + '</p> </a>');
                    var space = $('<div class="space"> <br /> </div>');
                    var compiledElement = $compile(ele)($scope);
                    angular.element('.searchTree').append(compiledElement).append(space) ;
                });
            }
        });
    }

    $scope.confirmsearchID = function(firstname, lastname, id, uID) {
        $scope.guest = {
            firstName: firstname,
            lastName: lastname,
            id: id,
            rfid: uID
        }
    }

    $scope.submitRFID = function() {
        var options = {
          show: true,
          keyboard: false,
          backdrop: 'static'
        }
        $("#userprofile").modal(options);
        $rootScope.overwrite = false;
        $scope.hideNOTE = false;
    }

    $scope.goBackToInputRFID = function() {
        $scope.hideRFID = false;
        $scope.guest.uID = "";
        $rootScope.overwrite = false;
    }

    $scope.submitwithRFID = function(){
        if ($rootScope.overwrite == false) {
            $.ajax({
                method: 'GET',
                url: '/getGuestDetails',
                data: {guestID: null, uID: $scope.guest.uID.toUpperCase(), isGuestID: false}
            }).done(function(data) {
                if ((data.status == "OK") && (data.data != null)) {
                    console.log('hellsad', data)
                    // $scope.$apply(function() {
                    //     $scope.hideRFID = true;
                    // })
                    // $rootScope.overwrite = true;
                    $scope.$apply(function() {
                        $scope.hideNOTE = true;
                    })
                }
                else {
                    $scope.hideNOTE = false;
                    console.log($scope.guest)
                    if ($scope.guest.rfid != "undefined") {
                        console.log($scope.guest.rfid);

                        $scope.$apply(function() {
                            $scope.hideRFID = true;
                        })
                        $rootScope.overwrite = true;
                    }
                    else {
                        $rootScope.overwrite = false;
                        console.log('iris',$scope.guest)
                        $scope.guest.uID = $scope.guest.uID.toUpperCase();
                        $http({
                            method: 'POST',
                            url: '/updateGuestUID',
                            data: {guest: $scope.guest}
                        }).success(function(data, status, headers, config) {
                            if (data.status == "OK") {
                                console.log(data);
                                $scope.guest = {};
                                $("#userprofile").modal('hide');
                                var options = {
                                    show: true,
                                    keyboard: false,
                                    backdrop: 'static'
                                }
                                $("#messagealertbody").html("RFID assigned successfully");
                                $("#messagealertheader").html("Registration successful");
                                $("#messagealert").modal(options);

                                angular.element(".searchTree div").html('');
                                angular.element('#searchbox').val('');
                            }
                            else {
                                console.log(data);
                            }
                        });    
                    }
                    
                }
            });
        }
        else {
            $scope.hideRFID = false;
            $scope.guest.uID = $scope.guest.uID.toUpperCase();
            $http({
                method: 'POST',
                url: '/updateGuestUID',
                data: {guest: $scope.guest}
            }).success(function(data, status, headers, config) {
                if (data.status == "OK") {

                    $rootScope.overwrite = false;
                    console.log(data);
                    $scope.guest = {};
                    $("#userprofile").modal('hide');
                    var options = {
                        show: true,
                        keyboard: false,
                        backdrop: 'static'
                    }
                    $("#messagealertbody").html("RFID assigned successfully");
                    $("#messagealertheader").html("Registration successful");
                    $("#messagealert").modal(options);

                    angular.element(".searchTree div").html('');
                    angular.element('#searchbox').val('');
                }
                else {
                    console.log(data);
                }
            });
        }
    }
});