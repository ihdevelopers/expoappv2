
$(document).ready(function() {
	var posX = (Math.floor($(window).width() / 16)).toString() + 'px';
	var posY = (Math.floor($(window).height() / 5)).toString() + 'px';

	$('.turniton').css('visibility', 'hidden')
	$('.name').css('visibility', 'hidden')

	$('.turniton').css('margin-left', posX)
	$('.turniton').css('margin-top', posY)

	$('.name').css('margin-top', Math.floor($(window).height() / 5).toString() + 'px')
	
	var socket = io();
	// code snippet for exit greeting (data = "FirstName LastName")
	socket.on('exitGreeting', function(data) {
		$('.turniton').html('THANK YOU');
		$('.name').html(data.split(' ')[0] + '!');
		var namePos = Math.floor($(window).width() / 2) - Math.floor($('.name').width() / 2)
		$(".name").css('margin-left', namePos)
		$('.turniton').css('visibility', 'visible')
		$('.name').css('visibility', 'visible')
		$("#animateThis").addClass('animated pulse');
		$("#thenAnimateThis").addClass('animated pulse');
		sleep(2500, foobar_cont);
	});
	// code snippet for entrance greeting (data = "FirstName LastName")
	// socket.on('entranceGreeting', function(data) {
	// 	$('.name').html(data.split(' ')[0] + '!');
	// 	var namePos = Math.floor($(window).width() / 2) - Math.floor($('.name').width() / 2)
	// 	$(".name").css('margin-left', namePos)
	// 	$('.turniton').css('visibility', 'visible')
	// 	$('.name').css('visibility', 'visible')
	// 	$("#animateThis").addClass('animated pulse');
	// 	$("#thenAnimateThis").addClass('animated pulse');
	// 	sleep(2000, foobar_cont);
	// });
	
}); 

function foobar_cont(){
    location.reload();
};

function sleep(millis, callback) {
    setTimeout(function()
            { callback(); }
    , millis);
}
