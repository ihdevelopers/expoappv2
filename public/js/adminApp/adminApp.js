/*
 *   MAIN APP SCRIPT
 */
var adminExhibitApp = angular.module('adminExhibitApp', ['ngRoute']);

//This configures the routes and associates each route with a view and a controller
adminExhibitApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/',
            {
                controller: 'ProfileController',
                templateUrl: 'js/adminApp/partials/profile.html',
            })
        .when('/profile',
            {
                controller: 'ProfileController',
                templateUrl: 'js/adminApp/partials/profile.html',
            })
        .when('/events',
            {
                controller: 'EventsController',
                templateUrl: 'js/adminApp/partials/events.html'
            })
        .when('/events/:eventsId',
            {
                controller: 'EventsController',
                templateUrl: 'js/adminApp/partials/eventDetail.html'
            })
        .when('/events/:eventsId/exhibitors',
            {
                controller: 'EventsExhibitorsController',
                templateUrl: 'js/adminApp/partials/eventExhibitors.html'
            })
        .when('/organizers',
            {
                controller: 'OrganizersController',
                templateUrl: 'js/adminApp/partials/organizers.html'
            })
        .when('/organizers/:organizersId',
            {
                controller: 'OrganizerDetailsController',
                templateUrl: 'js/adminApp/partials/organizersDetail.html'
            })
        .when('/exhibitors',
            {
                controller: 'ExhibitorsController',
                templateUrl: 'js/adminApp/partials/exhibitors.html'
            })
        .when('/exhibitors/:exhibitorsId',
            {
                controller: 'ExhibitorDetailsController',
                templateUrl: 'js/adminApp/partials/exhibitorsDetail.html'
            })
        .when('/addUser',
            {
                controller: 'UserController',
                templateUrl: 'js/adminApp/partials/adduser.html'
            })
        .otherwise({ redirectTo: '/' });

});
