//This handles retrieving data and is used by controllers. 3 options (server, factory, provider) with 
//each doing the same thing just structuring the functions/data differently.
adminExhibitApp.service('AdminService', function ($http) {

	/*
	 *   Admin User related services
	 */
    this.getUserInfo = function () {
        return $http.get('/getUserInfo');
    };

    this.registerNewUser = function (newUser) {
        return $http.post('/registerNewUser', {"newUser": newUser});
    };

    /*
     *   Exhibitor User related services
     */
     this.getAllExhibitors = function() {
     	return $http.get('/getAllExhibitors');
     }

     this.getAllExhibitorsByEvent = function(eventId) {
     	return $http.post('/getAllEventExhibitors', {"eventId" : eventId});
     }

     this.addNewEventExhibitor = function(eventId, exhibitorCompany){
     	return $http.post('/addNewEventExhibitor', {"eventId" : eventId, "exhibitorCompany" : exhibitorCompany});
     }

    /*
     *   Organizer User related services
     */
     this.getAllOrganizers = function() {
     	return $http.get('/getAllOrganizers');
     }

     this.getOrganizerById = function(id) {
     	return $http.post('/getOrganizerById', {"id" : id});
     }

    /*
     *   Event related services
     */
     this.getAllEvents = function() {
     	return $http.get('/getAllEvents');
     }

     this.registerNewEvent = function(newEvent) {
     	return $http.post('/registerNewEvent', {"newEvent" : newEvent});
     }

     this.getEventById = function(id) {
     	return $http.post('/getEventById', {"id" : id});
     }


});