/*
 *   APP CONTROLLERS
 */
adminExhibitApp.controller('ProfileController', function ($scope, $http, AdminService) {

    $scope.User = {};

    init();

    function init() {
        AdminService.getUserInfo().success(function(data){
            $scope.User = data;
        });
    }

    $scope.changeEmail = function(){
        
    }

    $scope.changePassword = function(){
        
    }

});

adminExhibitApp.controller('EventsController', function ($scope, $route, $routeParams, $filter, AdminService) {

    $scope.allEvents = {};
    $scope.newEvent = {};
    $scope.Event = {};

    init();

    function init() {
        AdminService.getAllEvents().success(function(data){
            $scope.allEvents = data;
            getFocusEvent();

        });
    }

    function getFocusEvent() {
        var matches = $filter('filter')($scope.allEvents, {"_id" : $routeParams.eventsId});
        $scope.Event = matches[0];
        AdminService.getOrganizerById($scope.Event.eventOrganizer).success(function(data){
            $scope.Event.organizer = data;
        })
    }

    $scope.addNewEvent = function(){
        $scope.newEvent.date = Date.parse($scope.newEvent.date);
        AdminService.registerNewEvent($scope.newEvent).success(function(resp){
            console.log(resp);
            if(resp == 'ok'){
                $scope.newEvent.organizer = ""; 
                $scope.newEvent.name = "";
                $scope.newEvent.date = "";
                $scope.newEvent.location = "";
                $scope.newEvent.notes = "";
                $route.reload();
            }
        });
    }

    $scope.addNewEventExhibitor = function() {
        AdminService.addNewEventExhibitor($routeParams.eventsId, $scope.newExhibitor).success(function(resp){
            console.log(resp);
            if(resp == 'ok'){
                $scope.newExhibitor = ""; 
            }
        });
    }
    
    $scope.setOrder = function (orderby) {
        if (orderby === $scope.orderby)
        {
            $scope.reverse = !$scope.reverse;
        }
        $scope.orderby = orderby;
    };

});

adminExhibitApp.controller('EventDetailsController', function ($scope, AdminService) {

    $scope.Event = {};

    init();

    function init() {
        AdminService.getAllEvent().success(function(data){
            $scope.allEvents = data;
        });
    }

});

adminExhibitApp.controller('EventsExhibitorsController', function ($scope, $route, $routeParams, $filter, AdminService) {

    $scope.Event = {};
    $scope.eventExhibitors = {};

    init();

    function init() {
        AdminService.getEventById($routeParams.eventsId).success(function(data){
            $scope.Event = data;
            AdminService.getAllExhibitorsByEvent($routeParams.eventsId).success(function(exhibitors){
                $scope.eventExhibitors = exhibitors;
            });
        });
    }

    $scope.setOrder = function (orderby) {
        if (orderby === $scope.orderby)
        {
            $scope.reverse = !$scope.reverse;
        }
        $scope.orderby = orderby;
    };

});

adminExhibitApp.controller('ExhibitorsController', function ($scope, $route, AdminService) {

    $scope.allExhibitors = {};
    $scope.newExhibitor = {};

    init();

    function init() {
        AdminService.getAllExhibitors().success(function(data){
            $scope.allExhibitors = data;
        });
    }

    $scope.addNewExhibitor = function(){
        $scope.newExhibitor.accessType = 'exhibitor';
        AdminService.registerNewUser($scope.newExhibitor).success(function(resp){
            console.log(resp);
            if(resp == 'ok'){ 
                $scope.newExhibitor.company = "";
                $scope.newExhibitor.email = "";
                $route.reload();
            }
        });
    }
    
    $scope.setOrder = function (orderby) {
        if (orderby === $scope.orderby)
        {
            $scope.reverse = !$scope.reverse;
        }
        $scope.orderby = orderby;
    };


});

adminExhibitApp.controller('OrganizersController', function ($scope, $route, AdminService) {

    $scope.allOrganizers = {};
    $scope.newOrganizer = {};

    init();

    function init() {
        AdminService.getAllOrganizers().success(function(data){
            $scope.allOrganizers = data;
        });
    }

    $scope.addNewOrganizer = function(){
        $scope.newOrganizer.accessType = 'organizer';
        AdminService.registerNewUser($scope.newOrganizer).success(function(resp){
            console.log(resp);
            if(resp == 'ok'){ 
                $scope.newOrganizer.company = "";
                $scope.newOrganizer.email = "";
                $route.reload();
            }
        });
    }
    
    $scope.setOrder = function (orderby) {
        if (orderby === $scope.orderby)
        {
            $scope.reverse = !$scope.reverse;
        }
        $scope.orderby = orderby;
    };


});

adminExhibitApp.controller('ExhibitorDetailsController', function ($scope, AdminService) {


});

adminExhibitApp.controller('UserController', function ($scope, AdminService) {
    
    $scope.newUser = {};

    $scope.registerUser = function(){
        $scope.newUser.accessType = 'admin';
        AdminService.registerNewUser($scope.newUser).success(function(resp){
            console.log(resp);
            if(resp == 'ok'){ 
                $scope.newUser.name = "";
                $scope.newUser.email = "";
            }
        });
    }



});

adminExhibitApp.controller('NavbarController', function ($scope, $location) {
    $scope.getClass = function (path) {
        if ($location.path().substr(0, path.length) == path) {
            return true
        } else {
            return false;
        }
    }
});