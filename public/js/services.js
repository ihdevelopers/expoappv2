/*
 *   Popover on mouse hover on dashboard
 */

angular.module('expoApp.services', [])

.directive('toggle', function() {
    console.log('in toggle directive!')
    return {
    restrict: 'A',
    link: function(scope, element, attrs){
      if (attrs.toggle=="tooltip"){
        $(element).tooltip();
      }
      if (attrs.toggle=="popover"){
        $(element).popover();
      }
    }
  };
})