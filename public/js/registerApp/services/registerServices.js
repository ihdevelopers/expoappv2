
registerapp.service('info', function() {
  var guest = {};

  return {
    setGuest: function(guestDetails) {
      guest = angular.copy(guestDetails);
    },
    getGuest: function() {
      return guest;
    }
  } 
});
