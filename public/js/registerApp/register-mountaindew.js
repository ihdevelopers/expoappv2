var registerapp = angular.module('registerApp', ['ngRoute']);

registerapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'js/registerApp/partials/empty.html',
    controller: 'emptyController'
  });
  $routeProvider.when('/:message', {
    templateUrl: 'js/registerApp/partials/form.html',
    controller: 'formController'
  });
  $routeProvider.when('/form', {
    templateUrl: 'js/registerApp/partials/empty.html',
    controller: 'emptyController'
  });
  $routeProvider.when('/form/:message', {
    templateUrl: 'js/registerApp/partials/form.html',
    controller: 'formController'
  });
  $routeProvider.when('/thankyou', {
    templateUrl: 'js/registerApp/partials/thankyou.html',
    controller: 'thankyouController'
  });
  $routeProvider.otherwise({
    redirectTo: '/'
  });
}]);

registerapp.controller("registerController", function($scope, $http, $routeParams) {

});

registerapp.controller("formController", function($scope, $http, $routeParams) {
	window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '169921833199214',
	      xfbml      : true,
	      version    : 'v2.0'
	    });
	};

  	(function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
   	}(document, 'script', 'facebook-jssdk'));
	
	$scope.event;

	if ($routeParams.message != null) {
		$scope.showFirstName = false;
		$scope.showLastName = false;
		$scope.showMiddleName = false;
		$scope.showNickName = false;
		$scope.showGender = false;
		$scope.showEmail = false;
		$scope.showMobileNo = false;
		$scope.showTitle = false;
		$scope.showProfession = false;
		$scope.showCompany = false;
		$scope.showEducation = false;
		$scope.showStreetAddress1 = false;
		$scope.showStreetAddress2 = false;
		$scope.showCityAddress = false;
		$scope.showRegionAddress = false;
		$scope.showCountry = false;
		$scope.showZip = false;
		$scope.showReligion = false;
		$scope.showFacebookPage = false;
		$scope.showTwitterHandle = false;
		$scope.showInstagramHandle = false;

		$http({
			method: 'GET',
			url: '/getEventDetails',
			params: {eventID: $routeParams.message}
		}).success(function(data, status, headers, config) {
			if (data.status == "OK") {
				$scope.event = data.data;
				console.log(data.data.logoImage);
				$("#sanmigLogo").attr('src', data.data.logoImage);
				$("body").css('background-image','url('+ data.data.backgroundImage + ')');
				var guestDetails = data.data.guestDetailsRequired;
				angular.forEach(guestDetails, function(element) {
					if (element == 2) $scope.showFirstName = true;
					if (element == 3) $scope.showLastName = true;
					if (element == 4) $scope.showMiddleName = true;
					if (element == 5) $scope.showNickName = true;
					if (element == 6) $scope.showGender = true;
					if (element == 10) $scope.showEmail = true;
					if (element == 11) $scope.showMobileNo = true;
					if (element == 12) $scope.showTitle = true;
					if (element == 13) $scope.showProfession = true;
					if (element == 14) $scope.showCompany = true;
					if (element == 15) $scope.showEducation = true;
					if (element == 16) $scope.showStreetAddress1 = true;
					if (element == 17) $scope.showStreetAddress2 = true;
					if (element == 18) $scope.showCityAddress = true;
					if (element == 19) $scope.showRegionAddress = true;
					if (element == 20) $scope.showCountry = true;
					if (element == 21) $scope.showZip = true;
					if (element == 22) $scope.showReligion = true;
					if (element == 23) $scope.showFacebookPage = true;
					if (element == 24) $scope.showTwitterHandle = true;
					if (element == 25) $scope.showInstagramHandle = true;
				});
			}
			else {
				console.log(data);
			}
		});	
	}	

	$scope.submitForm = function() {
		var options = {
	        show: true,
	        keyboard: false,
	        backdrop: 'static'
	    }
	    $("#userprofile").modal(options);

		$scope.guest.eventID = $scope.event.id;
		$http({
			method: 'POST',
			url: '/addGuest',
			data: {newGuest: $scope.guest}
		}).success(function(data, status, headers, config) {
			if (data.status == "OK") {
				console.log(data);
				$scope.guest = {};
			}
			else {
				console.log(data);
			}
		});
	}

	$scope.showModal = function() {
	    var options = {
	        show: true,
	        keyboard: false,
	        backdrop: 'static'
	    }
	    $("#basicModal-displaytime").modal(options);
	}

	$scope.connectFB = function() {
		FB.login(function(responseStat) {
    		console.log('Facebook Response : ' + JSON.stringify(responseStat));
        if (responseStat.status == "connected") {
          FB.api("/me?access_token=" + responseStat.authResponse.accessToken, function(response) {
            if (response && !response.error) {
              console.log(response);

              $http({
                  method: 'POST',
                  url: "/checkFBduplicate",
                  data: {fbID: responseStat.authResponse.userID}
              }).success(function(data, status, headers, config) {
                  if (data.status == true) {
                    $(".tryagain-displaytime").html("Try again");
   
                    var options = {
                        show: true,
                        keyboard: false,
                        backdrop: 'static'
                    }
                    $("#basicModal-displaytime").modal(options);
                    $(".modal-body h3").html("This facebook account has already registered. Please use another account.");
                  }
                  else {
                    connected = true;
                    FB.api('/me/picture?redirect=false&width=500', function(responsePhoto) {
                      if (response && !response.error) {
                        angular.element("#firstName").val(response.first_name);
                        angular.element("#lastName").val(response.last_name);
                        angular.element("#emailAddress").val(response.email);
                        angular.element("#year").val(response.birthday.split('/')[2]);
                        angular.element("#month").val(response.birthday.split('/')[0]);
                        angular.element("#date").val(response.birthday.split('/')[1]);

                        if (response.gender == "female") {
                          angular.element("#female").prop("checked", true);
                        }
                        else if (response.gender == "male") {
                          angular.element("#male").prop("checked", true);
                        }
                        console.log(JSON.stringify(responsePhoto));
                        guestDetails.profilePicture = responsePhoto.data.url;
                        guestDetails.fbID = responseStat.authResponse.userID;
                        guestDetails.accessToken = responseStat.authResponse.accessToken;
                        
                        $scope.hideLoad = true;
                        angular.element("#firstName").prop("disabled", false);
                        angular.element("#lastName").prop("disabled", false);
                        angular.element("#month").prop("disabled", false);
                        angular.element("#date").prop("disabled", false);
                        angular.element("#year").prop("disabled", false);
                        angular.element("#emailAddress").prop("disabled", false);
                        angular.element("#mobileNumber").prop("disabled", false);
                        angular.element("#year").prop("disabled", false);
                        angular.element("input:radio").prop("disabled", false);
                        // angular.element("#eventpresent").prop("disabled", false);
                      }
                    });
                  } 
              });              
              console.log('To register: ' + JSON.stringify(guestDetails));
            }
          })
        }

    	}, {scope:'public_profile,publish_stream,email,user_about_me,publish_actions'})
	}
});

registerapp.controller("emptyController", function($scope, $http) {

});