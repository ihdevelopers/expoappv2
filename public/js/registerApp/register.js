var registerapp = angular.module('registerApp', ['ngRoute']);

 //$("#rfidInput").focus();

registerapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'js/registerApp/partials/empty.html',
    controller: 'emptyController'
  });
  $routeProvider.when('/:message', {
    templateUrl: 'js/registerApp/partials/_form.html',
    controller: 'formController'
  });
  /*
  $routeProvider.when('/form', {
    templateUrl: 'js/registerApp/partials/empty.html',
    controller: 'emptyController'
  });
  $routeProvider.when('/form/:message', {
    templateUrl: 'js/registerApp/partials/form.html',
    controller: 'formController'
  });
  */
  $routeProvider.otherwise({
    redirectTo: '/'
  });
}]);
