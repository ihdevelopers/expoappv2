var registerapp = angular.module('registerApp', ['ngRoute']);

 //$("#rfidInput").focus();

registerapp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'js/registerApp/partials/empty.html',
    controller: 'emptyController'
  });
  $routeProvider.when('/:message', {
    templateUrl: 'js/registerApp/partials/anonymousReg.html',
    controller: 'formController'
  });
  
  $routeProvider.otherwise({
    redirectTo: '/'
  });
}]);
