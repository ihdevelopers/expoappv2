
registerapp.controller("formController", function($scope, $http, $routeParams, $rootScope, info) {

  //GET EVENT DETAILS
  var getEvent = function(){
    $http({
      method: 'GET',
      url: '/getEventDetails',
      params: {eventID: $routeParams.message, regParamsOnly: true }
    }).success(function(data, status, headers, config) {
      if (data.status == "OK") {        
        if(data.data){
          buildForm(data.data);
        }
        else{
          alert("Invalid Registration Page!");
          window.location.href = '/404';
        }
      }
      else{
        alert("Invalid Registration Page!");
        window.location.href = '/404';
      }
    }).error(function(){
      alert("Invalid Registration Page!");
      window.location.href = '/404';
    });
  };

  getEvent();

  function buildForm(data){
    console.log(data);
    $scope.event = data;
    $("#sanmigLogo").attr('src', 'img/yamahalogo.png');
    $("body").css('background-image','url(img/yamaha.jpg)');
  }


  $scope.showError = false;
  $scope.disableRFID = false;
  $scope.rfidLoading = false;
  var timeoutID;

  $scope.anonymousRegistration = function(){
    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    };
    $('#userprofile').modal(options);
  };

  $scope.completeRegistration = function(){
    window.location.href = '/register#/' + $routeParams.message;
  };

  $scope.checkRFID = function() {
    $scope.disableRFID = true;
    $scope.rfidLoading = true;

    $http({
      method: 'GET',
      url: '/getGuestDetails',
      params: {guestID: null, uID: $scope.guest.uID.toUpperCase(), isGuestID: false}
    }).success(function(data) {

      //rfid already exists!
      if ((data.status == "OK") && (data.data != null)) {
        $scope.showError = true;
        $scope.disableRFID = false;
        $scope.rfidLoading = false;
      }

      //rfid does not exist yet
      else{
        $scope.showError = false;
        
        //post to api
        registerAnonymousGuest(data, function(err){
          if(err){
            console.log(err);
          }
          else{
            registerSuccessSequence();
          }
        });
      }
    }).error(function(data){
      console.log(data);
      $scope.showError = true;
      $scope.disableRFID = false;
      $scope.rfidLoading = false;
    });
  };

  function registerAnonymousGuest(data, callback){
    var finalGuest = info.getGuest();    
    console.log(finalGuest);
    console.log(data);
    callback();
  }

  function registerSuccessSequence(){
    $scope.showError = false;
    $scope.disableRFID = false;
    $scope.rfidLoading = false;
    $scope.guest = {};            
    window.clearTimeout(timeoutID);
    $('#userprofile').modal('hide');

    var options = {
      show: true,
      keyboard: false,
      backdrop: 'static'
    }
    $("#thankyouprofile").modal(options);

    timeoutID = window.setTimeout(hideThankYouProfile, 5000);
  }

  function hideThankYouProfile(){
    if($('#thankyouprofile').hasClass('in')){
      $('#thankyouprofile').modal('hide');
    }
  }

/*  

var finalGuest = info.getGuest();
finalGuest.eventID = $scope.event.id;
finalGuest.preRegistered = false;
if ($scope.event.activationRequired) {
  finalGuest.isPresent = false;
}
else {
  finalGuest.isPresent = true;
}
// finalGuest.timeRegistered = new Date().getTime();
finalGuest.uID = $scope.guest.uID.toUpperCase();
$http({
  method: 'POST',
  url: '/addGuest',
  data: {newGuest: finalGuest}
}).success(function(data, status, headers, config) {
  })
  */



});

registerapp.controller('emptyController', function(){

});
