
registerapp.controller("formController", function($scope, $http, $routeParams, $rootScope, info) {
  $scope.dataForTerms = {};

  //FB SCRIPT  
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '619566324728121',
      xfbml      : true,
      version    : 'v2.0'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  
  //Get Event Details
  var eventDetails = {};
  var options = {
    show: true,
    keyboard: false,
    backdrop: 'static'
  };

  function getEventDetails(){
    $http({
      method: 'GET',
      url: '/getEventDetails',
      params: {eventID: $routeParams.message, regParamsOnly: true }
    }).success(function(data, status, headers, config) {
      if (data.status == "OK") {        
        if(data.data){
          data.data.dateString = moment(data.data.date).format("MMMM DD, YYYY");
          data.data.organizer = "Nestea"; // should be dynamic when organizer model is implemented already
          $scope.dataForTerms = data.data;
          buildForm(data.data);
        }
        else{
          alert("Invalid Registration Page!");
          window.location.href = '/404';
        }
      }
      else{
        alert("Invalid Registration Page!");
        window.location.href = '/404';
      }
    }).error(function(){
      alert("Invalid Registration Page!");
      window.location.href = '/404';
    });
  }

  function buildForm(data){

    eventDetails = data;
    console.log(JSON.stringify(eventDetails))
    
    if(data.logoImage){
      $("#sanmigLogo").attr('src', data.logoImage);
    }
    else{
      //default in case none is uploaded
      $("#sanmigLogo").attr('src', 'img/yamahalogo.png');
    }

    if(data.backgroundImage){
      $("body").css('background-image','url('+ data.backgroundImage + ')');
    }
    else{
      //default in case none is uploaded
      $("body").css('background-image','url(img/yamaha.jpg)');
    }
    
    $scope.showPersonalInfoPanel = false;
    $scope.showContactInfoPanel = false;
    $scope.showSocialInfoPanel = false;
    $scope.showOtherlInfoPanel = false;

    data.guestDetailsRequired.forEach(function(e){
      if (e === 2) { $scope.showFirstName = true; $scope.showPersonalInfoPanel = true; }
      if (e === 3) { $scope.showLastName = true; $scope.showPersonalInfoPanel = true; }
      if (e === 4) { $scope.showMiddleName = true; $scope.showPersonalInfoPanel = true; }
      if (e === 5) { $scope.showNickName = true; $scope.showPersonalInfoPanel = true; }
      if (e === 6) { $scope.showGender = true; $scope.showPersonalInfoPanel = true; }
      if (e === 10) { $scope.showEmail = true; $scope.showContactInfoPanel = true; }
      if (e === 11) { $scope.showMobileNo = true; $scope.showContactInfoPanel = true; }
      if (e === 12) { $scope.showTitle = true; $scope.showOtherlInfoPanel = true; }
      if (e === 13) { $scope.showProfession = true; $scope.showOtherlInfoPanel = true; }
      if (e === 14) { $scope.showCompany = true; $scope.showOtherlInfoPanel = true; }
      if (e === 15) { $scope.showEducation = true; $scope.showOtherlInfoPanel = true; }
      if (e === 16) { $scope.showStreetAddress1 = true; $scope.showContactInfoPanel = true; }
      if (e === 17) { $scope.showStreetAddress2 = true; $scope.showContactInfoPanel = true; }
      if (e === 18) { $scope.showCityAddress = true; $scope.showContactInfoPanel = true; }
      if (e === 19) { $scope.showRegionAddress = true; $scope.showContactInfoPanel = true; }
      if (e === 20) { $scope.showCountry = true; $scope.showContactInfoPanel = true; }
      if (e === 21) { $scope.showZip = true; $scope.showContactInfoPanel = true; }
      if (e === 22) { $scope.showReligion = true; $scope.showOtherlInfoPanel = true; }
      if (e === 23) { $scope.showFacebookPage = true; $scope.showSocialInfoPanel = true; }
      if (e === 24) { $scope.showTwitterHandle = true; $scope.showSocialInfoPanel = true; }
      if (e === 25) { $scope.showInstagramHandle = true; $scope.showSocialInfoPanel = true; }
      if (e === 27) { $scope.showbirthDate = true; $scope.showPersonalInfoPanel = true; }
    });
  }

  getEventDetails();


  //Scope Variables
  $scope.guest = {};
  $scope.fbEnabled;
  $scope.fbRequired = false;  
  $scope.showRFIDError;  

  $scope.disableShare = true;
  $scope.disableExit = false;

  $scope.eventID = $routeParams.message


  //Scope Functions
  $scope.connectFB = function() {
    $scope.facebookLoading = true;
    $scope.disableFacebook = true;

    FB.login(function(responseStat) {
      console.log('Facebook Response : ' + JSON.stringify(responseStat));
      if (responseStat.status == "connected") {
        FB.api("/me?access_token=" + responseStat.authResponse.accessToken, function(response) {
          if (response && !response.error) {

            $scope.facebookLoading = false;
            $scope.disableFacebook = false;
            $scope.$apply(function() {
              $scope.fbEnabled = true;
            });

            if ($scope.showFirstName) {
              angular.element("#firstName").val(response.first_name);
              $scope.guest.firstName = response.first_name;
            }

            if ($scope.showLastName) {
              angular.element("#lastName").val(response.last_name);
              $scope.guest.lastName = response.last_name;
            }

            if ($scope.showEmail) {
              angular.element("#email").val(response.email);
              $scope.guest.email = response.email;
            }

            if ($scope.showbirthDate) {
              if(response.birthday != null) {
                angular.element("#birthDate").val(response.birthday.split('/')[2] + '-' + response.birthday.split('/')[0] + '-' + response.birthday.split('/')[1]);
                $scope.guest.birthDate = response.birthday.split('/')[2] + '-' + response.birthday.split('/')[0] + '-' + response.birthday.split('/')[1];
              }
            }

            if ($scope.showGender) {
              if (response.gender == "female") {
                angular.element("#female").prop("checked", true);
                $scope.guest.gender = "Female";
              }
              else if (response.gender == "male") {
                  angular.element("#male").prop("checked", true);
                $scope.guest.gender = "Male";
              }
            }

            $scope.guest.fbToken = responseStat.authResponse.accessToken;
            $scope.guest.fbUsername = response.id;
            info.setGuest($scope.guest);
          }
          
          else {
            $('.messagealert').click(function() {
              location.reload();
            });
            
            $scope.facebookLoading = false;
            $scope.disableFacebook = false;

            $("#messagealertbody").html("Connection to Facebook failed. Please try again.");
            $("#messagealertheader").html("Facebook account needed");
            $("#messagealert").modal(options);
          }
        });
      }
      else {
        
        $('.messagealert').click(function() {
          location.reload();
        });

        $scope.facebookLoading = false;
        $scope.disableFacebook = false;
                
        $("#messagealertbody").html("Connection to Facebook failed. Please try again.");
        $("#messagealertheader").html("Facebook account needed");
        $("#messagealert").modal(options);
      }
    }, {scope:'public_profile,publish_stream,email,user_about_me,publish_actions'});
  };

  $scope.submitRegistrationForm = function() {

    $scope.disableSubmit = true;

    //Validation could be app specific
    validateFormDetails(function(err, proceedToGetRFID, guestInfo){

      $scope.disableSubmit = false;
      $scope.submitLoading = false;

      //error in validation
      if(err){                
        $("#messagealert").modal(options);
      }

      //no error in validation
      else{

        //Issue RFID Step
        if(proceedToGetRFID){          
          $("#userprofile").modal(options);
        }

        //Straight to register user
        else{
          registerGuest(guestInfo, function(err){
            
            //Success
            if (!err) {
              $scope.guest = {};
              $scope.agreement = false;
              $("#thankyouprofile").modal(options);
            }

            //Error On adding new guest
            else{              
              $("#messagealert").modal(options);
            }
          });
        }
      }
    });
  };

  $scope.submitGuestUID = function() {
    $scope.disableRFID = true;
    $scope.rfidLoading = true;

    //Validation could be app specific
    validateGuestUID(function(err){

      //Guest UID Already in user
      if(err){
        $scope.showRFIDError = true;
        $scope.disableRFID = false;
        $scope.rfidLoading = false;
      }

      //Guest UID Valid
      else{
        $scope.showRFIDError = false;

        var finalGuest = info.getGuest();

        finalGuest.eventID = eventDetails.id;
        finalGuest.preRegistered = false;

        if (eventDetails.activationRequired) {
          finalGuest.isPresent = false;          
        }
        else {
          finalGuest.isPresent = true;
          finalGuest.timePresent = new Date().getTime();
        }
        // finalGuest.timeRegistered = new Date().getTime();
        finalGuest.uID = $scope.guest.uID.toUpperCase();

        registerGuest(finalGuest, function(err){

          //Success
          if (!err) {
            //created transactions after registration could be app specific
            createTransactionsAfterRegistration(finalGuest);
          }

          //Error On adding new guest
          else{            
            $("#userprofile").modal('hide');
            $scope.disableRFID = false;
            $scope.rfidLoading = false;
            $scope.guest.uID = "";            
            $("#messagealert").modal(options);
          }
        });

      }
    });
  };

  $scope.showTermsAndConditionsModal = function() {    
    $("#termsAndConditionsModal").modal(options);
  };

  $scope.checkFacebook = function() {
    return false;
  };

  $scope.shareFB = function() {
    $scope.disableShare = true;
    $scope.exitLoading = true;
    FB.api("/me/feed","POST",{
      "place":"798014076894670",
      "message": 'Mga dewds! Dito na ako sa Circuit Makati. Kita kits! #dewignite',
      // "link": "https://www.facebook.com/mountaindewPH"
    }, function(response) {
        if (response && !response.error) {
          
            FB.logout(function(data){

          $scope.disableShare = false;
          $scope.exitLoading = false;
              $("#thankyouprofile").modal('hide');
              $scope.fbEnabled = false;
          info.setGuest({});
          location.reload();
            });
          }
          else {
            FB.logout(function(data){

          $scope.disableShare = false;
          $scope.exitLoading = false;
              $("#thankyouprofile").modal('hide');
              $scope.fbEnabled = false;
          info.setGuest({});
          location.reload();
            });
          }
    })
  };

  $scope.noThanks = function() {
    $scope.disableExit = true;
    $scope.exitLoading = true;
    if (info.getGuest().fbToken == null) {

      $scope.disableExit = true;
      $scope.exitLoading = true;
      $("#thankyouprofile").modal('hide');
      info.setGuest({});
      location.reload();
    }
    else {
      FB.logout(function(data){

        $scope.disableExit = true;
        $scope.exitLoading = true;
        $("#thankyouprofile").modal('hide');
              $scope.fbEnabled = false;
        info.setGuest({});
        location.reload();
      });
    }
  };


  //Scope Function Helpers -- could be app specific  
  function validateFormDetails(callback){

    //Checked Agreement
    if ($scope.agreement === true) {

      //Complete Details
      if (($scope.guest.firstName != "") && ($scope.guest.lastName != "") && ($scope.guest.birthDate != "") && ($scope.guest.email != "") && ($scope.guest.gender != "") && (!$scope.guest.birthDate.match(/[a-z]/i))) {
        
        //checked test ride
        if($scope.guest.testride == "Yes"){
          info.setGuest($scope.guest);
          callback(null, true, $scope.guest);          
        }

        //did not check test ride
        else {
          //FB Required
          if ($scope.fbRequired == true) {

            //Got FB Details
            if ($scope.fbEnabled == true) {              
              $scope.guest.eventID = eventDetails.id;
              $scope.guest.preRegistered = false;              
              
              if (eventDetails.activationRequired) {
                $scope.guest.isPresent = false;          
              }
              else {
                $scope.guest.isPresent = true;
                $scope.guest.timePresent = new Date().getTime();
              }
              // finalGuest.timeRegistered = new Date().getTime();
              callback(null, false, $scope.guest);

            }
            
            //Failed getting FB Details
            else {

              $("#messagealertbody").html("Please sign up using your facebook account. Thanks!");
              $("#messagealertheader").html("Facebook account needed");
              callback(true, false, null);
            }
          }


          //FB Not Required
          else {
            $scope.guest.eventID = eventDetails.id;            
            $scope.guest.preRegistered = false;
            if (eventDetails.activationRequired) {
              $scope.guest.isPresent = false;          
            }
            else {
              $scope.guest.isPresent = true;
              $scope.guest.timePresent = new Date().getTime();
            }

            callback(null, false, $scope.guest);

          }
        }
      }

      //Incomplete Details
      else {
        $("#messagealertbody").html("One or more fields is/are empty or in the wrong format. Please try again.");
        $("#messagealertheader").html("Invalid fields");
        callback(true, false, null);        
      }
    }

    //Did not check Agreement
    else {
      $("#messagealertbody").html("You need to check the terms and conditions to proceed. Thanks.");
      $("#messagealertheader").html("Please read terms and conditions");
      callback(true, false, null);
    }
  }

  function validateGuestUID(callback){
    $http({
      url: '/getGuestDetails/',
      method: 'GET',
      params: {guestID: null, uID: $scope.guest.uID.toUpperCase(), isGuestID: false}
    }).success(function(data){
      if ((data.status == "OK") && (data.data != null)) {
        callback("Error - UID already in use");
      }
      else{
        callback(null);
      }      
    });
  }  

  function registerGuest(guestInfo, callback){
    $http({
      method: 'POST',
      url: '/addGuest',
      data: {newGuest: guestInfo}
    }).success(function(data, status, headers, config) {

      if (data.status == "OK") {
        callback(null);
      }
      else if (data.status == "ERROR1") {
        $("#messagealertbody").html(data.message);    
        $("#messagealertheader").html("Facebook registration error");                
        callback('Error');
      }
      else if (data.status == "ERROR") {
        $("#messagealertbody").html("This " + data.data.split(' ')[0] + " has already been registered. Please use another email to complete your registration. Thank you.");
        $("#messagealertheader").html("Email already in use");
        callback('Error');        
      }

      //Some other error
      else{
        $("#messagealertbody").html("Some other error occured -  " + data);
        $("#messagealertheader").html("Error");
        callback('Error');        
      }
    });
  }

  function createTransactionsAfterRegistration(guestInfo){
    if (guestInfo.testride == "Yes") {
      $http({
        method: 'POST',
        url: '/recordTransaction',
        //Booth ID and credit data ID of survey credit
        data: {eventID: eventDetails.id, boothID: 7, creditDataID: 2, uID: guestInfo.uID, timestamp: new Date().getTime()}
      }).success(function(data) {
        if(data.status == "OK") {          
          $http({
            method: 'POST',
            url: '/recordTransaction',
            //Booth ID and credit data ID of raffle credit
            data: {eventID: eventDetails.id, boothID: 6, creditDataID: 1, uID: guestInfo.uID, timestamp: new Date().getTime()}
          }).success(function(data) {
            if(data.status == "OK") {
              console.log(data.data);
            }
          });
        }
      });
    }
    else {
      $http({
        method: 'POST',
        url: '/recordTransaction',
        //Booth ID and credit data ID of raffle credit
        data: {eventID: eventDetails.id, boothID: 6, creditDataID: 1, uID: guestInfo.uID, timestamp: new Date().getTime()}
      }).success(function(data) {
        if(data.status == "OK") {
          console.log(data.data);
        }
      });
    }

    $scope.disableRFID = false;
    $scope.rfidLoading = false;
    $scope.guest = {};
    $scope.agreement = false;
    $scope.guest.uID = "";
    $("#userprofile").modal('hide');    
    $("#thankyouprofile").modal(options);
  }


  //Aux
  $("#birthDate").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});

});

registerapp.controller("registerController", function($scope, $http, $routeParams) {
  

});

registerapp.controller("emptyController", function($scope, $http, $routeParams) {
});
