
registerapp.controller("registerController", function($scope, $http, $routeParams) {

});

registerapp.controller("formController", function($scope, $http, $routeParams, $rootScope, info) {
  $scope.guest = {};
  $scope.fbEnabled;
    $scope.fbRequired = false;
    $scope.hideRFID;
  $rootScope.overwrite;
  
  // $scope.fbEnabled = false;
  // $scope.fbRequired = false;

  $("#birthDate").inputmask("yyyy-mm-dd", {"placeholder": "yyyy-mm-dd"});

  window.fbAsyncInit = function() {
      FB.init({
        appId      : '619566324728121',
        xfbml      : true,
        version    : 'v2.0'
      });
  };

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "//connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

  $scope.event = {};

  if (($routeParams.message != null) || ($routeParams.message != "thankyou")) {
    $scope.hideRFID = false;

    $scope.showFirstName = false;
    $scope.showLastName = false;
    $scope.showMiddleName = false;
    $scope.showNickName = false;
    $scope.showGender = false;
    $scope.showEmail = false;
    $scope.showMobileNo = false;
    $scope.showTitle = false;
    $scope.showProfession = false;
    $scope.showCompany = false;
    $scope.showEducation = false;
    $scope.showStreetAddress1 = false;
    $scope.showStreetAddress2 = false;
    $scope.showCityAddress = false;
    $scope.showRegionAddress = false;
    $scope.showCountry = false;
    $scope.showZip = false;
    $scope.showReligion = false;
    $scope.showFacebookPage = false;
    $scope.showTwitterHandle = false;
    $scope.showInstagramHandle = false;
    $scope.showbirthDate = false;

    $scope.fbEnabled = false;

    if ($routeParams.message == 'nan') {
      $scope.showFirstName = true;
      $scope.showLastName = true;
      $scope.showGender = true;
      $scope.showEmail = true;
      $scope.showMobileNo = true;
      $scope.showbirthDate = true;
      $("body").css('background-image','url(img/blackwallpaper.jpeg)');
      $("#sanmigLogo").attr('src', 'img/logo/gooralogo1.png');
      $scope.event.id = 1;
    }
    else {
      $http({
        method: 'GET',
        url: '/getEventDetails',
        params: {eventID: $routeParams.message}
      }).success(function(data, status, headers, config) {
        if (data.status == "OK") {
          $scope.event = data.data;
          $("#sanmigLogo").attr('src', 'img/yamahalogo.png');
          $("body").css('background-image','url(img/yamaha.jpg)');
          // $("body").css('background-image','url(img/sample_preregsite.jpg)');
          var guestDetails = data.data.guestDetailsRequired;
          angular.forEach(guestDetails, function(element) {
            if (element == 2) $scope.showFirstName = true;
            if (element == 3) $scope.showLastName = true;
            if (element == 4) $scope.showMiddleName = true;
            if (element == 5) $scope.showNickName = true;
            if (element == 6) $scope.showGender = true;
            if (element == 10) $scope.showEmail = true;
            if (element == 11) $scope.showMobileNo = true;
            if (element == 12) $scope.showTitle = true;
            if (element == 13) $scope.showProfession = true;
            if (element == 14) $scope.showCompany = true;
            if (element == 15) $scope.showEducation = true;
            if (element == 16) $scope.showStreetAddress1 = true;
            if (element == 17) $scope.showStreetAddress2 = true;
            if (element == 18) $scope.showCityAddress = true;
            if (element == 19) $scope.showRegionAddress = true;
            if (element == 20) $scope.showCountry = true;
            if (element == 21) $scope.showZip = true;
            if (element == 22) $scope.showReligion = true;
            if (element == 23) $scope.showFacebookPage = true;
            if (element == 24) $scope.showTwitterHandle = true;
            if (element == 25) $scope.showInstagramHandle = true;
            if (element == 27) $scope.showbirthDate = true;
          });
        }
        else {
          console.log(data);
        }
      }); 
    }
  } 

  $scope.submitForm = function() {
    if ($scope.agreement == true) {
      if (($scope.guest.firstName != "") && ($scope.guest.lastName != "") && ($scope.guest.birthDate != "") && ($scope.guest.email != "") && ($scope.guest.gender != "") && (!$scope.guest.birthDate.match(/[a-z]/i))) {
        if($scope.guest.testride == "Yes") {
          var options = {
                show: true,
                keyboard: false,
                backdrop: 'static'
            }
            $("#userprofile").modal(options);
            $rootScope.overwrite = false;
            info.setGuest($scope.guest);
        }
        else {
          if ($scope.fbRequired == true) {
          if ($scope.fbEnabled == true) {
            var finalGuest = info.getGuest();
            finalGuest.eventID = $scope.event.id;
            finalGuest.preRegistered = true;
            finalGuest.isPresent = false;
            // finalGuest.timeRegistered = new Date().getTime();
            $http({
              method: 'POST',
              url: '/addGuest',
              data: {newGuest: finalGuest}
            }).success(function(data, status, headers, config) {
              console.log(data);
              if (data.status == "OK") {
                $scope.disableSubmit = false;
                $scope.submitLoading = false;
                $scope.guest = {};
                $scope.agreement = false;
                var options = {
                      show: true,
                      keyboard: false,
                      backdrop: 'static'
                  }
                  // $("#messagealertbody").html("Thank you for signing up!");
                  // $("#messagealertheader").html("Registration successful");
                  $("#thankyouprofile").modal(options);
              }
              else if (data.status == "ERROR1") {
                $scope.disableSubmit = false;
                $scope.submitLoading = false;
                var options = {
                      show: true,
                      keyboard: false,
                      backdrop: 'static'
                  }
                $("#messagealertbody").html(data.message);
                  $("#messagealertheader").html("Facebook registration error");
                  $("#messagealert").modal(options);
              }
              else if (data.status == "ERROR") {
                console.log('padis')

                $scope.disableSubmit = false;
                $scope.submitLoading = false;
                var options = {
                      show: true,
                      keyboard: false,
                      backdrop: 'static'
                  }
                $("#messagealertbody").html("This " + data.data.split(' ')[0] + " has already been registered. Please use another email to complete your registration. Thank you.");
                  $("#messagealertheader").html("Facebook account registered already");
                  $("#messagealert").modal(options);
              }
            }); 
          }
          else {

            $scope.disableSubmit = false;
            $scope.submitLoading = false;
            var options = {
                  show: true,
                  keyboard: false,
                  backdrop: 'static'
              }
            $("#messagealertbody").html("Please sign up using your facebook account. Thanks!");
              $("#messagealertheader").html("Facebook account needed");
              $("#messagealert").modal(options);
          }
        }
        else {
          $scope.guest.eventID = $scope.event.id;
          $scope.guest.preRegistered = true;
          $scope.guest.isPresent = false;
          // $scope.guest.timeRegistered = new Date().getTime();
          $http({
            method: 'POST',
            url: '/addGuest',
            data: {newGuest: $scope.guest}
          }).success(function(data, status, headers, config) {
            if (data.status == "OK") {
              console.log(data)
              $scope.disableSubmit = false;
              $scope.submitLoading = false;
              $scope.guest = {};
              $scope.agreement = false;
              var options = {
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                }
                // $("#messagealertbody").html("Thank you for signing up!");
                // $("#messagealertheader").html("Registration successful");
                $("#thankyouprofile").modal(options);
            }
            else {
              console.log(data.data)

              $scope.disableSubmit = false;
              $scope.submitLoading = false;
              var options = {
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                }
              $("#messagealertbody").html("This " + data.data.split(' ')[0] + " has already been registered. Please use another email to complete your registration. Thank you.");
                $("#messagealertheader").html("Facebook account registered already");
                $("#messagealert").modal(options);
            }
          }); 
        }
        }
      }
      else {
        var options = {
              show: true,
              keyboard: false,
              backdrop: 'static'
          }
        $("#messagealertbody").html("One or more fields is/are empty or in the wrong format. Please try again.");
          $("#messagealertheader").html("Invalid fields");
          $("#messagealert").modal(options);
      }
    }
    else {
      var options = {
            show: true,
            keyboard: false,
            backdrop: 'static'
        }
      $("#messagealertbody").html("You need to check the terms and conditions to proceed. Thanks.");
        $("#messagealertheader").html("Facebook account needed");
        $("#messagealert").modal(options);
    }
  }

  $scope.goBackToInputRFID = function() {
        $scope.hideRFID = false;
        $scope.guest.uID = "";
        $rootScope.overwrite = false;
    }

  $scope.submitwithRFID = function() {
    $scope.disableRFID = true;
    $scope.rfidLoading = true;
    $rootScope.overwrite = false;
    if ($rootScope.overwrite == false) {
      $.ajax({
        method: 'GET',
        url: '/getGuestDetails',
        data: {guestID: null, uID: $scope.guest.uID.toUpperCase(), isGuestID: false}
      }).done(function(data) {
        if ((data.status == "OK") && (data.data != null)) {
          $scope.$apply(function() {
            $scope.hideRFID = true;
            $scope.disableRFID = false;
            $scope.rfidLoading = false;
          });
        }
        else {
          $rootScope.overwrite = false;
          $scope.hideRFID = false;
          if ($scope.fbRequired == true) {
            if ($scope.fbEnabled == true) {
              var finalGuest = info.getGuest();
              finalGuest.eventID = $scope.event.id;
              finalGuest.preRegistered = false;
              if ($scope.event.activationRequired) {
                finalGuest.isPresent = false;
              }
              else {
                finalGuest.isPresent = true;
              }
              // finalGuest.timeRegistered = new Date().getTime();
              finalGuest.uID = $scope.guest.uID.toUpperCase();
              $http({
                method: 'POST',
                url: '/addGuest',
                data: {newGuest: finalGuest}
              }).success(function(data, status, headers, config) {
                if (data.status == "OK") {
                  //Yamaha specific
                  if (finalGuest.testride == "Yes") {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of survey credit
                      data: {eventID: $scope.event.id, boothID: 7, creditDataID: 2, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                        $http({
                          method: 'POST',
                          url: '/recordTransaction',
                          //Booth ID and credit data ID of raffle credit
                          data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                        }).success(function(data) {
                          if(data.status == "OK") {
                            console.log(data.data);
                          }
                        });
                      }
                    });
                  }
                  else {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of raffle credit
                      data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                      }
                    });
                  }

                  $scope.disableRFID = false;
                  $scope.rfidLoading = false;
                  $scope.guest = {};
                  $scope.agreement = false;

                  $scope.guest.uID = "";
                    $("#userprofile").modal('hide');
                  var options = {
                        show: true,
                        keyboard: false,
                        backdrop: 'static'
                    }
                    $("#thankyouprofile").modal(options);
                }
                else if (data.status == "ERROR1") {
                  $scope.disableRFID = false;
                  $scope.rfidLoading = false;

                  $scope.guest.uID = "";
                    $("#userprofile").modal('hide');
                  var options = {
                        show: true,
                        keyboard: false,
                        backdrop: 'static'
                    }
                  $("#messagealertbody").html(data.message);
                    $("#messagealertheader").html("Facebook registration error");
                    $("#messagealert").modal(options);
                }
                else {
                  $scope.disableRFID = false;
                  $scope.rfidLoading = false;
                  $scope.guest.uID = "";
                    $("#userprofile").modal('hide');

                  var options = {
                        show: true,
                        keyboard: false,
                        backdrop: 'static'
                    }
                  $("#messagealertbody").html("This " + data.data.split(' ')[0] + " has already been registered. Please use another email to complete your registration. Thank you.");
                    $("#messagealertheader").html("Facebook account registered already");
                    $("#messagealert").modal(options);
                }
              }); 
            }
            else {
              $scope.disableRFID = false;
              $scope.rfidLoading = false;

              $scope.guest.uID = "";
                $("#userprofile").modal('hide');
              var options = {
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                }
              $("#messagealertbody").html("Please sign up using your facebook account. Thanks!");
                $("#messagealertheader").html("Facebook account needed");
                $("#messagealert").modal(options);
            }
          }
          else {
            var finalGuest = info.getGuest();
            finalGuest.eventID = $scope.event.id;
            finalGuest.preRegistered = false;
            if ($scope.event.activationRequired) {
              finalGuest.isPresent = false;
            }
            else {
              finalGuest.isPresent = true;
            }
            // finalGuest.timeRegistered = new Date().getTime();
            finalGuest.uID = $scope.guest.uID.toUpperCase();
            $http({
              method: 'POST',
              url: '/addGuest',
              data: {newGuest: finalGuest}
            }).success(function(data, status, headers, config) {
              if (data.status == "OK") {
                //Yamaha specific
                  if (finalGuest.testride == "Yes") {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of survey credit
                      data: {eventID: $scope.event.id, boothID: 7, creditDataID: 2, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                        $http({
                          method: 'POST',
                          url: '/recordTransaction',
                          //Booth ID and credit data ID of raffle credit
                          data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                        }).success(function(data) {
                          if(data.status == "OK") {
                            console.log(data.data);
                          }
                        });
                      }
                    });
                  }
                  else {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of raffle credit
                      data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                      }
                    });
                  }


                $scope.disableRFID = false;
                $scope.rfidLoading = false;
                $scope.guest = {};
                $scope.agreement = false;

                $scope.guest.uID = "";
                  $("#userprofile").modal('hide');
                var options = {
                      show: true,
                      keyboard: false,
                      backdrop: 'static'
                  }
                  $("#thankyouprofile").modal(options);
              }
              else {
                $scope.disableRFID = false;
                $scope.rfidLoading = false;

                $scope.guest.uID = "";
                  $("#userprofile").modal('hide');
                var options = {
                      show: true,
                      keyboard: false,
                      backdrop: 'static'
                  }
                $("#messagealertbody").html("This " + data.data.split(' ')[0] + " has already been registered. Please use another email to complete your registration. Thank you.");
                  $("#messagealertheader").html("Facebook account registered already");
                  $("#messagealert").modal(options);
              }
            }); 
          }
        }
      })
    }
    else {
      $rootScope.overwrite = false;
      if ($scope.fbRequired == true) {
        if ($scope.fbEnabled == true) {
          var finalGuest = info.getGuest();
          finalGuest.eventID = $scope.event.id;
          finalGuest.preRegistered = false;
          if ($scope.event.activationRequired) {
            finalGuest.isPresent = false;
          }
          else {
            finalGuest.isPresent = true;
          }
          // finalGuest.timeRegistered = new Date().getTime();
          finalGuest.uID = $scope.guest.uID.toUpperCase();
          $http({
            method: 'POST',
            url: '/addGuest',
            data: {newGuest: finalGuest}
          }).success(function(data, status, headers, config) {
            if (data.status == "OK") {
              //Yamaha specific
                  if (finalGuest.testride == "Yes") {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of survey credit
                      data: {eventID: $scope.event.id, boothID: 7, creditDataID: 2, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                        $http({
                          method: 'POST',
                          url: '/recordTransaction',
                          //Booth ID and credit data ID of raffle credit
                          data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                        }).success(function(data) {
                          if(data.status == "OK") {
                            console.log(data.data);
                          }
                        });
                      }
                    });
                  }
                  else {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of raffle credit
                      data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                      }
                    });
                  }

              $scope.disableRFID = false;
              $scope.rfidLoading = false;
              $scope.guest = {};
              $scope.agreement = false;

              $scope.guest.uID = "";
                $("#userprofile").modal('hide');
              var options = {
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                }
                $("#thankyouprofile").modal(options);
            }
            else if (data.status == "ERROR1") {
              $scope.disableRFID = false;
              $scope.rfidLoading = false;

              $scope.guest.uID = "";
                $("#userprofile").modal('hide');
              var options = {
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                }
              $("#messagealertbody").html(data.message);
                $("#messagealertheader").html("Facebook registration error");
                $("#messagealert").modal(options);
            }
            else {
              $scope.disableRFID = false;
              $scope.rfidLoading = false;
              $scope.guest.uID = "";
                $("#userprofile").modal('hide');

              var options = {
                    show: true,
                    keyboard: false,
                    backdrop: 'static'
                }
              $("#messagealertbody").html("This " + data.data.split(' ')[0] + " has already been registered. Please use another email to complete your registration. Thank you.");
                $("#messagealertheader").html("Facebook account registered already");
                $("#messagealert").modal(options);
            }
          }); 
        }
        else {
          $scope.disableRFID = false;
          $scope.rfidLoading = false;

          $scope.guest.uID = "";
            $("#userprofile").modal('hide');
          var options = {
                show: true,
                keyboard: false,
                backdrop: 'static'
            }
          $("#messagealertbody").html("Please sign up using your facebook account. Thanks!");
            $("#messagealertheader").html("Facebook account needed");
            $("#messagealert").modal(options);
        }
      }
      else {
        var finalGuest = info.getGuest();
        finalGuest.eventID = $scope.event.id;
        finalGuest.preRegistered = false;
        if ($scope.event.activationRequired) {
          finalGuest.isPresent = false;
        }
        else {
          finalGuest.isPresent = true;
        }
        // finalGuest.timeRegistered = new Date().getTime();
        finalGuest.uID = $scope.guest.uID.toUpperCase();
        $http({
          method: 'POST',
          url: '/addGuest',
          data: {newGuest: finalGuest}
        }).success(function(data, status, headers, config) {
          if (data.status == "OK") {
            //Yamaha specific
                  if (finalGuest.testride == "Yes") {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of survey credit
                      data: {eventID: $scope.event.id, boothID: 7, creditDataID: 2, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                        $http({
                          method: 'POST',
                          url: '/recordTransaction',
                          //Booth ID and credit data ID of raffle credit
                          data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                        }).success(function(data) {
                          if(data.status == "OK") {
                            console.log(data.data);
                          }
                        });
                      }
                    });
                  }
                  else {
                    $http({
                      method: 'POST',
                      url: '/recordTransaction',
                      //Booth ID and credit data ID of raffle credit
                      data: {eventID: $scope.event.id, boothID: 6, creditDataID: 1, uID: finalGuest.uID, timestamp: new Date().getTime()}
                    }).success(function(data) {
                      if(data.status == "OK") {
                        console.log(data.data);
                      }
                    });
                  }

            $scope.disableRFID = false;
            $scope.rfidLoading = false;
            $scope.guest = {};
            $scope.agreement = false;

            $scope.guest.uID = "";
              $("#userprofile").modal('hide');
            var options = {
                  show: true,
                  keyboard: false,
                  backdrop: 'static'
              }
              $("#thankyouprofile").modal(options);
          }
          else {
            $scope.disableRFID = false;
            $scope.rfidLoading = false;

            $scope.guest.uID = "";
              $("#userprofile").modal('hide');
            var options = {
                  show: true,
                  keyboard: false,
                  backdrop: 'static'
              }
            $("#messagealertbody").html("This " + data.data.split(' ')[0] + " has already been registered. Please use another email to complete your registration. Thank you.");
              $("#messagealertheader").html("Facebook account registered already");
              $("#messagealert").modal(options);
          }
        }); 
      }
    }
  }

  $scope.goBackToInputRFID = function() {
      $("#messagealert").modal('hide');
    var options = {
          show: true,
          keyboard: false,
          backdrop: 'static'
      }
      $("#userprofile").modal(options);
  }

  $scope.showModal = function() {
      var options = {
          show: true,
          keyboard: false,
          backdrop: 'static'
      }
      $("#basicModal-displaytime").modal(options);
  }

  $scope.checkFacebook = function() {
    // if (info.getGuest().fbToken == null) {
    //  return false;
    // }
    // else {
    //  return true;
    // }
    return false;
  }

  $scope.noThanks = function() {
    $scope.disableExit = true;
    $scope.exitLoading = true;
    if (info.getGuest().fbToken == null) {

      $scope.disableExit = true;
      $scope.exitLoading = true;
      $("#thankyouprofile").modal('hide');
      info.setGuest({});
      location.reload();
    }
    else {
      FB.logout(function(data){

        $scope.disableExit = true;
        $scope.exitLoading = true;
        $("#thankyouprofile").modal('hide');
              $scope.fbEnabled = false;
        info.setGuest({});
        location.reload();
      });
    }
  }

  $scope.connectFB = function() {
    $scope.facebookLoading = true;
    $scope.disableFacebook = true;
    FB.login(function(responseStat) {
        console.log('Facebook Response : ' + JSON.stringify(responseStat));
          if (responseStat.status == "connected") {
        FB.api("/me?access_token=" + responseStat.authResponse.accessToken, function(response) {
                if (response && !response.error) {
                  $scope.facebookLoading = false;
                  $scope.disableFacebook = false;
                  $scope.$apply(function() {
                $scope.fbEnabled = true;
            });
                  if ($scope.showFirstName) {
                    angular.element("#firstName").val(response.first_name);
                    $scope.guest.firstName = response.first_name;
                  }
                  if ($scope.showLastName) {
                    angular.element("#lastName").val(response.last_name);
                    $scope.guest.lastName = response.last_name;
                  }
                  if ($scope.showEmail) {
                    angular.element("#email").val(response.email);
                    $scope.guest.email = response.email;
                  }
                  if ($scope.showbirthDate) {
                    if(response.birthday != null) {
                      angular.element("#birthDate").val(response.birthday.split('/')[2] + '-' + response.birthday.split('/')[0] + '-' + response.birthday.split('/')[1]);
                      $scope.guest.birthDate = response.birthday.split('/')[2] + '-' + response.birthday.split('/')[0] + '-' + response.birthday.split('/')[1];
                    }
                  }
                  if ($scope.showGender) {
                    if (response.gender == "female") {
                      angular.element("#female").prop("checked", true);
                      $scope.guest.gender = "Female";
                    }
                    else if (response.gender == "male") {
                        angular.element("#male").prop("checked", true);
                      $scope.guest.gender = "Male";
                    }
                  }
                  $scope.guest.fbToken = responseStat.authResponse.accessToken;
                  $scope.guest.fbUsername = response.id;
                  info.setGuest($scope.guest);
                }
                else {
                  $('.messagealert').click(function() {
                    location.reload();
                  });
                  $scope.facebookLoading = false;
                  $scope.disableFacebook = false;
                  var options = {
                  show: true,
                  keyboard: false,
                  backdrop: 'static'
              }
            $("#messagealertbody").html("Connection to Facebook failed. Please try again.");
              $("#messagealertheader").html("Facebook account needed");
              $("#messagealert").modal(options);
                }
            });
      }
      else {
        $('.messagealert').click(function() {
                location.reload();
              });
        $scope.facebookLoading = false;
            $scope.disableFacebook = false;
        var options = {
              show: true,
              keyboard: false,
              backdrop: 'static'
          }
        $("#messagealertbody").html("Connection to Facebook failed. Please try again.");
          $("#messagealertheader").html("Facebook account needed");
          $("#messagealert").modal(options);
      }
      }, {scope:'public_profile,publish_stream,email,user_about_me,publish_actions'});
  }

  $scope.shareFB = function() {
    $scope.disableShare = true;
    $scope.exitLoading = true;
    FB.api("/me/feed","POST",{
      "place":"798014076894670",
      "message": 'Mga dewds! Dito na ako sa Circuit Makati. Kita kits! #dewignite',
      // "link": "https://www.facebook.com/mountaindewPH"
    }, function(response) {
        if (response && !response.error) {
          
            FB.logout(function(data){

          $scope.disableShare = false;
          $scope.exitLoading = false;
              $("#thankyouprofile").modal('hide');
              $scope.fbEnabled = false;
          info.setGuest({});
          location.reload();
            });
          }
          else {
            FB.logout(function(data){

          $scope.disableShare = false;
          $scope.exitLoading = false;
              $("#thankyouprofile").modal('hide');
              $scope.fbEnabled = false;
          info.setGuest({});
          location.reload();
            });
          }
    })
  }
});

registerapp.controller("emptyController", function($scope, $http) {

});
