var socket = io.connect();

$(document).ready(function(){
  $('#drawButton').click(function(){
    socket.emit('draw',{});
  })

  $('#clearButton').click(function(){
    $('#winnersTable').empty();
    socket.emit('clear-winners',{})
  })

  getAllItems();
  // localStorage.clear();
})

socket.on('new-winnerfound', function (data2) {
	var data = data2.winner
    $('#winnersTable').append('<tr><td>'+(new Date()).toLocaleTimeString()+ '</td><td>'  + data.uID + '</td><td>' + data.firstName +'</td><td>'+data.lastName +'</td></tr>')
    saveState('raffledata;' + (new Date()).toLocaleTimeString() + ";" + data.uID + ';' + data.firstName + ';' + data.lastName)
});

function supportsLocalStorage() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
  } catch (e) {
    return false;
  }
}

function saveState(winnerString) {
    if (!supportsLocalStorage()) { alert('Local Storage not supported'); }
    else {
    	var count = 0;
    	for (var i in window.localStorage) {
    		var val = localStorage.getItem(i)
    		count++;
    	}
    	console.log('saving data', winnerString, count)
    	localStorage.setItem(count + 1, winnerString);
    }
}

function getAllItems() {
	 if (!supportsLocalStorage()) { alert('Local Storage not supported'); }
	  else {
    	for (var i in window.localStorage) {
    		var val = localStorage.getItem(i);
    		var value = val.split(";");
    		if (value[0] == 'raffledata')
    		$('#winnersTable').append('<tr><td>'+ value[1]+ '</td><td>'  + value[2] + '</td><td>' + value[3] +'</td><td>'+value[4] +'</td></tr>') 
    	}
    }
}