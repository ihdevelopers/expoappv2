'use strict';

var Event = require('../objects/Event');
var User = require('../objects/User');
var Booth = require('../objects/Booth');
var Transaction = require('../objects/Transaction');


exports.getAllUserEvents = function(req, res){
  console.log("USER : " + JSON.stringify(req.user.id));
  Event.fetchUserEventsAll(req.user.id, function(err, eventList){
    if(err.length === 0){  
      res.send({status : "OK", data : eventList});
    }
    else{ 
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getEventDetails = function(req, res){  
  
  var params = {
    eventID: req.query.eventID    
  };

  if(req.query.regParamsOnly){
    params.regParamsOnly = true;
  }
  else{
    params.regParamsOnly = false;
  }

  Event.fetchEventById(params, function(err, event){
    if(err.length === 0){
      res.send ({status : "OK", data : event});
    }
    else{      
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getAllEventTransactions = function(req, res){
  Event.fetchEventTransactionsAll(req.query.eventID, function(err, transactions){
    if(err.length === 0){
      res.send ({status : "OK", data : transactions});
    }
    else{ 
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.createEvent = function(req, res){
  var newEvent = req.body.newEvent;
  var user = req.user;
  Event.registerNewEvent(newEvent, function(err, newEventID){
    if(err.length === 0){
      User.addEventToUser(user.id, newEventID, function(err2){
        if(err2.length === 0){
          res.send({status : "OK", data : newEventID});
        }
        else{ 
          console.log("ERROR HERE: " + err2[0]); 
          res.send({status : "ERROR", data : null}); 
        }
      });
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : null});
    }
  });
};

exports.getEventEngagementsCount = function(req, res){
  /*var eventID = req.query.eventID;
  Event.getEventEngagementsCount(eventID, function(err, count){
    if(err.length === 0){
      res.send({status : "OK", data : count});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });*/
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Transaction.getEventEngagementsCount(eventID, date, function(err, count){
    if(err.length === 0){
      res.send({status : "OK", data : count});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getEventCheckinsCount = function(req, res){
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Booth.getEventCheckinBooths(eventID, function(err, booths){
    var selectParams = 'timestamp boothID';
    Transaction.getEventTransactionsByBoothID(eventID, date, booths, selectParams, function(err, trans){
        if(err.length === 0){
          res.send({status : "OK", data : trans.length});
        }
        else{
          res.send ({status : "ERROR", data : null}); 
        }
    })
  })
};

exports.getEventClaimsCount = function(req, res){
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Booth.getEventClaimBooths(eventID, function(err, booths){
    var selectParams = 'timestamp';
    Transaction.getEventTransactionsByBoothID(eventID, date, booths, selectParams, function(err, trans){
        if(err.length === 0){
          res.send({status : "OK", data : trans.length});
        }
        else{
          res.send ({status : "ERROR", data : null}); 
        }
    })
  })
};

exports.updateEventDetails = function(req, res){
  var updateParams = req.body;
  Event.updateEventDetails(updateParams, function(err, eventID){
    if(err.length === 0){
      res.send({status : "OK", data : eventID});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  })
};

exports.updateEventBoothDetails = function(req, res){
  var updateParams = req.body;
  Event.updateEventBoothDetails(updateParams, function(err, boothID){
    if(err.length === 0){
      res.send({status : "OK", data : boothID});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  })
};

exports.getEventCheckins = function(req, res){
  var eventID = req.query.eventID;
  Booth.getEventCheckinBooths(eventID, function(err, booths){
    var selectParams = 'timestamp boothID';
    Transaction.getEventTransactionsByBoothID2(eventID, booths, selectParams, function(err, trans){
        if(err.length === 0){
          res.send({status : "OK", data : trans});
        }
        else{
          res.send ({status : "ERROR", data : null}); 
        }
    })
  })
};

exports.getEventClaims = function(req, res){
  var eventID = req.query.eventID;
  Booth.getEventClaimBooths(eventID, function(err, booths){
    var selectParams = 'timestamp';
    Transaction.getEventTransactionsByBoothID2(eventID, booths, selectParams, function(err, trans){
        if(err.length === 0){
          res.send({status : "OK", data : trans});
        }
        else{
          res.send ({status : "ERROR", data : null}); 
        }
    })
  })
};
