'use strict';

var Guest = require('../objects/Guest');
var Event = require('../objects/Event');

exports.getAllEventGuests = function(req, res){
  console.log(req.query);
  var eventID = req.query.eventID;
  var limitedFieldsOnly = req.query.limitedFieldsOnly;
  Guest.fetchEventGuestsAll(eventID, limitedFieldsOnly, function(err, guests){
    if(err.length === 0){
      res.send({status : "OK", data : guests});
    }
    else{
      console.log(err);
      res.send({status : "ERROR", data : null});
    }
  });
};

exports.getGuestDetails = function(req, res){
  console.log(req.query)
  if(req.query.isGuestID == 'true'){
    Guest.fetchGuestById(req.query.guestID, function(err, guestDetails){
      if(err.length === 0){
        res.send({status : "OK", data : guestDetails});
      }
      else{
        console.log("ERROR HERE: " + err[0]);
        res.send({status : "ERROR", data : null});
      }
    });
  }
  else{
    Guest.fetchGuestByUID(req.query.uID, function(err, guestDetails){
      if(err.length === 0){
        res.send({status : "OK", data : guestDetails});
      }
      else{
        console.log("ERROR HERE: " + err[0]);
        res.send({status : "ERROR", data : null});
      }
    });
  }
};

exports.getGuestDetailsByName = function(req, res){
  var guest = req.query;

  Guest.fetchGuestByName(guest, function(err, guestDetails){
    if( (err.length === 0) && (guestDetails !== null) ){
      res.send({status : "OK", data : guestDetails});
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : null});
    }
  });
};

exports.createNewGuest = function(req, res){
  var newGuest = req.body.newGuest;
  console.log(newGuest);
  Guest.registerNewGuest(newGuest, function(err, newGuestID){
    if(err.length === 0){
      Event.addGuestToEvent(newGuest.eventID, newGuestID, function(err2){
        if(err2.length === 0){
          //----------------------------------------
          // ACTIVATE GUEST UPON REG IF APPLICABLE
          //----------------------------------------
          var eventParams = new Object();
          eventParams.eventID = newGuest.eventID;
          eventParams.regParamsOnly = true;

          Event.fetchEventById(eventParams, function(err3, matchingEvent){
            if(err3.length === 0){
              if(matchingEvent != null){
                if(!matchingEvent.activationRequired){
                  Guest.activateGuest(newGuestID, function(err4, id){
                    if(err4.length === 0){
                      res.send({status : "OK", data : newGuestID});
                    }
                    else{ res.send({status : "ERROR", data : null}); }
                  })
                }
                else{ res.send({status : "OK", data : newGuestID}); }
              }
              else{ res.send ({status : "ERROR", data : null}); }
            }
            else{      
              res.send ({status : "ERROR", data : null}); 
            }
          })
        }
        else{
          console.log("ERROR HERE: " + err2[0]);
          res.send({status : "ERROR", data : null});
        }
      });
    }
    else{ 
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : err[0]});
    }
  });
};

exports.updateGuestUID = function(req, res){
  var guest = req.body.guest;
  console.log(guest);

  Guest.updateGuestUID(guest, function(err, guestID){
    if(err.length === 0){
      res.send({status : "OK", data : guestID});
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getRegisteredGuestsCount = function(req, res){
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Guest.getRegisteredGuestsCount(eventID, date, function(err, count){
    if(err.length === 0){
      res.send({status : "OK", data : count});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getPresentGuestsCount = function(req, res){
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Guest.getPresentGuestsCount(eventID, date, function(err, count){
    if(err.length === 0){
      res.send({status : "OK", data : count});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getWalkinGuestsCount = function(req, res){
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Guest.getWalkinGuestsCount(eventID, date, function(err, count){
    if(err.length === 0){
      res.send({status : "OK", data : count});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getPreRegisteredGuestsCount = function(req, res){
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Guest.getPreRegisteredGuestsCount(eventID, date, function(err, count){
    if(err.length === 0){
      res.send({status : "OK", data : count});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.getAnonymousGuestsCount = function(req, res){
  var eventID = req.query.eventID;
  var date = req.query.dateString;
  Guest.getAnonymousGuestsCount(eventID, date, function(err, count){
    if(err.length === 0){
      res.send({status : "OK", data : count});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.updateGuestDetails = function(req, res){
  console.log("in updateGuestDetails");
  var updateParams = req.body;
  console.log(JSON.stringify(updateParams));
  Guest.updateGuestDetails(updateParams, function(err, guestObjId){
    if(err.length === 0){
      res.send({status : "OK", data : guestObjId});
    }
    else{
      res.send ({status : "ERROR", data : null}); 
    }
  });
};
