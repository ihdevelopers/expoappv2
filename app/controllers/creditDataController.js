'use strict';

var CreditData = require('../objects/CreditData');

exports.getCreditDataIDByName = function(req, res){
  var booth = JSON.parse(req.query.booth);
  console.log(booth);
  CreditData.getCreditDataIDByName(booth, function(err, boothID){
    if(err.length === 0){
      res.send({status : "OK", data : boothID});
    }
    else{ 
      console.log("ERROR HERE: " + err[0]); 
      res.send({status : "ERROR", data : null}); 
    }
  });
};
