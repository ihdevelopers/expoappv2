'use strict';

var passport = require('passport');
var User = require('../objects/User');

//-------------------------------
// Authentication
//-------------------------------
exports.signin = function(req, res, next){
  passport.authenticate('local', function(err, user, info){
    if (err || !user) {
      console.log(info);
      res.redirect('/login');
    } else {
      req.login(user, function(err) {
        if (err) {
          res.redirect('/login');
        } else {
          res.redirect('/home');
        }
      });
    }
  })(req, res, next);
};

exports.signout = function(req, res){
  console.log('logging out');
  req.logout();
  res.redirect('/login');
};

exports.validateUser = function(credentials, callback){
  User.validateUser(credentials, callback);
};

exports.fetchUserById = function(userId, callback){
  User.fetchUserById(userId, callback);
};

exports.getUserInfo = function(req, res){
  var user = req.user;  
  User.fetchUserById(user.id, function(err, userInfo){
    if(err.length === 0){  
      res.send({status : "OK", data : userInfo});
    }
    else{ 
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.specialAddAdmin = function(req, res){
  var admin = req.body;
  SuperAdmin.addSuperUser(admin, function(err){
    res.send(err);
  });
};

exports.updateUserDetails = function(req, res){
  var updateParams = req.body;
  User.updateUserDetails(updateParams, function(err, userID){
    if(err.length === 0){  
      res.send({status : "OK", data : userID});
    }
    else{ 
      res.send ({status : "ERROR", data : null}); 
    }
  })
};
