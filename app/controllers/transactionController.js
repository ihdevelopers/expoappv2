'use strict';

var Transaction = require('../objects/Transaction');
var Event = require('../objects/Event');
var Guest = require('../objects/Guest');

exports.getAllEventTransactions = function(req, res){
  Transaction.fetchEventTransactionsAll(req.query.eventID, function(err, transactions){
    if(err.length === 0){
      res.send ({status : "OK", data : transactions});
    }
    else{ 
      res.send ({status : "ERROR", data : null}); 
    }
  });
};

exports.recordTransaction = function(req, res){
  // check if MAX TRANSACTION IS ACHIEVED BY GUEST
  Transaction.checkIfTransactionMaxedOut(req.body, function(initErr, isMaxedOut){
    if(initErr.length === 0){
      if( !isMaxedOut ){
        Transaction.registerNewTransaction(req.body, function(err, newTransactionID, boothType){
          if(err.length === 0){
            // add transaction to event
            Event.addTransactionToEvent(req.body.eventID, newTransactionID, function(err2){
              if(err2.length === 0){
                // add transaction to guest
                Guest.addTransactionToGuestByUID(req.body.uID, newTransactionID, function(err3){
                  if(err3.length === 0){
                   res.send({status : "OK", data : newTransactionID});
                  }
                  else{
                    console.log(err3);
                    res.send({status : "ERROR", data : null}); 
                  }
                }); 
              }
              else{
                console.log(err2);
                res.send({status : "ERROR", data : null}); 
              }
            });  
          }
          else{
            console.log(err);
            res.send({status : "ERROR", data : null});
          }
        });
      }
      else{ res.send({status : "MAXVISITERROR", data : null}); }
    }
    else{
      console.log(initErr);
      res.send ({status : "ERROR", data : null});
    }
  });
};

exports.createNewCreditData = function(req, res){
  console.log(req.body);
  var newCreditData = req.body;

  console.log("asdf " + JSON.stringify(newCreditData.boothIDs.length));

  Transaction.addNewCreditData(newCreditData, function(err, newCreditDataID){
    if(err.length === 0){
      res.send({status : "OK", data : newCreditDataID}); 
    }
    else{ 
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : null});
    }
  });
};

exports.createNewClaimData = function(req, res){
  var newClaimData = req.body;
  appDataProvider.addNewClaimData(newClaimData, function(err, newClaimDataID){
    if(err.length === 0){
      res.send({status : "OK", data : newClaimDataID});
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : null});
    }
  });
};

exports.getAllGuestCredits = function(req, res){
  var guestData = req.query.guestData;
  Transaction.getAllGuestCredits(guestData, function(err, allCredits){
    if(err.length === 0){
      res.send({status : "OK", data : allCredits});
    }
    else{ 
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : null});
    }
  });
};

exports.getAllGuestsWithValidCredits = function(req, res){
  var creditData = req.query.credit;
  Transaction.getAllGuestsWithValidCredits(creditData, function(err, allGuests){
    if(err.length === 0){
      res.send({status : "OK", data : allGuests});
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : null});
    }
  });
};

