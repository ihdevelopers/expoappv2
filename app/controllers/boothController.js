'use strict';

var Booth = require('../objects/Booth');
var Event = require('../objects/Event');
var CreditData = require('../objects/CreditData');

exports.getAllEventBooths = function(req, res){
  var eventID = req.query.eventID;
  Booth.getAllEventBooths(eventID, function(err, allBooths){
    if(err.length === 0){
      res.send({status : "OK", data : allBooths});
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.send ({status : "ERROR", data : null});
    }
  });
};

exports.getBoothIDByName = function(req, res){
  var booth = req.query.booth;
  console.log("in getBoothIDByName")
  console.log(booth);
  Booth.getBoothIDByName(booth, function(err, boothID){
    if(err.length === 0){
      res.send({status : "OK", data : boothID});
    }
    else{ 
      console.log("ERROR HERE: " + err[0]); 
      res.send({status : "ERROR", data : null}); 
    }
  });
};

exports.getBoothDetailsByID = function(req, res){
  var booth = req.body.booth;
  Booth.getBoothDetailsByID(booth, function(err, boothObj){
    if(err.length === 0){
      res.send({status : "OK", data : boothObj});
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.send({status : "ERROR", data : null});
    }
  });
};

exports.getCreditDataIDByName = function(req, res){
  var booth = req.query.booth;
  console.log("in getCreditDataIDByName inside boothControllers.js");
  console.log(booth);
  CreditData.getCreditDataIDByName(booth, function(err, boothID){
    if(err.length === 0){
      res.send({status : "OK", data : boothID});
    }
    else{ 
      console.log("ERROR HERE: " + err[0]); 
      res.send({status : "ERROR", data : null}); 
    }
  });
};

exports.createNewBooth = function(req, res){
  var newBooth = req.body.newBooth;
  Booth.registerNewBooth(newBooth, function(err, newBoothID){
    if(err.length === 0){
      Event.addBoothToEvent(newBooth.eventID, newBoothID, function(err2){
        if(err2.length === 0){
          res.send({status : "OK", data : newBoothID});
        }
        else{
          console.log("ERROR HERE: " + err2[0]); 
          res.send({status : "ERROR", data : null}); 
        }
      });
    }
    else{
      console.log("ERROR HERE: " + err[0]); 
      res.send({status : "ERROR", data : null}); 
    }
  });
};