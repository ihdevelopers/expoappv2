//==========================================================
// APP OBJECT SCHEMAS - EVENT SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var eventDataSchema = mongoose.Schema({
    id  : Number,
    name : String,
    date : Date,
    location : String,
    bannerImage : String,
    logoImage : String,
    backgroundImage : String,
    notes : String, // additional notes regarding the event
    guestEstimate : Number, // estimated number of guests
    organizerID : Number, // infoID of event organizer
    boothIDs : [Number], // list of exhibitorIDs who participated in the event
    guestIDs : [Number], // list of all Guests
    transactionIDs : [Number],
    guestDetailsRequired : [Number], // list of indices of guest details required
    preRegAvailable : Boolean,
    activationRequired : Boolean,
    registrationURL : String
})

eventDataSchema.index( {ID : 1} );

module.exports = mongoose.model('eventDB', eventDataSchema); 