//==========================================================
// APP OBJECT SCHEMAS - CREDIT DATA SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var creditDataSchema = mongoose.Schema({
    id  : Number,
    name : String,
    units : String,
    boothIDs : [Number],
    boothCreditValues : [Number],
    eventID : Number
})

creditDataSchema.index( {ID : 1} );

module.exports = mongoose.model('creditDataDB', creditDataSchema); 