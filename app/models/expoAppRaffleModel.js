//==========================================================
// APP OBJECT SCHEMAS - GUEST SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var raffleDataSchema = mongoose.Schema({
    time: String,
    uID: String,
    firstName: String,
    lastName: String
})

raffleDataSchema.index( {id : 1} );

module.exports = mongoose.model('raffleDB', raffleDataSchema); 