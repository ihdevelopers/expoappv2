//==========================================================
// APP OBJECT SCHEMAS - ORGANIZER SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var userDataSchema = mongoose.Schema({
    id  : Number,
    username : String,
    hash : String,
    email : String,
    mobileNo : String,
    eventIDs : [Number],
    firstName : String,
    lastName : String
})

userDataSchema.index( {id : 1} );

module.exports = mongoose.model('userDB', userDataSchema); 