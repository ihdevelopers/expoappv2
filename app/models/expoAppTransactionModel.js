//==========================================================
// APP OBJECT SCHEMAS - TRANSACTION SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var transactionDataSchema = mongoose.Schema({
    id  : Number,
    boothID : Number,
    guestID : Number,
    eventID : Number,
    transactionDataID : Number,
    timestamp : Date,
    uID : String,
})

transactionDataSchema.index( {ID : 1} );

module.exports = mongoose.model('transactionDB', transactionDataSchema); 