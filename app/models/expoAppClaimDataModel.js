//==========================================================
// APP OBJECT SCHEMAS - CLAIM DATA SCHEMA
//==========================================================
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var claimDataSchema = mongoose.Schema({
  id  : Number,
  name : String,
  creditDataIDs : [Number],
  creditDataPoints : [Number],
  boothIDs : [Number],
  eventID : Number
});

claimDataSchema.index( {ID : 1} );

module.exports = mongoose.model('claimDataDB', claimDataSchema); 