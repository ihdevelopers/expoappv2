//==========================================================
// APP TRANSACTION OBJECT SCHEMAS - CLAIM Trans SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var claimTransDataSchema = mongoose.Schema({
    id  : Number,
    claimDataID : Number,
    guestID : Number,
    uID : String
})

claimTransDataSchema.index( {ID : 1} );

module.exports = mongoose.model('claimTransDB', claimTransDataSchema);