//==========================================================
// APP TRANSACTION OBJECT SCHEMAS - ACTIVATION Trans SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var activationTransDataSchema = mongoose.Schema({
    id  : Number,
    uID : String,
    type : {type : String, enum : ["activation"]}
})

activationTransDataSchema.index( {ID : 1} );

module.exports = mongoose.model('activationTransDB', activationTransDataSchema);