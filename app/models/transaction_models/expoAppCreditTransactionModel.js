//==========================================================
// APP TRANSACTION OBJECT SCHEMAS - CREDIT Trans SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var creditTransDataSchema = mongoose.Schema({
    id  : Number,
    creditDataID : Number,
    creditValue : Number,
    type : {type : String, enum : ["credit"]}
})

creditTransDataSchema.index( {ID : 1} );

module.exports = mongoose.model('creditTransDB', creditTransDataSchema);