//==========================================================
// APP TRANSACTION OBJECT SCHEMAS - CHECKIN Trans SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var checkinTransDataSchema = mongoose.Schema({
    id  : Number,
    uID : String,
    type : {type : String, enum : ["checkin", "activation", "status"]}
})

checkinTransDataSchema.index( {ID : 1} );

module.exports = mongoose.model('checkinTransDB', checkinTransDataSchema);