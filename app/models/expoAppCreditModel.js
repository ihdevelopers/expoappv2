//==========================================================
// APP OBJECT SCHEMAS - CREDIT SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var creditSchema = mongoose.Schema({
    id  : Number,
    totalValue : Number,
    guestID : Number,
    uID: String,
    creditDataID : Number
})

creditSchema.index( {ID : 1} );

module.exports = mongoose.model('creditDB', creditSchema); 