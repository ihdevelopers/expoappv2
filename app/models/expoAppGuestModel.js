//==========================================================
// APP OBJECT SCHEMAS - GUEST SCHEMA
//==========================================================
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var guestDataSchema = mongoose.Schema({
    id  : Number,
    firstName : String,
    lastName : String,
    middleName : String,
    nickName : String,
    gender : { type: String, enum : ['Male', 'Female'] },
    email : String,
    mobileNo : String,
    title : String,
    profession : String,
    company : String,
    education : { type: String, enum : ["Primary", "Secondary", "Tertiary", "Graduate", "Post Graduate"] },
    streetAddress1 : String,
    streetAddress2 : String,
    cityAddress : String,
    regionAddress : String,
    country : String,
    zip : Number,
    religion : String,
    facebookPage : String,
    twitterHandle : String,
    instagramHandle : String,
    uID : String,
    uID2 : String,
    transaction_id : String,
    fb_registration_id : Number,
    isPresent : Boolean,
    creditIDs : [Number], // IDs of related credits
    transactionIDs : [Number], // IDs of related transactions
    eventID: Number,
    preRegistered : Boolean,
    birthDate : String,
    fbToken : String,
    fbUsername : String,
    timeRegistered : Number,
    timePreRegistered : Number,
    timePresent : Number,
    isAnonymous : Boolean
})

guestDataSchema.index( {id : 1} );

module.exports = mongoose.model('guestDB', guestDataSchema); 