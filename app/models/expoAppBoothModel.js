//==========================================================
// APP OBJECT SCHEMAS - BOOTH SCHEMA
//==========================================================
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var boothDataSchema = mongoose.Schema({
  id : Number,
  name : String,
  type : { type: String, enum: ['credit','checkin', 'activation', 'loading', 'claiming', 'survey', 'photobooth', 'raffle','status']},
  eventID : Number,
  creditDataIDs : [Number],
  claimDataIDs : [Number],
  maxTransactionsPerGuest : Number
});

boothDataSchema.index( {ID : 1} );



module.exports = mongoose.model('boothDB', boothDataSchema); 

/*

var BoothData = mongoose.model('boothDB', boothDataSchema);

exports.createBoothData = function(params, callback){
  var boothData = new BoothData(params);
  boothData.save(callback);
};

exports.clearBoothData = function(params, callback){
  BoothData.remove(params, callback);
};

exports.fetchBoothDatas = function(params, callback){
  var populateParams = params.populate;

  if(populateParams){
    BoothData.find(params.find).populate(populateParams).select(params.select).setOptions({lean:true}).exec(callback); 
  }
  else{
    BoothData.find(params.find).select(params.select).setOptions({lean:true}).exec(callback); 
  }
};

exports.fetchBoothData = function(params, callback){
  var populateParams = params.populate;  
  if(populateParams){
    BoothData.findOne(params.find).populate(populateParams).select(params.select).setOptions({lean:false}).exec(callback); 
  }
  else{    
    BoothData.findOne(params.find).select(params.select).setOptions({lean:false}).exec(callback); 
  }
};

exports.BoothData = BoothData;
*/