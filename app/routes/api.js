'use strict';

//var routes = require('../controllers').routeController;
var userController = require('../controllers').userController;
var eventController = require('../controllers').eventController;
var boothController = require('../controllers').boothController;
var transactionController = require('../controllers').transactionController;
var guestController = require('../controllers').guestController;
// var creditDataController = require('../controllers').creditDataController;
var passport = require('passport');
var authentication = require('../objects/Authentication.js');

module.exports = function (app) {

  //--------------------------
  // Authentication APIs
  //--------------------------
  app.post('/login', userController.signin);
  app.get('/logout', userController.signout);


  //--------------------------
  // Admin-level APIs [in Expo App User accounts are ALWAYS ADMIN]
  //--------------------------  
  app.get('/getUserInfo', authentication.ensureAuthenticated, userController.getUserInfo);
  app.post('/specialAddAdmin', userController.specialAddAdmin);


  //--------------------------
  // Event APIs
  //--------------------------
  app.get('/getAllEvents', authentication.ensureAuthenticated, authentication.ensureAdmin, eventController.getAllUserEvents);
  app.get('/getEventDetails', eventController.getEventDetails);
  app.post('/createEvent', authentication.ensureAuthenticated, authentication.ensureAdmin, eventController.createEvent);
  app.get('/getEventEngagementsCount', authentication.ensureAuthenticated, eventController.getEventEngagementsCount);
  app.get('/getEventCheckinsCount', authentication.ensureAuthenticated, eventController.getEventCheckinsCount);
  app.get('/getEventClaimsCount', authentication.ensureAuthenticated, eventController.getEventClaimsCount);
  app.post('/updateEventDetails', authentication.ensureAuthenticated, eventController.updateEventDetails);
  app.post('/updateEventBoothDetails', authentication.ensureAuthenticated, eventController.updateEventBoothDetails);
  app.get('/getEventCheckins', authentication.ensureAuthenticated, eventController.getEventCheckins);
  app.get('/getEventClaims', authentication.ensureAuthenticated, eventController.getEventClaims);

  //--------------------------
  // Booth APIs
  //--------------------------
  app.get('/getAllEventBooths', /*authentication.ensureAuthenticated, authentication.ensureAdmin, */boothController.getAllEventBooths);
  app.get('/getBoothIDByName', boothController.getBoothIDByName);
  app.get('/getBoothDetailsByID', authentication.ensureAuthenticated, authentication.ensureAdmin, boothController.getBoothDetailsByID);
  app.post('/addBooth', authentication.ensureAuthenticated, authentication.ensureAdmin, boothController.createNewBooth);
  // app.get('/getAllBoothTransactions', authentication.ensureAuthenticated, authentication.ensureAdmin, routes.getAllBoothTransactions);

  //--------------------------
  // CreditData APIs
  //--------------------------
  app.get('/getCreditDataIDByName', boothController.getCreditDataIDByName);

  //--------------------------
  // Transaction APIs
  //--------------------------
  app.get('/getAllEventTransactions', authentication.ensureAuthenticated, authentication.ensureAdmin, transactionController.getAllEventTransactions);
  app.post('/recordTransaction', transactionController.recordTransaction);
  app.post('/addCreditData', transactionController.createNewCreditData);
  app.post('/addClaimData', transactionController.createNewClaimData);
  app.get('/getAllGuestCredits', transactionController.getAllGuestCredits);
  app.get('/getAllGuestsWithValidCredits', transactionController.getAllGuestsWithValidCredits);


  //--------------------------
  // Guest APIs
  //--------------------------
  app.get('/getAllEventGuests', /*authentication.ensureAuthenticated, authentication.ensureAdmin,*/ guestController.getAllEventGuests);
  app.get('/getGuestDetails', guestController.getGuestDetails);
  app.get('/getGuestDetailsByName', guestController.getGuestDetailsByName);  
  app.post('/addGuest', guestController.createNewGuest);
  app.post('/updateGuestUID', guestController.updateGuestUID);
  // app.get('/getAllGuestTransactions', routes.getAllGuestTransactions);
  // app.post('/getAllGuestCredits', routes.getAllGuestCredits);
  app.get('/getRegisteredGuestsCount', authentication.ensureAuthenticated, guestController.getRegisteredGuestsCount);
  app.get('/getPreRegisteredGuestsCount', authentication.ensureAuthenticated, guestController.getPreRegisteredGuestsCount);
  app.get('/getAnonymousGuestsCount', authentication.ensureAuthenticated, guestController.getAnonymousGuestsCount);
  app.get('/getPresentGuestsCount', authentication.ensureAuthenticated, guestController.getPresentGuestsCount);
  app.get('/getWalkinGuestsCount', authentication.ensureAuthenticated, guestController.getWalkinGuestsCount);
  app.post('/updateGuestDetails', /*authentication.ensureAuthenticated,*/ guestController.updateGuestDetails);

  //--------------------------
  // User APIs
  //--------------------------
  app.post('/updateUserDetails', authentication.ensureAuthenticated, userController.updateUserDetails);

  // socket io
  //routes.setSocketIO(socketIO);

  // MD Ignite Specific
  //app.post('/postMDIgniteExit', routes.postMDIgniteExit);
};
