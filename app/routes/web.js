'use strict';

var authentication = require('../objects/Authentication.js');

module.exports = function (app) {


  app.get('/', function(req, res){
    res.render('login',{});
  });

  app.get('/admin', function(req, res){
    res.render('adminindex',{});
  });

  app.get('/home', authentication.ensureAuthenticated, function(req, res){
    res.render('index',{});
  });

  app.get('/register', function(req, res) {
    res.render('register',{});
  });

  app.get('/anonymousRegistration', function(req, res){
    res.render('anonymousRegister',{});
  });

  app.get('/preregister', function(req, res) {
    res.render('preregister',{});
  });

  app.get('/login', function(req, res){
    res.render('login',{});
  });

  app.get('/greeter-entry', function(req, res) {
    res.render('greeter-entry', {});
  });

  app.get('/greeter-exit', function(req, res) {
    res.render('greeter-exit', {});
  });

  app.get('/search', function(req, res) {
    res.render('search', {});
  });

  app.get('/rafflecontrol', function(req, res) {
    res.render('rafflecontrol', {});
  });

  app.get('/raffle', function(req, res) {
    res.render('raffle', {})
  }); 

};