'use strict';

module.exports = function (socket) {
  
  console.log("a user is now connected via socketio...");
  console.log(socket.id);

  socket.on('draw', function(data) {
    console.log('Drawing from raffle');
    socket.broadcast.emit('start-draw', {draw: 'now'});
  });

  socket.on('new-winner', function(data) {
    console.log('New winner!', data.data.data);
    //routes.recordTransaction({body: data.data.data}, {send: function(){}});
    socket.broadcast.emit('new-winnerfound', data);
  });

  socket.on('clear-winners', function(data) {
    console.log('Clearing winners');
  });

};