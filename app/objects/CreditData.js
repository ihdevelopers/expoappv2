'use strict';

var creditDataDB = require('../models/expoAppCreditDataModel.js');

exports.getCreditDataIDByName = function(booth, callback){
  console.log("in objects/CreditData.js")
  console.log(booth)
  creditDataDB.findOne({}, function(err, data) {
    console.log("data on sample find");
    console.log(data)
  })
  creditDataDB.findOne({ 'eventID' : booth.eventID, 'name' : booth.name }, function(err, dbItem){
    console.log(dbItem)
    if(err){ 
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else if(dbItem !== null){
      callback([], dbItem.id); 
    }
    else{ 
      callback([], null); 
    }
  });
};
