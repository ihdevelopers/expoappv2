/*
 *  Log-in and Authentication functions
 */

exports.ensureAuthenticated = function(req, res, next) {
  //console.log("ensuring AUTH...");
  if (req.isAuthenticated()) {
    return next();
  } else {
    return res.send(401);
  }
};

exports.ensureAdmin = function(req, res, next) {
  //console.log("[@AUTH @ENSUREADMIN] : ALWAYS ADMIN FOR EXPO APP...")
  return next();
};

