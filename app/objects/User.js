'use strict';

var userDB = require('../models/expoAppUserModel.js');

var fetchUserByUsername = function(username, callback){
  var query = userDB.findOne({'username' : username});  
  query.exec(function (err, dbItem) {
    if(err){
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else if(dbItem !== null){
      callback([], dbItem); 
    }
    else{
      callback([], null);
    }
  });
};

exports.fetchUserById = function(id, callback){
  var query = userDB.findOne({'id' : id});
  query.select('id username password email mobileNo eventIDs firstName lastName');
  query.exec(function (err, dbItem) {
    if(err){ 
      console.log(err);
      callback([], null); 
    }
    else if(dbItem != null){ 
      callback([], dbItem); 
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.validateUser = function(credentials, callback){
  fetchUserByUsername(credentials.username, function(err, user){
    if(err.length === 0){
      if(user !== null){
        console.log('asdasdasdasdasdas',user.hash)
        if( credentials.password === user.hash){ 
          var tempUser = {};
          tempUser = user;
          delete tempUser.hash;
          callback([], tempUser);
        }
        else{
          callback(["INVALID PASSWORD"], null); 
        }
      }
      else{
        callback(["INVALID USERNAME"], null); 
      }
    }
    else{
      console.log(err);
      callback(["DB ERROR"], null); 
    }
  });
};

exports.addEventToUser = function(userId, eventID, callback){
  userDB.update({'id' : userId}, { $push: {eventIDs:eventID }}, function(err){
    if(err){ 
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else{ 
      callback([]);
    }
  });
};

exports.addSuperUser = function(user, callback){
  userDB.find({}, function (err, users){
    if(err){
      console.log(err);
      callback(["ERROR ACCESSING DB"]);
    }
    else{
      console.log("[NEW USER]: " + user.username + " -- " + user.password );
      var newUser = new userDB();
      newUser.id = users.length + 1;
      newUser.email = user.email;
      newUser.mobileNo = user.mobileNo;
      newUser.username = user.username;
      newUser.hash = user.password;
      newUser.eventIDs = [];
      newUser.save(callback([]));
    }
  });
};

exports.updateUserDetails = function(updateParams, callback){
  userDB.update( {id : updateParams.id}, 
    {$set : 
      {firstName : updateParams.firstName,
      lastName : updateParams.lastName,
      email : updateParams.email, 
      mobileNo : updateParams.mobileNo } }, function(err){
        if(err){
          callback(["DB ERROR"], null);
        }
        else{ 
          callback([], updateParams.id);
        }
  })
};
