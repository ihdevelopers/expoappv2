'use strict';

var guestDB = require('../models/expoAppGuestModel.js');
var request = require('request');
var fbConfig = require('../../config/fbConfig');

/*var config = [
    {
      appID: '826051450772637',
      appSecret: '239d4bdf351c0798c90503c59d9728b1'
    },
    {
      appID: '826051450772637',
      appSecret: '239d4bdf351c0798c90503c59d9728b1'
    },
    {
      appID: '847707098624226',
      appSecret: '6c60eb5bd77d36f38f3f9c6b2840ff5d'
    },
    {
      appID: '847707098624226',
      appSecret: '6c60eb5bd77d36f38f3f9c6b2840ff5d'
    },
    {
      appID: '169921833199214',
      appSecret: '69982b11a488e9ed56d8cd560beef662'
    }
];*/


//---------------------------//
// Guest Object Getters           
//---------------------------//

exports.fetchEventGuestsAll = function(eventID, limitedFieldsOnly, callback){

  var query = guestDB.find({'eventID': eventID});
  if(limitedFieldsOnly){
    query.select('id email firstName lastName gender mobileNo isPresent timeRegistered uID isPresent timePresent preRegistered');
  }
  else{ }

  query.exec(function (err, dbItems){
    if(err){ 
      callback(["DB ERROR"], null); 
    }
    else if(dbItems !== null){
      callback([], dbItems); 
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.fetchGuestById = function(id, callback){
  var query = guestDB.findOne({'id' : id});  
  query.exec(function (err, dbItem){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItem !== null){
      callback([], dbItem);
    }
    else{
      callback([], null);
    }
  });
};

exports.fetchGuestByUID = function(uIDin, callback){
  //var query = guestDB.findOne({$and: [{$or: [{'uID' : new RegExp(uID, "i")}, {'uID2' : new RegExp(uID, "i")}] }] });
  var query = guestDB.findOne({$or: [{'uID' : uIDin.toUpperCase()}, {'uID2' : uIDin.toUpperCase()}] });

  query.exec(function (err, dbItem){
    console.log(dbItem)
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItem !== null){
      //console.log(dbItem.email + " : " + dbItem.uID2);
      callback([], dbItem);
    }
    else{
      callback([], null);
    }
  });
};

exports.fetchGuestByName = function(guest, callback){
  var query = guestDB.find({$and: [{$or :[{'firstName' : new RegExp(guest.searchEntry, "i")}, {'lastName' : new RegExp(guest.searchEntry, "i")}, {'email': new RegExp(guest.searchEntry, "i")}, {'uID' : new RegExp(guest.searchEntry, "i")}, {'uID2' : new RegExp(guest.searchEntry, "i")}, {'transaction_id' : new RegExp(guest.searchEntry, "i")}]}]});
  query.exec(function (err, dbItems){
    if(err){ 
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItems !== null){
      callback([], dbItems);
    }
    else{
      callback([], null);
    }
  });
};

exports.fetchGuestsByEmailorFBID = function(guest, callback){

  var query = guestDB.find({ 
    $and: [ 
      { 'eventID': guest.eventID }, 
      { $or :[{'fbUsername' : new RegExp(guest.fbUsername, "i")}, {'email': new RegExp(guest.email, "i")}]} 
    ]
  });

  query.exec(function(err, dbItems){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItems !== null){
      callback([], dbItems);
    }
    else{
      callback([], null);
    }
  });
};

exports.fetchGuestByEmail = function(guest, callback){
  var query = guestDB.find({ 
    $and: [ 
      { 'eventID': guest.eventID }, 
      { 'email': new RegExp(guest.email, "i")}
    ]
  });

  query.exec(function(err, dbItems){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItems !== null){
      callback([], dbItems);
    }
    else{
      callback([], null);
    }
  });
};

exports.fetchGuestByUID2 = function(guest, callback){
  var query = guestDB.find({ 
    $and: [ 
      { 'eventID': guest.eventID }, 
      { $or: [{'uID': new RegExp(guest.uID, "i")}, {'uID2': new RegExp(guest.uID, "i")} ] }
    ]
  });

  query.exec(function(err, dbItems){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItems !== null){
      callback([], dbItems);
    }
    else{
      callback([], null);
    }
  });
};


//---------------------------//
// Guest Setters
//---------------------------//

exports.registerNewGuest = function(Guest, callback){

  //----------------------------------
  // REGISTRATION VIA FACEBOOK
  //----------------------------------
  if ((Guest.fbToken !== undefined) && (Guest.fbToken !== "")){
    
    //check duplicate fb username
    guestDB.find({ $and : [{'fbUsername' : Guest.fbUsername}, {'eventID' : Guest.eventID}] })
    .exec(function(err, guests){
      if(err){
        callback(["DB ERROR"], null);
      }
      else{

        //FB Username already exists!
        if(guests.length > 0){
          callback(["FBACCOUNT ALREADY EXISTS"], null);
        }

        //no user yet
        else{

          // Determine what Facebook App was used during registration
          if(Guest.fb_registration_id == null){
            console.log("INVALID FB REG ID SUPPLIED!"); 
            callback(["INVALID FB REG ID"], null); }
          else{ console.log("FB REG ID : " + Guest.fb_registration_id.toString()); }

          var fbAppID;
          var fbAppSecret;
          if( Guest.fb_registration_id == null){ 
            console.log("UNDEFINED FB REGISTRATION ID!!!");
            callback(["DB ERROR SAVING"], null);
          }
          else if( (Guest.fb_registration_id < 0) || (Guest.fb_registration_id >= fbConfig.apps.length) ){
          //else if( (Guest.fb_registration_id < 0) || (Guest.fb_registration_id >= config.length) ){
            console.log("OUT OF RANGE FB REGISTRATION ID!!!");
            callback(["DB ERROR SAVING"], null);
          }
          else{
            console.log("FB REGISTRATION ID OK");
            fbAppID = fbConfig.apps[Guest.fb_registration_id].appID; //config[Guest.fb_registration_id].appID;
            fbAppSecret = fbConfig.apps[Guest.fb_registration_id].appSecret; //config[Guest.fb_registration_id].appSecret;
          }
          var r = request.get('https://graph.facebook.com/oauth/access_token?client_id=' + fbAppID + '&client_secret=' + fbAppSecret + '&grant_type=fb_exchange_token&fb_exchange_token=' + Guest.fbToken, function fbCallback(err, httpresponse, body) {
          //var r = request.get('https://graph.facebook.com/oauth/access_token?client_id=' + config.appID + '&client_secret=' + config.appSecret + '&grant_type=fb_exchange_token&fb_exchange_token=' + Guest.fbToken, function fbCallback(err, httpresponse, body) {
            if (err !== null) {
              console.log("Error: ", err);
              res.send({status: "ERROR", message: "Error: " + err});
            }

            else {
              console.log(JSON.stringify(body));
              if (body.error) {
                console.log("Facebook Error: " + body.error.message);
                res.send({status:"ERROR1", message: body.error.message});
              }
              else {
                console.log("Response : " + body);
                var token = body.split('=')[1];
                var finaltoken = token.split('&')[0];

                console.log('NEW TOKEN - ' + finaltoken);

                guestDB.count({},function(err, count) { 
                  
                  var newGuest = new guestDB();

                  newGuest.id = count + 1;
                  newGuest.fbUsername = Guest.fbUsername;
                  newGuest.fbToken = finaltoken;                
                  newGuest.firstName = Guest.firstName;
                  newGuest.lastName = Guest.lastName;
                  newGuest.middleName = Guest.middleName;
                  newGuest.nickName = Guest.nickName;
                  newGuest.gender = Guest.gender;
                  newGuest.email = Guest.email;
                  newGuest.mobileNo = Guest.mobileNo;
                  newGuest.title = Guest.title;
                  newGuest.profession = Guest.profession;
                  newGuest.company = Guest.company;
                  newGuest.education = Guest.education;
                  newGuest.streetAddress1 = Guest.streetAddress1;
                  newGuest.streetAddress2 = Guest.streetAddress2;
                  newGuest.cityAddress = Guest.cityAddress;
                  newGuest.regionAddress = Guest.regionAddress;
                  newGuest.country = Guest.country;
                  newGuest.zip = Guest.zip;
                  newGuest.religion = Guest.religion;
                  newGuest.facebookPage = Guest.facebookPage;
                  newGuest.twitterHandle = Guest.twitterHandle;
                  newGuest.instagramHandle = Guest.instagramHandle;
                  newGuest.uID = Guest.uID;
                  newGuest.uID2 = Guest.uID2;
                  newGuest.transaction_id = "";
                  newGuest.fb_registration_id = Guest.fb_registration_id;
                  newGuest.timePresent = null;
                  newGuest.isPresent = false;
                  newGuest.creditIDs = [];
                  newGuest.transactionIDs = [];
                  newGuest.eventID = Guest.eventID;
                  newGuest.preRegistered = Guest.preRegistered;
                  newGuest.birthDate = Guest.birthDate;
                  newGuest.fbUsername = Guest.fbUsername;
                  newGuest.timeRegistered = new Date().getTime();
                  if(Guest.preRegistered){ newGuest.timePreRegistered = new Date().getTime(); }
                  else{ newGuest.timePreRegistered = null; }
                  newGuest.isAnonymous = false;

                  //----------------------------------
                  // ...
                  // ADD MORE GUEST DETAILS HERE
                  // ...
                  //----------------------------------
                  
                  newGuest.save(function(errSave){
                    if(errSave){ 
                      console.log(errSave);
                      callback(["DB ERROR SAVING"], null); 
                    }
                    else{                       
                      callback([], newGuest.id); 
                    }
                  });
                });
              }
            }
        });
        }
      }
    });
  }

  //----------------------------------
  // REGISTRATION VIA EMAIL
  //----------------------------------
  else { 
    //check duplicate email
    console.log('registration via email')
    guestDB.findOne({'email':Guest.email, 'eventID' : Guest.eventID}, function(err, _guest){

      //DB ERROR
      if(err){ 
        console.log(err);
        callback(["DB ERROR"], null);
      }

      //EMAIL EXISTS!
      else if(_guest !== null){
        callback(["EMAIL ALREADY EXISTS"], null);
      }

      //NO ERROR
      else{
        var newGuest = new guestDB();
        
        guestDB.count({},function(err, count) { 
          newGuest.id = count + 1;
          newGuest.fbToken = "";
          newGuest.firstName = Guest.firstName;
          newGuest.lastName = Guest.lastName;
          newGuest.middleName = Guest.middleName;
          newGuest.nickName = Guest.nickName;
          newGuest.gender = Guest.gender;
          newGuest.email = Guest.email;
          newGuest.mobileNo = Guest.mobileNo;
          newGuest.title = Guest.title;
          newGuest.profession = Guest.profession;
          newGuest.company = Guest.company;
          newGuest.education = Guest.education;
          newGuest.streetAddress1 = Guest.streetAddress1;
          newGuest.streetAddress2 = Guest.streetAddress2;
          newGuest.cityAddress = Guest.cityAddress;
          newGuest.regionAddress = Guest.regionAddress;
          newGuest.country = Guest.country;
          newGuest.zip = Guest.zip;
          newGuest.religion = Guest.religion;
          newGuest.facebookPage = Guest.facebookPage;
          newGuest.twitterHandle = Guest.twitterHandle;
          newGuest.instagramHandle = Guest.instagramHandle;
          newGuest.uID = Guest.uID;
          newGuest.uID2 = Guest.uID2;
          newGuest.transaction_id = "";
          newGuest.fb_registration_id = null;
          newGuest.timePresent = null;
          newGuest.isPresent = false;
          newGuest.creditIDs = [];
          newGuest.transactionIDs = [];
          newGuest.eventID = Guest.eventID;
          newGuest.preRegistered = Guest.preRegistered;
          newGuest.birthDate = Guest.birthDate;
          newGuest.fbUsername = "_UNIQUE_FB_USERNAME_" + (newGuest.id).toString();
          newGuest.timeRegistered = new Date().getTime();
          if(Guest.preRegistered){ newGuest.timePreRegistered = new Date().getTime(); }
          else{ newGuest.timePreRegistered = null; }
          newGuest.isAnonymous = Guest.isAnonymous;

          //----------------------------------
          // ...
          // ADD MORE GUEST DETAILS HERE
          // ...
          //----------------------------------

          newGuest.save(function(errSave){
            if(errSave){ 
              console.log('errSave');
              callback(["DB ERROR SAVING"], null); 
            }
            else{ 
              console.log('no errSave')
              console.log("newGuest.id: " + newGuest.id)
              callback([], newGuest.id); 
            }
          });
        });
          
      }
    });
  }

/*
  guestDB.find({}, function (err, guests) {

    if(err)
    { 
      callback(["DB ERROR"], null);
    }
    else if(guests !== null){

      var newGuest = new guestDB();

      //----------------------------------
      // REGISTRATION VIA FACEBOOK
      //----------------------------------
      if ((Guest.fbToken !== undefined) && (Guest.fbToken !== "")){

        var r = request.get('https://graph.facebook.com/oauth/access_token?client_id=' + config.appID + '&client_secret=' + config.appSecret + '&grant_type=fb_exchange_token&fb_exchange_token=' + Guest.fbToken, function fbCallback(err, httpresponse, body) {
          if (err !== null) {
            console.log("Error: ", err);
            res.send({status: "ERROR", message: "Error: " + err});
          }

          else {
            if (body.error !== null) {
              console.log("Facebook Error: " + body.error.message);
              res.send({status:"ERROR1", message: body.error.message});
            }

            else {
              console.log("Response : " + body);
              var token = body.split('=')[1];
              var finaltoken = token.split('&')[0];
              newGuest.fbToken = finaltoken;

              newGuest.id = guests.length + 1;
              newGuest.firstName = Guest.firstName;
              newGuest.lastName = Guest.lastName;
              newGuest.middleName = Guest.middleName;
              newGuest.nickName = Guest.nickName;
              newGuest.gender = Guest.gender;
              newGuest.email = Guest.email;
              newGuest.mobileNo = Guest.mobileNo;
              newGuest.title = Guest.title;
              newGuest.profession = Guest.profession;
              newGuest.company = Guest.company;
              newGuest.education = Guest.education;
              newGuest.streetAddress1 = Guest.streetAddress1;
              newGuest.streetAddress2 = Guest.streetAddress2;
              newGuest.cityAddress = Guest.cityAddress;
              newGuest.regionAddress = Guest.regionAddress;
              newGuest.country = Guest.country;
              newGuest.zip = Guest.zip;
              newGuest.religion = Guest.religion;
              newGuest.facebookPage = Guest.facebookPage;
              newGuest.twitterHandle = Guest.twitterHandle;
              newGuest.instagramHandle = Guest.instagramHandle;
              newGuest.uID = Guest.uID;
              newGuest.timePresent = null;
              newGuest.isPresent = false;
              newGuest.creditIDs = [];
              newGuest.transactionIDs = [];
              newGuest.eventID = Guest.eventID;
              newGuest.preRegistered = Guest.preRegistered;
              newGuest.birthDate = Guest.birthDate;
              newGuest.fbUsername = Guest.fbUsername;
              newGuest.timeRegistered = new Date().getTime();
              if(Guest.preRegistered){ newGuest.timePreRegistered = new Date().getTime(); }
              else{ newGuest.timePreRegistered = null; }
              newGuest.isAnonymous = false;

              //----------------------------------
              // ...
              // ADD MORE GUEST DETAILS HERE
              // ...
              //----------------------------------

              newGuest.save(function(errSave){
                if(errSave !== null) {
                  if(errSave.code === 11000){
                    guestDB.update({ $and : [{'fbUsername' : Guest.fbUsername},{'eventID' : Guest.eventID}] }, 
                      {$set : {fbToken : finaltoken}}, 
                      function(err4){
                      if(err4){ 
                        console.log(err4);
                        callback(["DB ERROR"], null); 
                      }
                      else{
                        callback(["GUEST ALREADY EXISTS"], errSave.code);
                      }
                    });
                  }
                  else{ 
                    callback([], errSave); 
                  }
                }
                else{
                  callback([], newGuest.id); 
                }
              })

              // manually search if fbUsername already exists
              // SEEMS LIKE CODE EXEC WILL NOT REACH THIS POINT... (?)
              
              guestDB.findOne({
                $and : [{'fbUsername' : Guest.fbUsername}, {'eventID' : Guest.eventID}] },
                function (err, dbItem){
                if(err){
                  console.log(err);
                  callback(["DB ERROR"], null);
                }
                else if(dbItem !== null){ 
                  guestDB.update({'fbUsername' : Guest.fbUsername}, {$set : {fbToken : finaltoken}}, function(err4){
                    if(err4){ 
                      console.log(err4);
                      callback(["DB ERROR"], null);
                    }
                    else{ 
                      callback(["FBACCOUNT ALREADY EXISTS"], null);
                    }
                  });
                }
                else{ 
                  newGuest.save(function(errSave){
                    if(errSave){ callback(["DB ERROR SAVING"], null); }
                    else{ callback([], newGuest.id); }
                  })
                }
              });
            }
          }
        });

      }
      //----------------------------------
      // REGISTRATION VIA EMAIL
      //----------------------------------
      else {
        newGuest.fbToken = "";

        newGuest.id = guests.length + 1;
        newGuest.firstName = Guest.firstName;
        newGuest.lastName = Guest.lastName;
        newGuest.middleName = Guest.middleName;
        newGuest.nickName = Guest.nickName;
        newGuest.gender = Guest.gender;
        newGuest.email = Guest.email;
        newGuest.mobileNo = Guest.mobileNo;
        newGuest.title = Guest.title;
        newGuest.profession = Guest.profession;
        newGuest.company = Guest.company;
        newGuest.education = Guest.education;
        newGuest.streetAddress1 = Guest.streetAddress1;
        newGuest.streetAddress2 = Guest.streetAddress2;
        newGuest.cityAddress = Guest.cityAddress;
        newGuest.regionAddress = Guest.regionAddress;
        newGuest.country = Guest.country;
        newGuest.zip = Guest.zip;
        newGuest.religion = Guest.religion;
        newGuest.facebookPage = Guest.facebookPage;
        newGuest.twitterHandle = Guest.twitterHandle;
        newGuest.instagramHandle = Guest.instagramHandle;
        newGuest.uID = Guest.uID;
        newGuest.isPresent = false;
        newGuest.timePresent = null;
        newGuest.creditIDs = [];
        newGuest.transactionIDs = [];
        newGuest.eventID = Guest.eventID;
        newGuest.preRegistered = Guest.preRegistered;
        newGuest.birthDate = Guest.birthDate;
        newGuest.fbUsername = "_UNIQUE_FB_USERNAME_" + (newGuest.id).toString();
        newGuest.timeRegistered = new Date().getTime();
        if(Guest.preRegistered){ newGuest.timePreRegistered = new Date().getTime(); }
        else{ newGuest.timePreRegistered = null; }
        newGuest.isAnonymous = Guest.isAnonymous;

        //----------------------------------
        // ...
        // ADD MORE GUEST DETAILS HERE
        // ...
        //----------------------------------

        console.log("HERE YO");

        if(Guest.isAnonymous)
        {
          //----------------------------------
          // ANONYMOUS REGISTRATION
          //----------------------------------
          newGuest.save(function(errSave){
            if(errSave){ callback(["DB ERROR SAVING"], null); }
            else{ callback([], newGuest.id); }
          }) 
        }
        else
        {
          //----------------------------------
          // UNIQUE EMAIL REQUIRED BY DEFAULT
          //----------------------------------
          guestDB.findOne({
            $and : [{'email' : Guest.email}, {'eventID' : Guest.eventID}] },
            function (err, dbItem) {
            if(err){ 
              console.log(err);
              callback(["DB ERROR"], null); 
            }
            else if(dbItem !== null){ 
              callback(["EMAIL ALREADY EXISTS"], null); 
            }
            else{ 
              newGuest.save(function(errSave){
                if(errSave){ callback(["DB ERROR SAVING"], null); }
                else{ callback([], newGuest.id); }
              }) 
            }
          });
        }
      }
    }
    else{ callback(["DB ERROR"], null); }
  });
*/
};

exports.updateGuestDetails = function(updateParams, callback){
  console.log('in Guest.updateGuestDetails')
  //--------------------------------------
  // check if email is already in use
  //--------------------------------------
  guestDB.findOne({'email' : updateParams.email }, 
    function(err, guest){
      console.log(guest)
    if(err){ console.log("err: " + err); callback(["DB ERROR"], null); }
    else if(guest != null)
    { 
      //--------------------------------------------------
      // ERROR : If already used by another user...
      //--------------------------------------------------
      if(guest._id != updateParams._id){
        console.log("EMAIL ALREADY IN USE");
        callback(["EMAIL ALREADY IN USE"], null); 
      }
      else{
        //--------------------------------------------------
        // ELSE : same user (no change in email)
        //--------------------------------------------------
        console.log("_id : " + guest._id);
        console.log("Update _id : " + updateParams._id);
        guestDB.update( {_id : updateParams._id}, 
          {$set : 
            {email : updateParams.email,
            lastName : updateParams.lastName,
            firstName : updateParams.firstName, 
            uID : updateParams.uID.toUpperCase(),
            uID2: updateParams.uID2.toUpperCase() } }, function(err){
              if(err){
                console.log("DB ERROR")
                callback(["DB ERROR"], null);
              }
              else{ 
                callback([], updateParams._id);
              }
        })
      }
    }
    else{
      //--------------------------------------------------
      // ELSE : email is still available for use
      //--------------------------------------------------
      guestDB.update( {_id : updateParams._id}, 
        {$set : 
          {email : updateParams.email,
          lastName : updateParams.lastName,
          firstName : updateParams.firstName, 
          uID : updateParams.uID.toUpperCase(),
          uID2 : updateParams.uID2.toUpperCase() } }, function(err){
            if(err){
              console.log("DB ERROR");
              callback(["DB ERROR"], null);
            }
            else{ 
              callback([], updateParams._id);
            }
      })
    }
  })
};

exports.updateGuestUID = function(guest, callback){
  guestDB.update({'id' : guest.id},{$set : {uID :  guest.uID.toUpperCase()}}, function(err){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else{ 
      callback([], guest.id);
    }
  });
};


//---------------------------//
// Guest Count Getters           
//---------------------------//

exports.getRegisteredGuestsCount = function(eventID, date, callback){
  var query;
  if(date == "All Days"){
    query = guestDB.find({$and : [{eventID : eventID}, {isAnonymous : false}]}); 
  }
  else{ 
    var unixDate = Date.parse(date);
    query = guestDB.find({
      $and : [
      {eventID : eventID}, 
      {isAnonymous : false}, 
      {timeRegistered : {$gte : unixDate, $lt : (unixDate + 86399000)} }
      ]}); 
  }
  query.exec(function(err, results){
    if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            if(results != null){
              callback([], results.length);
            }
            else { callback([], 0); }
          }
  })
};

exports.getPresentGuestsCount = function(eventID, date, callback){
  var query;
  if(date == "All Days"){ 
    query = guestDB.find({$and : [{eventID : eventID}, {isPresent : true}]}); 
  }
  else{ 
    var unixDate = Date.parse(date);
    query = guestDB.find({
      $and : [
      {eventID : eventID}, 
      {isPresent : true}, 
      {timePresent : {$gte : unixDate, $lt : (unixDate + 86399000)} }
      ]}); 
  }
  query.exec(function(err, results){
    if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            if(results != null){
              callback([], results.length);
            }
            else { callback([], 0); }
          }
  })
};

exports.getWalkinGuestsCount = function(eventID, date, callback){
  var query;
  if(date == "All Days"){ 
    query = guestDB.find({$and : [{eventID : eventID}, {preRegistered : false}]}); 
  }
  else{ 
    var unixDate = Date.parse(date);
    query = guestDB.find({
      $and : [
      {eventID : eventID}, 
      {preRegistered : false}, 
      {timeRegistered : {$gte : unixDate, $lt : (unixDate + 86399000)} }
      ]}); 
  }
  query.exec(function(err, results){
    if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            if(results != null){
              callback([], results.length);
            }
            else { callback([], 0); }
          }
  })
};

exports.getPreRegisteredGuestsCount = function(eventID, date, callback){
  var query;
  if(date == "All Days"){ 
    query = guestDB.find({$and : [{eventID : eventID}, {preRegistered : true}]}); 
  }
  else{ 
    var unixDate = Date.parse(date);
    query = guestDB.find({
      $and : [
      {eventID : eventID}, 
      {preRegistered : true}, 
      {timePreRegistered : {$gte : unixDate, $lt : (unixDate + 86399000)} }
      ]}); 
  }
  query.exec(function(err, results){
    if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            if(results != null){
              callback([], results.length);
            }
            else { callback([], 0); }
          }
  })
};

exports.getAnonymousGuestsCount = function(eventID, date, callback){
  var query;
  if(date == "All Days"){ 
    query = guestDB.find({$and : [{eventID : eventID}, {isAnonymous : true}]}); 
  }
  else{ 
    var unixDate = Date.parse(date);
    query = guestDB.find({
      $and : [
      {eventID : eventID}, 
      {isAnonymous : true}, 
      {timeRegistered : {$gte : unixDate, $lt : (unixDate + 86399000)} }
      ]}); 
  }
  query.exec(function(err, results){
    if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            if(results != null){
              callback([], results.length);
            }
            else { callback([], 0); }
          }
  })
};


//---------------------------//
// Guest Actions
//---------------------------//

exports.activateGuest = function(guestID, callback){
  //--------------------------------------------------
  // SET guest 'isPresent' and 'timePresent'
  //--------------------------------------------------
  var timenow = new Date().getTime();
  guestDB.update( {id : guestID}, 
    {$set : 
      {isPresent : true, 
      timePresent : timenow } }, function(err){
        if(err){
          callback(["DB ERROR"], null);
        }
        else{ 
          callback([], guestID);
        }
  })
};

exports.addTransactionToGuestByUID = function(uID, transactionID, callback){
  guestDB.update({'uID' : uID}, {$push : {transactionIDs : transactionID}}, function(err){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else{
      callback([]); 
    }
  });
};