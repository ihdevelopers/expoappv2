'use strict';

var eventDB = require('../models/expoAppEventModel.js');
var boothDB = require('../models/expoAppBoothModel.js');

exports.fetchUserEventsAll = function(userId, callback){
  var query = eventDB.find({'organizerID' : userId});
  query.select('id name date location notes guestEstimate organizerID boothIDs guestIDs');
  query.exec(function (err, dbItems) {
    if(err){
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else{
     callback([], dbItems); 
    }
  });
};

exports.fetchEventById = function(params, callback){

  var query = eventDB.findOne({'id': params.eventID});

  if(params.regParamsOnly){
    query.select('guestDetailsRequired activationRequired id name logoImage backgroundImage date location');
  }

  query.exec(function (err, dbItem){
    if(err){ 
      callback(["DB ERROR"], null); 
    }
    else if(dbItem !== null){
      callback([], dbItem); 
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.registerNewEvent = function(Event, callback){
  eventDB.find({}, function (err, events) {
    if(err){ 
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else if(events !== null){ 
      var newEvent = new eventDB();
      newEvent.id = events.length + 1;
      newEvent.name = Event.name;
      newEvent.date = Event.date;
      newEvent.location = Event.location;
      newEvent.bannerImage = Event.bannerImage;
      newEvent.logoImage = Event.logoImage;
      newEvent.backgroundImage = Event.backgroundImage;
      newEvent.notes = Event.notes;
      newEvent.guestEstimate = Event.guestEstimate;
      newEvent.organizerID = Event.organizerID;
      newEvent.boothIDs = [];
      newEvent.guestIDs = [];
      newEvent.transactionIDs = [];
      newEvent.guestDetailsRequired = Event.guestDetailsRequired;
      newEvent.preRegAvailable = Event.preRegAvailable;
      newEvent.activationRequired = Event.activationRequired;
      newEvent.save(callback([], newEvent.id));
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.addBoothToEvent = function(eventID, boothID, callback){
  eventDB.update({'id' : eventID}, {$push : {boothIDs : boothID}}, function(err){
    if(err){ 
      callback(["DB ERROR"], null); 
    }
    else{
      callback([]); 
    }
  });
};

exports.addGuestToEvent = function(eventID, guestID, callback){
  eventDB.update({'id' : eventID}, {$push : {guestIDs : guestID}}, function(err){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else{
      callback([]); 
    }
  });
};

exports.addTransactionToEvent = function(eventID, transactionID, callback){
  eventDB.update({'id' : eventID}, {$push : {transactionIDs : transactionID}}, function(err){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else{
      callback([]);
    }
  });
};

exports.getEventEngagementsCount = function(eventID, callback){
  eventDB.findOne( 
      {'id' : eventID},
        function(err, result){
          if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{
            if(result != null){
              callback([], result.transactionIDs.length);
            } 
            else{ callback(["DB ERROR"], null); }
          }
  })
};

exports.updateEventDetails = function(updateParams, callback){
  eventDB.update({'id' : updateParams.id}, {$set : 
    {name : updateParams.name, 
    location : updateParams.location, 
    guestEstimate : updateParams.guestEstimate,
    date : updateParams.date } }, function(err){
      if(err){
        callback(["DB ERROR"], null);
      }
      else{ 
        callback([], updateParams.id);
      }
    })
};

exports.updateEventBoothDetails = function(updateParams, callback){
  boothDB.update( {'id' : updateParams.id}, 
    {$set : 
      {name : updateParams.name, 
      maxTransactionsPerGuest : updateParams.maxTransactionsPerGuest } }, function(err){
        if(err){
          callback(["DB ERROR"], null);
        }
        else{ 
          callback([], updateParams.id);
        }
  })
};
