'use strict';

var boothDB = require('../models/expoAppBoothModel.js');

exports.getAllEventBooths = function(id, callback){
  var query = boothDB.find({'eventID' : id});
  query.exec(function (err, dbItems) {
    if(err){
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else if(dbItems !== null){ 
      callback([], dbItems); 
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.getBoothIDByName = function(booth, callback){
  boothDB.findOne({ 'eventID' : booth.eventID, 'name' : booth.name }, function(err, dbItem){
    if(err){ 
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else if(dbItem !== null){
      callback([], dbItem.id); 
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.getBoothDetailsByID = function(booth, callback){
  boothDB.findOne({ 'eventID' : booth.eventID, 'id' : booth.id }, function(err, dbItem){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null); 
    }
    else if(dbItem != null){ 
      callback([], dbItem); 
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.registerNewBooth = function(Booth, callback){
  boothDB.find({}, function (err, booths) {
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(booths !== null){ 
      var newBooth = new boothDB();
      newBooth.id = booths.length + 1;
      newBooth.name = Booth.name;
      newBooth.type = Booth.type;
      newBooth.eventID = Booth.eventID;
      newBooth.creditDataIDs = [];
      newBooth.maxTransactionsPerGuest = Booth.maxTransactionsPerGuest;
      newBooth.oneTimeTransactionOnly =  Booth.oneTimeTransactionOnly;
      newBooth.save(callback([], newBooth.id));
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.getEventCheckinBooths = function(eventID, callback){
  boothDB.find({ 
    $and : [ 
      {eventID : eventID}, 
      {type : 'checkin'} ] }, 
        function(err, results){
          if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            callback([], results);
          }
  })
};

exports.getEventClaimBooths = function(eventID, callback){
  boothDB.find({ 
    $and : [ 
      {eventID : eventID}, 
      {type : 'claiming'} ] }, 
        function(err, results){
          if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            callback([], results);
          }
  })
};
