var checkinTransDB = require('../../models/transaction_models/expoAppCheckinTransactionModel.js');

exports.registerNewCheckinTransaction = function(data, callback){
  checkinTransDB.find({}, function (err, checkins){
    if(err){
      console.log(err);
      callback(["ERROR ACCESSING DB"], null); 
    }
    else{
      var newCheckin = new checkinTransDB();
      newCheckin.id = checkins.length + 1;
      newCheckin.uID = data.uID;
      newCheckin.type = "checkin";
      newCheckin.save(callback([], newCheckin.id ));
    }
  });
};
