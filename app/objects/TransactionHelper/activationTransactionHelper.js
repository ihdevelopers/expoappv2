var activationTransDB = require('../../models/transaction_models/expoAppActivationTransactionModel.js');

exports.registerNewActivationTransaction = function(data, callback){
  activationTransDB.find({}, function (err, activations){
    if(err){
      consol.log(err);
      callback(["ERROR ACCESSING DB"], null); 
    }
    else{
      var newActivation = new activationTransDB();
      newActivation.id = activations.length + 1;
      newActivation.uID = data.uID;
      newActivation.type = "activation";
      newActivation.save(callback([], newActivation.id ));
    }
  });
};
