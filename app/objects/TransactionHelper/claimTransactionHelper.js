var async = require('async');

var claimTransDB = require('../../models/transaction_models/expoAppClaimTransactionModel.js');
var claimDataDB = require('../../models/expoAppClaimDataModel.js');
var creditDB = require('../../models/expoAppCreditModel.js');

var processDataCheckCreditDataForClaim = function(item, done){
  creditDB.findOne({$and:[{ 'uID' : item.uID}, {'creditDataID' : item.creditDataID }]}, function(err, credit){
    if(err){ 
      done(err);      
    }
    else if(credit != null){
      if( credit.totalValue >= item.creditDataPoints ){
        done();        
      }
      else{
        done('insufficient credits');
      }
    }
    else{
      done('missing credit');
    }
  });
};

var processDataUpdateCreditsForClaim = function(item, done){
  creditDB.findOne({$and:[{ 'uID' : item.uID}, {'creditDataID' : item.creditDataID }]}, function(err, credit){
    if(err){
      done(err);
    }
    else if(credit !== null){
      var newTotalValue = 0;
      console.log("init " + credit.totalValue);
      newTotalValue = credit.totalValue - item.creditDataPoints;
      credit.update({$set : {totalValue : newTotalValue}}, function(updateErr){
        if(updateErr){
          done(updateErr);          
        }
        else{
          console.log('updated!');
          done();
        }
      });
    }
    else{
      done('credit data not found!');
    }
  });
};

exports.registerNewClaimTransaction = function(data, callback){
  claimTransDB.find({}, function (err, claims) {
    if(err){
      console.log(err);
      callback(["ERROR ACCESSING DB"], null); 
    }
    else{
      // add Claim transaction data
      var newClaim = new claimTransDB();
      newClaim.id = claims.length + 1;
      newClaim.claimDataID = data.claimDataID;
      newClaim.guestID = data.guestID;
      newClaim.type = "claim";

      console.log("yo " + data.claimDataID);

      claimDataDB.findOne({'id' : data.claimDataID}, function(findClaimDataErr, dbItem){
        
        if(findClaimDataErr){
          console.log(findClaimDataErr);
          callback(["DB ERROR"], null);
        }

        else if(dbItem != null){
          var tempObj = [];
          console.log(dbItem.creditDataIDs);

          for(var i=0; i<dbItem.creditDataIDs.length; i++){
            var temp = {};
            temp.creditDataID = dbItem.creditDataIDs[i];
            temp.creditDataPoints = dbItem.creditDataPoints[i];
            temp.uID = data.uID;
            tempObj.push(temp);
          }

          var _resultAccumulator = [];

          async.eachSeries(tempObj, function(item, _callback){
            processDataCheckCreditDataForClaim(item, function(err){
              if(err){
                console.log(err);
                _resultAccumulator.push(false);
                _callback();
              }
              else{
                _resultAccumulator.push(true); 
                _callback();
              }
            });
          }, function(){

            if(_resultAccumulator.indexOf(false) !== -1){
              callback(["INSUFFICIENT CREDITS"], null);
            }
            else{
              async.eachSeries(tempObj, function(item, _callback){
                processDataUpdateCreditsForClaim(item, function(err){
                  if(err){
                    console.log(err);
                  }
                  _callback();
                });
              }, function(){
                newClaim.save(callback([], newClaim.id ));
              });
            }
          });
        }

        else{ 
          console.log('db item not found!'); 
          callback(["DB ERROR"], null); 
        }
      });
    }
  });
};

