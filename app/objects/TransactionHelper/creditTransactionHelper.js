var creditTransDB = require('../../models/transaction_models/expoAppCreditTransactionModel.js');
var creditDB = require('../../models/expoAppCreditModel.js');
var creditDataDB = require('../../models/expoAppCreditDataModel.js');

var updateGuestCreditByUID = function(data, callback){
  creditDB.findOne({$and:[{ 'uID' : data.uID}, {'creditDataID' : data.creditDataID }]}, function(err, credit){
    
    //error
    if(err){ 
      console.log(err);
      callback(["DB ERROR"], null);
    }

    // create new
    else if( credit == null){
      console.log("creating new credit line...");
      
      creditDB.find({}, function(addNewErr, allCredits){
        if(addNewErr){
          console.log(addNewErr);
          callback(["DB ERROR"], null); 
        }
        else{                  
          var newCreditLine = new creditDB();
          newCreditLine.id = allCredits.length + 1;
          newCreditLine.guestID = data.guestID;
          newCreditLine.uID = data.uID;
          newCreditLine.totalValue = 0;
          newCreditLine.creditDataID = data.creditDataID;
          console.log('creditDataID:',data.creditDataID);

          // update total value
          creditDataDB.findOne({'id' : data.creditDataID}, function(updateTotalErr, dbItem2){
            if(addNewErr){
              console.log(addNewErr);
              callback(["DB ERROR"], null); 
            }
            else{
              for(var i=0; i<dbItem2.boothIDs.length; i++){
                if(dbItem2.boothIDs[i] == data.boothID){
                  newCreditLine.totalValue = dbItem2.boothCreditValues[i];
                }
              }
              newCreditLine.save(callback([], newCreditLine.id));
            }
          });
        }
      });
    }

    // update total value of existing
    else{      
      console.log('updateing credit line....!')
      creditDataDB.findOne({'id' : data.creditDataID}, function(updateTotalErr, dbItem2){
        if(updateTotalErr){ 
          console.log(updateTotalErr);
          callback(["DB ERROR"], null);
        }
        else{
          var newTotalValue = 0;;
          for(var i=0; i<dbItem2.boothIDs.length; i++){
            if(dbItem2.boothIDs[i] == data.boothID){
              console.log('prrrrrinting', credit.totalValue)
              newTotalValue = credit.totalValue + dbItem2.boothCreditValues[i];
            }
          }
          credit.update({ $set : { totalValue : newTotalValue}}, function(updateErr){
            if(updateErr){ 
              console.log(updateErr);
              callback(["DB ERROR"], null);
            }
            else{
              callback([], null);
            }
          });
        }
      });
    }
  });
};

exports.registerNewCreditTransaction = function(data, callback){
  creditTransDB.find({}, function (err, credits) {
    if(err){ callback(["ERROR ACCESSING DB"], null); }
    else{
      // add Credit transaction data
      var newCredit = new creditTransDB();
      newCredit.id = credits.length + 1;
      newCredit.creditDataID = data.creditDataID;
      newCredit.creditValue = data.creditValue;
      newCredit.type = "credit";
      updateGuestCreditByUID(data, function(updateErr, creditLineID){
        if(updateErr.length === 0){
          newCredit.save(callback([], newCredit.id ));
        }
        else{
          newCredit.save(callback(["ERROR CREATING CREDIT"], null));
        }
      });
    }
  });
};
