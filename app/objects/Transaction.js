'use strict';

var async = require('async');

var guestDB = require('../models/expoAppGuestModel.js');
var transactionDB = require('../models/expoAppTransactionModel.js');
var creditDB = require('../models/expoAppCreditModel.js');
var creditDataDB = require('../models/expoAppCreditDataModel.js');
var boothDB = require('../models/expoAppBoothModel.js');
var claimDataDB = require('../models/expoAppClaimDataModel.js');

var checkinTransactionHelper = require('./TransactionHelper/checkInTransactionHelper.js');
var activationTransactionHelper = require('./TransactionHelper/activationTransactionHelper.js');
var creditTransactionHelper = require('./TransactionHelper/creditTransactionHelper.js');
var claimTransactionHelper = require('./TransactionHelper/claimTransactionHelper.js');

exports.fetchEventTransactionsAll = function(eventID, callback){
  transactionDB.find({'eventID' : eventID}, function (err, dbItems) {
    // console.log(dbItems);
    if(err){ 
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItems !== null){
      callback([], dbItems);
    }
    else{
     callback([], null);
    }
  });
};

exports.checkIfTransactionMaxedOut = function(transaction, callback){
  // check via uID
  transactionDB.find( {$and:[{ 'uID' : transaction.uID}, {'boothID' : transaction.boothID }]}, function(err, dbItems){
    if(err){
      console.log(err);
      callback(["DB ERROR"], true);
    }
    else if( (dbItems !== null) && (dbItems.length > 0) ){
      boothDB.findOne({ 'id' : transaction.boothID }, function(err2, dbItem){
        if(err2){ 
          console.log(err2);
          callback(["DB ERROR"], null);
        }
        else if( dbItem !== null){
          if(dbItems.length < dbItem.maxTransactionsPerGuest){ 
            callback([], false);
          }
          else{
            callback([], true); 
          }
        }
        else{
          callback(["DB ERROR"], true); 
        }
      });
    }
    else{
      callback([], false);
    }
  });
};

exports.registerNewTransaction = function(transaction, callback){
  // GET TRANSACTION TYPE FROM BOOTH ID
  boothDB.findOne({'id' : transaction.boothID}, function(err, booth){
    console.log("BOOTH " + transaction.boothID + " is of " + booth.type + " type");
    
    // Create checkin
    if(booth.type === "checkin"){
      checkinTransactionHelper.registerNewCheckinTransaction(transaction, function(err2, checkinID){
        if(err2.length === 0){
          transactionDB.find({}, function (err3, transactions){
            if(err3){
              console.log(err3);
              callback(["DB ERROR"], null, null); 
            }
            else{
              var newTransaction = new transactionDB();
              newTransaction.id = transactions.length + 1;
              newTransaction.boothID = transaction.boothID;
              newTransaction.eventID = transaction.eventID;
              newTransaction.timestamp = transaction.timestamp;
              newTransaction.uID = transaction.uID;
              newTransaction.transactionDataID = checkinID;
              newTransaction.guestID = transaction.guestID;
              newTransaction.save(callback([], newTransaction.id, booth.type), function() {
                console.log("recorded!")
              });
            }
          });
        }
        else{
          console.log(err2);
          callback(["ERROR SAVING TRANSACTION"], null, null); 
        }
      });
    }

    // Create activation
    else if(booth.type === "activation"){
      activationTransactionHelper.registerNewActivationTransaction(transaction, function(err2, activationID){
        if(err2.length === 0){
          transactionDB.find({}, function (err3, transactions) {
            if(err3){ 
              console.log(err3);
              callback(["DB ERROR"], null, null); 
            }
            else{
              var newTransaction = new transactionDB();
              newTransaction.id = transactions.length + 1;
              newTransaction.boothID = transaction.boothID;
              newTransaction.eventID = transaction.eventID;
              newTransaction.timestamp = transaction.timestamp;
              newTransaction.uID = transaction.uID;
              newTransaction.transactionDataID = activationID;
              newTransaction.guestID = transaction.guestID;

              // Update guest's isPresent field
              DataProvider.prototype.updateGuestIsPresentByUID(transaction.uID, true, function(err4){
                if(err4.length == 0){ newTransaction.save(callback([], newTransaction.id, booth.type)); }
                else{ callback(["ERROR ADDING TRANSACTION TO GUEST"], null, null); }
              })
            }
          })
        }
        else{
          console.log(err2);
          callback(["ERROR SAVING TRANSACTION"], null, null); 
        }
      })
    }

    // Create credit    
    else if(booth.type === "credit"){
      creditTransactionHelper.registerNewCreditTransaction(transaction, function(err2, creditID){
        if(err2.length === 0){
          transactionDB.find({}, function (err3, transactions) {
            if(err3){ 
              console.log(err3);
              callback(["DB ERROR"], null, null); 
            }
            else{
              var newTransaction = new transactionDB();
              newTransaction.id = transactions.length + 1;
              newTransaction.boothID = transaction.boothID;
              newTransaction.eventID = transaction.eventID;
              newTransaction.timestamp = transaction.timestamp;
              newTransaction.uID = transaction.uID;
              newTransaction.transactionDataID = creditID;
              newTransaction.guestID = transaction.guestID;
              newTransaction.save(callback([], newTransaction.id, booth.type));
            }
          });
        }
        else{
          console.log(err2);
          callback(["ERROR SAVING TRANSACTION"], null, null); 
        }
      });
    }

    // Create claim
    else if(booth.type === "claiming"){
      claimTransactionHelper.registerNewClaimTransaction(transaction, function(err2, claimID){
        if(err2.length === 0){
          transactionDB.find({}, function (err3, transactions) {
            if(err3){
              console.log(err3);
              callback(["DB ERROR"], null, null); 
            }
            else{
              var newTransaction = new transactionDB();
              newTransaction.id = transactions.length + 1;
              newTransaction.boothID = transaction.boothID;
              newTransaction.eventID = transaction.eventID;
              newTransaction.timestamp = transaction.timestamp;
              newTransaction.uID = transaction.uID;
              newTransaction.transactionDataID = claimID;
              newTransaction.guestID = transaction.guestID;
              newTransaction.save(callback([], newTransaction.id, booth.type));
            }
          });
        }
        else{
          console.log(err2);
          callback(["ERROR SAVING TRANSACTION"], null, null);
        }
      });
    }

    // unknown transaction type/booth received
    else{    
      callback(["UNKNOWN TRANSACTION"], null, null);
    }
  });
};

exports.addNewCreditData = function(creditData, callback){
  console.log("ADD NEW CREDIT DATA");
  creditDataDB.find({}, function (err, credits) {
    if(err){
      console.log(err);
      callback(["ERROR ACCESSING DB"], null);
    }
    else{
      // add Credit Data
      var newCreditData = new creditDataDB();
      newCreditData.id = credits.length + 1;
      newCreditData.name = creditData.name;
      newCreditData.units = creditData.units;
      newCreditData.boothIDs = creditData.boothIDs;
      newCreditData.boothCreditValues = creditData.boothCreditValues;
      newCreditData.eventID = creditData.eventID;

      // create temporary object to facilitate updating of related booths

      var forBoothsUpdate = [];

      for(var i=0; i<creditData.boothIDs.length; i++){
        var tempObj = {};
        tempObj.boothID = creditData.boothIDs[i];
        tempObj.creditDataID = newCreditData.id;
        forBoothsUpdate.push(tempObj);
      }

      // Update all concerned booths      
      async.eachSeries(forBoothsUpdate, function(booth, _callback){
        boothDB.update({'id' : booth.boothID}, {$push : {creditDataIDs : booth.creditDataID}}, function(err){
          if(err){
            console.log(err);
          }
          _callback();
        });
      }, function(err){
        newCreditData.save(callback([], newCreditData.id )); 
      });
    }
  });
};

exports.addNewClaimData = function(claimData, callback){
  console.log("ADDING NEW CLAIM DATA!");
  claimDataDB.find({}, function (err, claims) {
    if(err){ 
      console.log(err);
      callback(["ERROR ACCESSING DB"], null); 
    }
    else{
      // add Claim Data
      var newClaimData = new claimDataDB();
      newClaimData.id = claims.length + 1;
      newClaimData.name = claimData.name;
      newClaimData.creditDataIDs = claimData.creditDataIDs;
      newClaimData.creditDataPoints = claimData.creditDataPoints;
      newClaimData.boothIDs = claimData.boothIDs;
      newClaimData.eventID = claimData.eventID;
    
      // create temporary object to facilitate updating of related booths
      var forBoothsUpdate = [];
      
      for(var i=0; i<claimData.boothIDs.length; i++){
        var tempObj = new Object();
        tempObj.boothID = claimData.boothIDs[i];
        tempObj.creditDataID = newClaimData.id;
        forBoothsUpdate.push(tempObj);
      }

      // Update all concerned booths
      async.eachSeries(forBoothsUpdate, function(booth, _callback){
        boothDB.update({'id' : booth.boothID}, {$push : {claimDataIDs : booth.claimDataID}}, function(err){
          if(err){ 
            console.log(err);
          }
          _callback();
        });
      }, function(err){
        newClaimData.save(callback([], newClaimData.id )); 
      });
    }
  });
};

exports.getAllGuestCredits = function(data, callback){
  var query = null;
  console.log(data);
  if(data.isGuestID == "true"){ 
    query = creditDB.find({'guestID' : data.id});
    console.log('guestid');
  }
  else{ 
    query = creditDB.find({'uID' : data.id}); 
  }

  console.log(data.id);
  console.log(data.isGuestID);

  query.exec(function (err, dbItems) {
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItems !== null){
      var allCredits = [];
      console.log(dbItems);
      callback([], dbItems);
    }
    else{ 
      callback([], null); 
    }
  });
};

exports.getAllGuestsWithValidCredits = function(creditData, callback){
  creditDB.find( {$and:[{'creditDataID' : creditData.creditDataID}, {'totalValue' : {$gte : creditData.minTotalValue}} ]}, function(err, dbItems){
    if(err){
      console.log(err);
      callback(["DB ERROR"], null);
    }
    else if(dbItems !== null){
      var allCredits = [];
      console.log(dbItems)
      
      var _resultAccumulator = [];

      async.eachSeries(dbItems, function(item, _callback){
        guestDB.findOne({'uID' : item.uID}, function(err, dbItem){
          if(err){
            console.log(err);
            _callback();
          }
          else if(dbItem !== null){
            var creditItem = {};
            creditItem.uID = item.uID;
            creditItem.guestID = item.guestID;
            creditItem.totalValue = item.totalValue;
            creditItem.firstName = dbItem.firstName;
            creditItem.lastName = dbItem.lastName;
            creditItem.timeRegistered = dbItem.timeRegistered;
            _resultAccumulator.push(creditItem);
            _callback();
          }
          else{
            console.log('DB Item not found');
            _callback();
          }
        });
      }, function(){
        callback([], _resultAccumulator);
      });
    }
    else{
      callback([], []); 
    }
  });
};

exports.getEventTransactionsByBoothID = function(eventID, date, booths, selectParams, callback){
  if(booths == null){ callback(["NO MATCHES"], null); }
  else if(booths.length == 0){ callback(["NO MATCHES"], null); }
  else
  {
  var allBoothIDs = [];
    for(var i=0; i<booths.length; i++){ allBoothIDs.push(booths[i].id); }
    var query;
    if(date == "All Days"){ 
      query = transactionDB.find({
      $and : [ 
        {eventID : eventID}, 
        {boothID : { $in : allBoothIDs} } ] },
        selectParams ); 
    }
    else{ 
      var unixDate = Date.parse(date);
      var unixDate2 = new Date(unixDate + 86399000)
      query = transactionDB.find({
      $and : [ 
        {eventID : eventID},
        {timestamp : {$gte : unixDate, $lt : unixDate2} }, 
        {boothID : { $in : allBoothIDs} } ] },
        selectParams );
    }
    query.exec(function(err, results){
        if(err){
              callback(["DB ERROR"], null);
            }
            else{
              if(results != null){
                callback([], results);
              }
              else{ callback(["NO MATCHES"], null); }
            }
    })
    /*transactionDB.find({ 
      $and : [ 
        {eventID : eventID}, 
        {boothID : { $in : allBoothIDs} } ] },
        selectParams, 
          function(err, results){
            if(err){
              callback(["DB ERROR"], null);
            }
            else{
              if(results != null){
                callback([], results);
              }
              else{ callback(["NO MATCHES"], null); }
            }
    })*/
  }
};

exports.getEventTransactionsByBoothID2 = function(eventID, booths, selectParams, callback){
  if(booths == null){ callback(["NO MATCHES"], null); }
  else if(booths.length == 0){ callback(["NO MATCHES"], null); }
  else
  {
    var allBoothIDs = [];
    for(var i=0; i<booths.length; i++){ allBoothIDs.push(booths[i].id); }
    var query = transactionDB.find({
      $and : [ 
        {eventID : eventID}, 
        {boothID : { $in : allBoothIDs} } ] },
        selectParams );
    query.exec(function(err, results){
        if(err){
              callback(["DB ERROR"], null);
            }
            else{
              if(results != null){
                callback([], results);
              }
              else{ callback(["NO MATCHES"], null); }
            }
    })
  }
}

exports.getEventEngagementsCount = function(eventID, date, callback){
  var query;
  if(date == "All Days"){ 
    query = transactionDB.find({eventID : eventID}); 
  }
  else{ 
    var unixDate = Date.parse(date);
    var unixDate2 = new Date(unixDate + 86399000)
    query = transactionDB.find({
      $and : [
      {eventID : eventID}, 
      {timestamp : { $gte : unixDate, $lt : unixDate2} }
      ]}); 
  }
  query.exec(function(err, results){
    if(err){
            console.log(err);
            callback(["DB ERROR"], null);
          }
          else{ 
            if(results != null){
              callback([], results.length);
            }
            else { callback([], 0); }
          }
  })
};
