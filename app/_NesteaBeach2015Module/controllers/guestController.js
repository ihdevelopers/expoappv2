'use strict';

var Guest = require('../../objects/Guest');
var _nesteaGuest = require('../objects/Guest');

exports.verifyGuestDetails = function(req, res){

  console.log("Verify Guest Details -- ");
  console.log(JSON.stringify(req.body));

  if(req.body.fbUsername){
    Guest.fetchGuestsByEmailorFBID(req.body, function(err, guestDetails){
      if(err.length === 0){
        if(guestDetails.length > 0){

          //check duplicate type:
          if(guestDetails[0].fbUsername == req.body.fbUsername){
            res.send({status: true, message: 'Facebook has already been registered. Please proceed to \'Search\''});
          }
          else{
            res.send({status: true, message: 'Email already in use. Kindly provide another email address.'});
          }
        }
        else{
          res.send({status: false});
        }
      }
      else{
        console.log("ERROR HERE: " + err[0]);
        res.status(500).send(err[0]);
      }
    });
  }

  else{
    Guest.fetchGuestByEmail(req.body, function(err, guestDetails){
      if(err.length === 0){
        if(guestDetails.length > 0){
          res.send({status: true, message: 'Email already in use. Kindly provide another email address.'});
        }          
        else{
          res.send({status: false});
        }
      }
      else{
        console.log("ERROR HERE: " + err[0]);
        res.status(500).send(err[0]);
      }
    });
  }
    
};

exports.verifyUID = function(req, res){
  console.log("Verify Tag Data -- ");
  console.log(JSON.stringify(req.body));

  Guest.fetchGuestByUID2(req.body, function(err, guestDetails){
    if(err.length === 0){

      if(guestDetails.length > 0){
        res.send({status: true});
      }
      else{
        res.send({status: false});
      }
    }
    else{
      console.log("ERROR HERE: " + err[0]);
      res.status(500).send(err[0]);
    }
  });

};

exports.verifyFbToken = function(req, res){
  console.log("@NESTEA2015/VERIFYFBTOKEN");
  _nesteaGuest.verifyFbToken(req.body, function(err, status){
    if(err.length === 0){
      res.send({status : "OK", data : status});
    }
    else{ res.status(500).send({status : "ERROR", data : "error"}); }
  })
}

exports.postFbMessage = function(req, res){
  console.log("@NESTEA2015/POSTTOFB");
  _nesteaGuest.postFbMessage(req.body, function(err, status){
    if(err.length === 0){
      res.send({status : "OK", data : status});
    }
    else{ res.status(500).send({status : "ERROR", data : "error"}); }
  })
}

exports.registerAdditionalDetails = function(req, res){
  console.log("@NESTEA2015/REGISTERADDITIONALDETAILS");
  console.log(JSON.stringify(req.body));
  _nesteaGuest.registerAdditionalDetails(req.body.guestDetails, function(err, status){
    if(err.length === 0){
      res.send({status : "OK", data : status});
    }
    else{ res.status(500).send({status : "ERROR", data : "error"}); }
  })
}

exports.getAdditionalDetails = function(req, res){
  console.log("@NESTEA2015/GETADDITIONALDETAILS");
  var guestID = req.query.guestID;
  _nesteaGuest.getAdditionalDetails(guestID, function(err, guestDetails){
    if(err.length === 0){
      res.send({status : "OK", data : guestDetails});
    }
    else{ res.status(500).send({status : "ERROR", data : null}); }
  })
}