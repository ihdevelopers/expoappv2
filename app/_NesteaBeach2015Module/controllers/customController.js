'use strict';
var http = require('http');

var callbackLocal = function(data){ console.log("here somehow"); }

exports.getChargingSlotStatusAll = function(req, res){

	var postData = JSON.stringify({request : "status"});

	var options = {
	  hostname: 'nwlabs.ph',
	  path: '/?request=status',
	  method: 'GET',
	  headers: {
	    'Content-Type': 'application/jsonp',
	    'Content-Length': postData.length
	  },
	  callback: callbackLocal
	};

	var req = http.request(options, function(res) {
	  console.log('STATUS: ' + res.statusCode);
	  console.log('HEADERS: ' + JSON.stringify(res.headers));
	  //res.setEncoding('utf8');
	  res.on('data', function (chunk) {
	    console.log('BODY: ' + chunk);
	  });
	});
	
	req.on('error', function(e) {
	  console.log('problem with request: ' + e.message);
	});
	
	// write data to request body
	req.write(postData);
	req.end();

	res.send({status : "OK", data : "status"});
}