'use strict';

var guestController = require('./guestController');
var  customController = require('./customController');

exports.guestController = guestController;
exports.customController = customController;