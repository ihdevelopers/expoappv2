
var guestController = require('../controllers').guestController;
var customController = require('../controllers').customController;

module.exports = function (app) {

  //check guest duplicates
  app.post('/nestea2015/verifyGuestDetails', guestController.verifyGuestDetails);

  //check for uid duplicates
  app.post('/nestea2015/verifyUID', guestController.verifyUID);

  //check if user fb token is still valid
  app.post('/nestea2015/verifyFbToken', guestController.verifyFbToken);

  //post a status message to fb app
  app.post('/nestea2015/postFbMessage', guestController.postFbMessage);

  //request status to chargingstation queue server
  app.post('/nestea2015/getChargingSlotStatusAll', customController.getChargingSlotStatusAll);

  //register additional and Nestea2015-specific  
  app.post('/nestea2015/registerAdditionalGuestDetails', guestController.registerAdditionalDetails);

  // get additional and Nestea2015-specific guest details
  app.get('/nestea2015/getAdditionalGuestDetails', guestController.getAdditionalDetails);

};