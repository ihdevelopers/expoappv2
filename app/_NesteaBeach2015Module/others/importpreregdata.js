//*****************************************************
// READS CSV FILE from Nuworks Nestea Beach Reg site
// and imports data to local DB
//*****************************************************
var guestDB = require('../../models/expoAppGuestModel.js');
var nesteaGuestDetailsDB = require('../models/nesteaBeachUserModel.js');

var _csv = require("fast-csv");
var csvFilePath = "nuworks_reg_04292015_1342h_EDITED_2.csv";

var dbConnectionString = 'mongodb://localhost:27017/ExpoAppBetaDB-Nestea2015Test1';
var mongoose = require('mongoose');
var db = mongoose.connect(dbConnectionString);

var count = 1;

_csv
 .fromPath(csvFilePath, {headers : true, ignoreEmpty: true})
 .on("data", function(data){

     // ADD TO GUEST DB 
                  console.log("GUEST DB COUNT : " + count.toString());

                  var newGuest = new guestDB();
                  newGuest.id = count;
                  newGuest.fbToken = data.accessToken;                
                  newGuest.firstName = data.firstName;
                  newGuest.lastName = data.lastName;
                  newGuest.middleName = data.middleName;
                  newGuest.nickName = "";
                  newGuest.gender = "Male";
                  newGuest.email = data.email;
                  newGuest.mobileNo = data.mobile;
                  newGuest.title = null;
                  newGuest.profession = null;
                  newGuest.company = "";
                  newGuest.education = "Graduate";
                  newGuest.streetAddress1 = "";
                  newGuest.streetAddress2 = "";
                  newGuest.cityAddress = "";
                  newGuest.regionAddress = "";
                  newGuest.country = "";
                  newGuest.zip = "";
                  newGuest.religion = "";
                  newGuest.facebookPage = "";
                  newGuest.twitterHandle = "";
                  newGuest.instagramHandle = "";
                  newGuest.uID = "";
                  newGuest.uID2 = "";
                  newGuest.transaction_id = data.transaction_id;
                  newGuest.fb_registration_id = 0;
                  newGuest.creditIDs = [];
                  newGuest.transactionIDs = [];
                  newGuest.eventID = 1;
                  newGuest.preRegistered = true;
                  newGuest.birthDate = data.birthday;
                  newGuest.fbUsername = data.fb_id;
                  var parts = data.paymentDate.split('/');
                  newGuest.timeRegistered = (new Date(parts[2],parts[0]-1,parts[1])).getTime();
                  newGuest.preRegistered = newGuest.timeRegistered;
                  newGuest.timePresent = newGuest.timeRegistered;
                  newGuest.isPresent = true;
                  newGuest.isAnonymous = false;

                  //----------------------------------
                  // ...
                  // ADD MORE GUEST DETAILS HERE
                  // ...
                  //----------------------------------
                  
                  newGuest.save(function(errSave){
                    if(errSave){ 
                      console.log(errSave);
                      //callback(["DB ERROR SAVING"], null); 
                    }
                    else{                       
                      //callback([], newGuest.id);
                      // ADD TO NESTEA GUEST DB
                      var newNesteaGuest = new nesteaGuestDetailsDB();

                      newNesteaGuest.guestID            = newGuest.id;
                      newNesteaGuest.transaction_id     = data.transaction_id;
                      newNesteaGuest.securityQuestion   = data.securityQuestion;
                      newNesteaGuest.securityAnswer     = data.answer;
                      newNesteaGuest.isLegalAge         = true;
                      
                      if(data.LovetheBeach == "1"){ newNesteaGuest.lovethebeach = true; }
                      else{ newNesteaGuest.lovethebeach = false; }
                      
                      if(data.SanderVanDoorn == "1"){ newNesteaGuest.sandervandoorn = true; }
                      else{ newNesteaGuest.sandervandoorn = false; }
                      
                      if(data.KnifeParty == "1"){ newNesteaGuest.knifeparty = true; }
                      else{ newNesteaGuest.knifeparty = false; }
                      
                      var parts = data.paymentDate.split('/');
                      newNesteaGuest.paymentDate  = new Date(parts[2],parts[0]-1,parts[1]);

                      var parts2 = data.buyDate.split('/');
                      newNesteaGuest.buyDate  = new Date(parts2[2],parts2[0]-1,parts2[1]);
                      
                      newNesteaGuest.paymentStatus      = true;
                      newNesteaGuest.paymentAmount      = parseInt(data.amount);

                      newNesteaGuest.save(function(errSave2){
                        if(errSave2){ 
                          console.log(errSave2);
                          //callback(["DB ERROR SAVING"], null); 
                        }
                        else{

                        }
                      }) 

                    }
                  });

      count = count + 1;


     // ADD TO NESTEA GUEST DB

     // ADD TO CREDITS DB

     // ADD TO TRANSACTIONS DB

 })
 .on("end", function(){
     console.log("done");
 })