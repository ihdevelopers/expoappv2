//==========================================================
// APP OBJECT SCHEMAS - ADDITIONAL GUEST DETAILS SCHEMA
//==========================================================
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var nesteaBeachUserSchema = mongoose.Schema({
  guestID : Number,
  transaction_id : String,
  securityQuestion : String,
  securityAnswer : String,
  isLegalAge : Boolean,
  lovethebeach : Boolean,
  sandervandoorn : Boolean,
  knifeparty : Boolean,
  buyDate : Date,
  paymentDate : Date,
  paymentStatus : String,
  paymentAmount : Number
});

module.exports = mongoose.model('nesteaBeachUserDB', nesteaBeachUserSchema);