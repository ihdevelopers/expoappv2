'use strict';

var guestDB = require('../../models/expoAppGuestModel.js');
var nesteaGuestDetailsDB = require('../models/nesteaBeachUserModel.js');
var mainAppGuestController = require('../../objects/Guest.js');
var request = require('request');
var config = require('../../../config/_NesteaBeach2015/all.js');
var FB = require('fb');
var fbConfig = require('../../../config/fbConfig.js');

/*var dummyFBAccounts = [
		{
			guestAccessToken : 'CAALvShdKHJ0BAEmv2qkBwz7TIyhw7NpUHrAEJT7fML0sFDeBl21WZA0juMEIFHsZBcI0u8fcd0hB9vq8hophvfFhKSxqUZAam945QTJCHK2x8nRvD2NZAisOzkeiAZCZAkXWddkpagAZCwKa7P6tfjnfdj8XRHd0jsP3FA9EgZA4Bmpk2h4LbZA63',
			appSecret : '239d4bdf351c0798c90503c59d9728b1'
		},
		{
			guestAccessToken : 'CAALvShdKHJ0BAHVjeVoLQ0lU2aVHtoBfNGO8CZBt3ZBW73d1v0pTAgsS6Cj3tnYk801rGZCZBKFXPJqkkd5K5ZC0ZASh5CF4iH7ynvj2GicE1ZAW5iDLq9osl4vkKZBTNvj4g7GTxt0mvmHwYkf2DMRpVAZBn5JZAdUvmsx7KZAwbuZCBNI9vHhhHvDwC1SJnlfC6lkZD',
			appSecret : '239d4bdf351c0798c90503c59d9728b1'
		},
		{
			guestAccessToken : 'CAAMCZCDB8sOIBAPhOnFrFkmcOAw6jGne9jTNZAVX6w6BePYIHdyunNmrni2CLDCw49nrCaLfUEEGHPXsj2XvgJQkx4N9gZC9UVPXdo4xHuNAGyrGVwEk7j9KZCyry1BQlw6ifkZAJr3XzTMoSMgDBHwxiCAlfKXBKraKZAKT5ovD1pZBkKCNR6Cj3c2SCnSeXIZD',
			appSecret : '6c60eb5bd77d36f38f3f9c6b2840ff5d'
		},
		{
			guestAccessToken : 'CAAMCZCDB8sOIBAE8j7tJxKPKUOc7SLHb2YgL1jT2snvyzvoKOaYYZBaZA3buhoRT6oE1uWnZA5viGMZCA2Dgf8p8ZAXMinrIhnIyFajLrQd5tXkMZCihZAuCSgHLmz5ZB6l3TdxJNz3ZA8losGzhaLJRH2cAmTjrPbrECiuaolqRxK4KgZAhM7FcjYf',
			appSecret : '6c60eb5bd77d36f38f3f9c6b2840ff5d'
		},
		{
			guestAccessToken : 'CAAMCZCDB8sOIBADe2sDIN9UaCPcuk1VldnK0WxOh0ElGQtwpClZCrSBzLCvlJM7vZBZCwZA1DlhOCA7VY7evq7QysRJZCmkOnzHgiRiwAN4iyoy9ncmdccnhqlRZAdhZAxaJ6gxMbhAGee7QchQrmT5xEm35MEKSoPcllNo45WrrZBJ6gZAvERsZAsO',
			appSecret : '6c60eb5bd77d36f38f3f9c6b2840ff5d'
		},
		{
			guestAccessToken : 'CAAMCZCDB8sOIBAMhONXKSQKmonU41v3ryX4K0lOMR8S6HCmqWQ3oQqCX8xVwYnA5FSdAjscZBVQXi9ccR4c968zF8ChGfSeYUzZAKKOD8ZAHysOaOebI7HAYaHYHWx9UutiqSg8tqcz5EtmoSD7BNgByVZAGWytsVZBwLyQG63lopYXzohWZAoq8OZC5ZAtsqGWcZD',
			appSecret : '6c60eb5bd77d36f38f3f9c6b2840ff5d'
		},
		{
			guestAccessToken : 'CAAIzfgazQTkBAN7DxOWxkL3PfjZCSvZASzzAG2ERRnncRxrzmeKsmAo3SDA8uH89NPC3rqUZC9pdHsQMOBH4JZC5ZB8sBoOb9ltahLsHwK2ntP3tFS4MrFuzA1OhKMGGdJ1NDP2xfKFDYk51Mjf36bXPP8JqAEtlh4NhjQcVSvsZAkOpfvf57J',
			appSecret : '89d63aaa3676dd841f18e8e0e3b389f4'
		},
		{
			guestAccessToken : 'CAAIzfgazQTkBAOZBRSbYyXJQPIBh5WT1bwTknf3RVsDuaTxsM7wGHsEbTIMuDtqeva7ZB5857YkMw5P4xRyjH8piyr4KRMpvLsdAVVqh8qxK8keY5uBzNRRDjpI8mzmwJNRVVN9Op32UfIpTl6cZC9aG4twDZAXjczv1ZApyxx2ABLRuLWflH',
			appSecret : '89d63aaa3676dd841f18e8e0e3b389f4'
		}
];

var fb_apps_config = [
    {
      appID: '826051450772637',
      appSecret: '239d4bdf351c0798c90503c59d9728b1'
    },
    {
      appID: '826051450772637',
      appSecret: '239d4bdf351c0798c90503c59d9728b1'
    },
    {
      appID: '847707098624226',
      appSecret: '6c60eb5bd77d36f38f3f9c6b2840ff5d'
    },
    {
      appID: '847707098624226',
      appSecret: '6c60eb5bd77d36f38f3f9c6b2840ff5d'
    },
    {
      appID: '169921833199214',
      appSecret: '69982b11a488e9ed56d8cd560beef662'
    }
];*/

exports.registerAdditionalDetails = function(params, callback){
  nesteaGuestDetailsDB.findOne({'guestID':params.guestID}, function(err, _nesteaGuest){
      //DB ERROR
      if(err){ 
        console.log(err);
        callback(["DB ERROR"], null);
      }

      //ALREADY EXISTS!
      else if(_nesteaGuest !== null){
        callback(["GUEST ALREADY EXISTS"], null);
      }

      //NO ERROR
      else{
        var newNesteaGuestDetails = new nesteaGuestDetailsDB();
        newNesteaGuestDetails.guestID = params.guestID;
        newNesteaGuestDetails.transaction_id = params.transaction_id;
  		  newNesteaGuestDetails.securityQuestion = params.securityQuestion;
  		  newNesteaGuestDetails.securityAnswer = params.securityAnswer;
  		  newNesteaGuestDetails.isLegalAge = params.isLegalAge;
  		  newNesteaGuestDetails.lovethebeach = params.loveTheBeach;
  		  newNesteaGuestDetails.sandervandoorn = params.sandervandoorn;
  		  newNesteaGuestDetails.knifeparty = params.knifeparty;
  		  newNesteaGuestDetails.buyDate = params.buyDate;
  		  newNesteaGuestDetails.paymentDate = params.paymentDate;
  		  newNesteaGuestDetails.paymentStatus = params.paymentStatus;
  		  newNesteaGuestDetails.paymentAmount = params.paymentAmount;

  		  newNesteaGuestDetails.save(function(errSave){
            if(errSave){ 
              console.log('ERROR SAVING TO DB');
              callback(["DB ERROR SAVING"], null); 
            }
            else{ 
              callback([], params.guestID); 
            }
          });
      }
	})
};

exports.getAdditionalDetails = function(guestID, callback){
	nesteaGuestDetailsDB.findOne({'guestID':guestID}, function(err, _nesteaGuest){
      //DB ERROR
      if(err){ 
        console.log(err);
        callback(["DB ERROR"], null);
      }

      //ALREADY EXISTS!
      else if(_nesteaGuest !== null){
        callback([], _nesteaGuest);
      }

      //NO ERROR
      else{
        console.log("NO GUEST MATCH");
        callback(["DB ERROR"], null);
      }
	})
};

exports.verifyFbToken = function(params, callback){
	callback([], "VALID TOKEN");
};

exports.postFbMessage = function(params, callback){
	// get user fb access token
	mainAppGuestController.fetchGuestByUID(params.uID, function(err, guest){
		if(guest == null){ 
			console.log("GUEST NOT ON DB! @ POST FB MESSAGE : " + params.uID.toString());
			callback(["error"], "ERROR POSTING, GUEST NOT FOUND");
		}
		else if(err.length === 0){
			console.log("GUEST FOUND @ POST FB MESSAGE");
			if(guest.fbToken == ""){
				console.log("GUEST HAS NO FB TOKEN, POSTING TO DUMMY FB ACCT");
				//var randomFBAcct = dummyFBAccounts[(Math.floor(Math.random() * (dummyFBAccounts.length) ))];
        var randomFBAcct = fbConfig.dummyFBAccounts[(Math.floor(Math.random() * (fbConfig.dummyFBAccounts.length) ))];
				FB.options({ 'appSecret': randomFBAcct.appSecret });
				FB.setAccessToken(randomFBAcct.guestAccessToken);
			}
			else{
        console.log("POSTING TO GUEST FB PAGE");
        //FB.options({ 'appSecret': fb_apps_config[guest.fb_registration_id].appSecret });
        FB.options({ 'appSecret': fbConfig.apps[guest.fb_registration_id].appSecret });
        FB.setAccessToken(guest.fbToken);
        //if( (fb_apps_config[guest.fb_registration_id].appSecret == 'e8555940c90c159b25a72b3df9a19987') || (fb_apps_config[guest.fb_registration_id].appSecret == '89d63aaa3676dd841f18e8e0e3b389f4') ){
        /*if( ( (guest.fb_registration_id.appSecret == 1) || (guest.fb_registration_id == 3) ) && ( guest.timeRegistered < 1430654669330) ){
          console.log("APP NO POST ACTION, POSTING TO DUMMY FB ACCT");
          var randomFBAcct = dummyFBAccounts[(Math.floor(Math.random() * (dummyFBAccounts.length) ))];
          FB.options({ 'appSecret': randomFBAcct.appSecret });
          FB.setAccessToken(randomFBAcct.guestAccessToken);
        }
        else{
          console.log("POSTING TO GUEST FB PAGE");
          FB.options({ 'appSecret': fb_apps_config[guest.fb_registration_id].appSecret });
          FB.setAccessToken(guest.fbToken);
        }*/
			} 
			var message = params.message;
			FB.api('', 'post', {
			    batch: [
			        { method: 'post', relative_url: 'me/feed', body:'message=' + encodeURIComponent(message) }
			    ]
			}, function (res) {
			    var res0;
			
			    if(!res || res.error) {
			        console.log(!res ? 'error occurred' : res.error);
			        callback(["error"], "ERROR POSTING");
			    }
				else{
			    	res0 = JSON.parse(res[0].body);
			    	if(res0.error) {
			    	    console.log(res0.error);
			    	    callback(["error"], "ERROR POSTING");
			    	} else {
			    	    console.log('Post Id: ' + res0.id);
			    	    callback([], "MESSAGE POSTING SUCCESSFUL");
			    	}
				}
			});
      	}
      	else{
        	console.log("ERROR HERE: " + err[0]);
        	callback(["error"], "ERROR POSTING");
      	}
	})
};
